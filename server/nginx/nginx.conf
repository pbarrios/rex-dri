user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;  ## Default: 1024, increase if you have lots of clients
}

http {
    include       /etc/nginx/mime.types;
    # fallback in case we can't determine a type
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    # Parameters of gzip encoding
    gzip on;
    gzip_disable "msie6";

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/x-javascript text/javascript application/javascript image/svg+xml;

    # Forbid huge uploads
    client_max_body_size 2m;

    include /etc/nginx/conf.d/default.conf;
}
