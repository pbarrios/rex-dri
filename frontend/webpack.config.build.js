const merge = require("webpack-merge");
const webpackConfig = require("./webpack.config.base");

module.exports = merge(webpackConfig, {
  mode: "production",
  optimization: {
    minimize: true,
    nodeEnv: "production",
  },
});
