import DateFnsUtils from "@date-io/date-fns";
import format from "date-fns/format";
import parseISO from "date-fns/parseISO";
import frLocale from "date-fns/locale/fr";

/**
 * @class LocalizedUtils
 * @extends {DateFnsUtils}
 */
class LocalizedUtils extends DateFnsUtils {
  getDateText(date) {
    return format(date, "d MMM yyyy, HH:mm", { locale: frLocale });
  }
}

const util = new LocalizedUtils();

export default function toDateFr(dateIso) {
  return util.getDateText(parseISO(dateIso));
}
