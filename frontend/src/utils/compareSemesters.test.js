import compareSemester, { getMostNRecentSemesters } from "./compareSemesters";

test("Compare same", () => {
  expect(compareSemester(2019, "a", 2019, "a")).toBeCloseTo(0);
});

test("Compare same year different semester", () => {
  expect(compareSemester(2019, "a", 2019, "p")).toBeGreaterThan(0);
});

test("Compare different year same semester", () => {
  expect(compareSemester(2020, "a", 2019, "a")).toBeGreaterThan(0);
});

test("Both different", () => {
  expect(compareSemester(2018, "p", 2019, "a")).toBeLessThan(0);
});

test("Extract the correct list of semester", () => {
  const listOfSemester = [
    "A2019",
    "A2014",
    "A2013",
    "A2012",
    "A2011",
    "P2014",
    "P2009",
    "A2010",
    "A2009",
    "A2008",
    "P2012",
    "P2011",
    "P2010",
    "P2015",
    "P2013",
    "A2018",
  ];

  const mostRecents = getMostNRecentSemesters(listOfSemester, 5);
  expect(mostRecents[0]).toBe("A2019");
  expect(mostRecents[1]).toBe("A2018");
  expect(mostRecents[2]).toBe("P2015");
  expect(mostRecents[3]).toBe("A2014");
  expect(mostRecents[4]).toBe("P2014");
});
