/**
 * Function to render the appropriate string to pass to component when multiple classes are wanted.
 * @param classes
 * @returns {string}
 */
export default function classNames(...classes) {
  return classes.join(" ");
}
