/**
 * Truncate a string to respect the maxLength argument.
 *
 * @param {string} str
 * @param {number} maxLength
 * @returns {string}
 */
export default function truncateString(str, maxLength) {
  if (maxLength && str.length > maxLength + 1) {
    return str.substring(0, maxLength + 1);
  }
  return str;
}
