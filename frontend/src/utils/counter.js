/**
 * Helper class just like the one in python
 */
class Counter {
  /**
   * @param {Iterable} elements
   */
  constructor(elements) {
    const res = new Map();

    elements.forEach((el) => {
      if (!res.has(el)) {
        res.set(el, 0);
      }
      res.set(el, res.get(el) + 1);
    });

    /**
     * @type {Map<any, number>}
     */
    this.res = res;
  }

  /**
   * return the keys that have duplicates
   * @returns {Array.<any>}
   */
  getDuplicatedElements() {
    return [...this.res.entries()]
      .filter(([, count]) => count > 1)
      .map(([key]) => key);
  }
}

export default Counter;
