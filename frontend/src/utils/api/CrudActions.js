/*
 * This file contains the functions and class to create the CRUD actions
 */

import axiosLib from "axios";
import getCrudActionTypes from "./getCrudActionTypes";
import RequestParams from "./RequestParams";

/** base configuration of axios. */
const axios = axiosLib.create({
  baseURL: "/api",
  xsrfCookieName: "csrftoken",
  xsrfHeaderName: "X-CSRFToken",
});

/**
 * Class that contains all actions related to API handling
 *
 * @export
 * @class CrudActions
 */
export default class CrudActions {
  /**
   * For request optimization purposes
   * @type {Set<string>}
   */
  static interactionStatus = new Set();

  /**
   * Creates an instance of CrudActions.
   * @param {string} route Api route associated with the instance
   */
  constructor(route) {
    this.apiEndPoint = route;
    this.types = getCrudActionTypes(route);
  }

  /**
   *
   *
   *
   *
   *
   *
   *
   *  Helper functions
   *
   *
   *
   *
   *
   *
   */

  /**
   * Wrap the action to add the `rootType` for optimization purposes.
   *
   * @param {object} action
   * @returns {object}
   * @private
   */
  wrap(action) {
    return Object.assign(action, { rootType: this.types.rootType });
  }

  /**
   * Helper function around axios to make the request to the backend API
   *
   * @param {RequestParams} params axios config (see axios doc)
   * @param {string} method HTTP method to use
   * @param {string} temporaryType Type to dispatch when things start
   * @param {string} successType Type to dispatch when the request is successful
   * @param {string} errorType Type to dispatch when an error occurred during request
   * @returns {function}
   * @private
   */
  performAxios(params, method, temporaryType, successType, errorType) {
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV !== "production") {
      if (!(params instanceof RequestParams)) {
        throw new Error("`params` must be an instance of RequestParams");
      }
    }

    let { data } = params;
    if (method === "post" || method === "put") {
      // We need different sending method than Json when sending files
      // Not 100% bullet proof but should do the trick for the project;
      const dataHasFile = Object.values(data).some((v) => v instanceof File);

      if (dataHasFile) {
        // We convert the data to formData
        const formData = new FormData();
        Object.entries(data).forEach(([key, val]) => {
          const value = val === null ? "" : val;
          formData.set(key, value);
        });
        data = formData;
      }
    }

    const config = {
      url: `${this.apiEndPoint}/${params.url}`,
      method,
      data,
    };
    const requestId = `${config.method}_${config.url}`;

    // a bit of optimization to make sure not to make overlapping requests
    if (CrudActions.interactionStatus.has(requestId)) {
      return () => {};
    }

    CrudActions.interactionStatus.add(requestId);

    return (dispatch) => {
      const wrapDispatch = (action) => dispatch(this.wrap(action));

      wrapDispatch({ type: temporaryType });
      axios
        .request(config)
        .then(({ data: newData }) => {
          wrapDispatch({
            type: successType,
            data: newData,
            requestParams: params,
          });
          params.onSuccessCallback(newData);
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(JSON.stringify(error, null, 2));
          wrapDispatch({
            type: errorType,
            error,
          });
        })
        .then(() => CrudActions.interactionStatus.delete(requestId));
    };
  }

  /**
   *
   *
   *
   *
   *
   *
   *
   *  Actions
   *
   *
   *
   *
   *
   *
   *
   */

  /**
   *
   *
   * Read all
   *
   *
   */

  /**
   * Read all objects on the API route.
   * You can use request parameters to filter on the endpoint. Set them in the `params` attributes.
   * You can specify other parameters with `params`.
   *
   * @param {RequestParams} params
   * @returns {function}
   */
  readAll = (params = RequestParams.Builder.build()) => {
    params.checkDoesntHaveId(true);
    const { readAllStarted, readAllSucceeded, readAllFailed } = this.types;
    return this.performAxios(
      params,
      "get",
      readAllStarted,
      readAllSucceeded,
      readAllFailed
    );
  };

  /**
   * Action to reset a read all failure.
   *
   * @returns
   */
  clearReadAllFailed = () =>
    this.wrap({
      type: this.types.clearReadAllFailed,
    });

  /**
   *
   *
   *  Read One
   *
   *
   */

  /**
   * Read one object.
   * The id of the object must be specified in `params`.
   * You can specify other parameters with `params`.
   *
   * @param {RequestParams} params Request parameters
   * @returns {function}
   */
  readOne = (params) => {
    params.checkHasId(true);
    const { readOneStarted, readOneSucceeded, readOneFailed } = this.types;
    return this.performAxios(
      params,
      "get",
      readOneStarted,
      readOneSucceeded,
      readOneFailed
    );
  };

  /**
   * Action to reset a read one failure.
   *
   * @returns
   */
  clearReadOneFailed = () =>
    this.wrap({
      type: this.types.clearReadOneFailed,
    });

  /**
   *
   *
   * create
   *
   *
   */

  /**
   * Create an object with fields and values `data` (inside `params`).
   *
   * @param {RequestParams} params
   * @returns {function}
   */
  create(params) {
    params.checkDoesntHaveId(true);
    const { createStarted, createSucceeded, createFailed } = this.types;
    return this.performAxios(
      params,
      "post",
      createStarted,
      createSucceeded,
      createFailed
    );
  }

  /**
   *
   * @returns
   */
  clearCreateFailed = () =>
    this.wrap({
      type: this.types.clearCreateFailed,
    });

  /**
   *
   *
   *
   *  update
   *
   *
   */

  /**
   * Updates an object identified by its id (in params.id).
   *
   * @param {RequestParams} params
   * @returns {function}
   */
  update(params) {
    params.checkHasId(true);
    const { updateStarted, updateSucceeded, updateFailed } = this.types;

    return this.performAxios(
      params,
      "put",
      updateStarted,
      updateSucceeded,
      updateFailed
    );
  }

  /**
   * Action to reset an update failure.
   * @returns
   */
  clearUpdateFailed = () =>
    this.wrap({
      type: this.types.clearUpdateFailed,
    });

  /**
   *
   *
   *
   * performDelete
   *
   *
   */

  /**
   * Deletes an object identified by its id (inside params)
   *
   * @param {RequestParams} params
   * @returns {function}
   */
  delete(params) {
    params.checkHasId(true);
    const { deleteStarted, deleteSucceeded, deleteFailed } = this.types;
    return this.performAxios(
      params,
      "delete",
      deleteStarted,
      deleteSucceeded,
      deleteFailed
    );
  }

  /**
   * Action to reset a performDelete failure.
   * @returns
   */
  clearDeleteFailed = () =>
    this.wrap({
      type: this.types.clearDeleteFailed,
    });

  /**
   *
   *
   * invalidate all
   *
   *
   *
   */

  /**
   * Action to mark the "read all" data as invalidated.
   * @returns
   */
  invalidateAll = () =>
    this.wrap({
      type: this.types.invalidateAll,
    });

  /**
   * Action to clear the invalidation of the "read all" data.
   * @returns
   */
  clearInvalidationAll = () =>
    this.wrap({
      type: this.types.clearInvalidationAll,
    });

  /**
   *
   *
   *
   *
   * invalidate One
   *
   *
   */

  /**
   * Action to mark the "read one" data as invalidated.
   * @returns
   */
  invalidateOne = () =>
    this.wrap({
      type: this.types.invalidateOne,
    });

  /**
   * Action to clear the invalidation of the "read one" data.
   * @returns
   */
  clearInvalidationOne = () =>
    this.wrap({
      type: this.types.clearInvalidationOne,
    });
}
