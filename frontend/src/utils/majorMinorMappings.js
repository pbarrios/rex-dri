/**
 * Mappings for updating majors
 * DUMMIES are for tests
 * @type {Object.<String, String>}
 */
export const majorMapping = {
  GSM: "IM",
  GM: "IM",
  GSU: "GU",
  _DUMMY_MAJOR_1: "_DUMMY_MAJOR_1",
};

/**
 * Mapping for updating minors
 * DUMMIES are for tests
 * @type {Object.<String, Object.<String,String>>}
 */
export const minorMapping = {
  _DUMMY_MAJOR_1: {
    _DUMMY_MINOR_1: "_DUMMY_MINOR_2",
  },
};

/**
 * Returns the updated major
 * @param {String} major
 * @returns {String}
 */
export function getUpdatedMajor(major) {
  if (major in majorMapping) {
    return majorMapping[major];
  }
  return major;
}

/**
 * Returns the updated minor
 * @param {String} major
 * @param {String} minor
 * @returns {String}
 */
export function getUpdatedMinor(major, minor) {
  if (major in minorMapping && minor in minorMapping[major]) {
    return minorMapping[major][minor];
  }
  return minor;
}

/**
 * Returns all the majors that correspond to a major
 * @param {String} major
 * @returns {Array.<String>}
 */
export function getAllMajors(major) {
  const out = new Set();
  out.add(major);
  Object.entries(majorMapping)
    .filter(([, newMajor]) => newMajor === major)
    .forEach(([oldMajor]) => {
      out.add(oldMajor);
    });

  return [...out];
}

/**
 * Returns all the minors that correspond to a minor
 * @returns {Array.<String>}
 */
export function getAllMinors(major, minor) {
  const out = new Set();
  out.add(minor);

  const allMajors = getAllMajors(major);
  allMajors.forEach((el) =>
    Object.entries(minorMapping)
      .filter(([maj]) => maj === el)
      .forEach(([, minors]) => {
        Object.entries(minors)
          .filter(([, newMinor]) => newMinor === minor)
          .forEach(([oldMinor]) => {
            out.add(oldMinor);
          });
      })
  );
  return [...out];
}

/**
 * Returns the updated data from univMajorMinors endpoint
 *
 * @param {Array.<{major: string, minors: Array.<String>}>} univMajorMinors
 * @returns {Array.<{major: string, minors: Array.<String>}>}
 */
export function getUpdatedUnivMajorMinors(univMajorMinors) {
  /**
   * @type {Map<string, Set.<string>>}
   */
  const agg = new Map();

  univMajorMinors.forEach((el) => {
    const newMajor = getUpdatedMajor(el.major);
    const newMinors = el.minors.map((min) => getUpdatedMinor(el.major, min));

    if (!agg.has(newMajor)) agg.set(newMajor, new Set(newMinors));
    const aggMajor = agg.get(newMajor);
    newMinors.forEach((m) => {
      aggMajor.add(m);
    });
  });

  return [...agg.entries()].map(([major, minorsSet]) => ({
    major,
    minors: [...minorsSet],
  }));
}
