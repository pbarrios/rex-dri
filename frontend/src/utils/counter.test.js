import Counter from "./counter";

test("Count nothing empty string", () => {
  const counter = new Counter([]);
  expect(counter.res).toMatchObject(new Map());
});

test("Count one element", () => {
  const counter = new Counter(["coucou"]);
  expect(counter.res).toMatchObject(new Map([["coucou", 1]]));
});

test("Count twice element", () => {
  const counter = new Counter(["coucou", "coucou"]);
  expect(counter.res).toMatchObject(new Map([["coucou", 2]]));
});

test("Count two elements diff", () => {
  const counter = new Counter(["coucou", "coucou", "test"]);
  expect(counter.res).toMatchObject(
    new Map([
      ["coucou", 2],
      ["test", 1],
    ])
  );
});

test("Test show duplicated", () => {
  const counter = new Counter(["coucou", "coucou", "test"]);
  expect(counter.getDuplicatedElements()).toMatchObject(["coucou"]);
});
