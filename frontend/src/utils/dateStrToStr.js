/**
 * Converts a string "yyyy-mm-dd" into "dd/mm/yyyy"
 *
 * @export
 * @param {string} dateTimeStr
 * @returns {string}
 */
export default function dateStrToStr(dateTimeStr) {
  const reg = /(\d{4})-(\d{2})-(\d{2})/;
  const res = reg.exec(dateTimeStr);
  const yyyy = res[1];
  const mm = res[2];
  const dd = res[3];

  return `${dd}/${mm}/${yyyy}`;
}
