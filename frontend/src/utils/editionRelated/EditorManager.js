import NotificationService from "../../services/NotificationService";

class EditorManager {
  // Store the "saving" notification so that we can performDelete it when done saving.
  savingNotification = null;

  // Notifications related

  notifyNoChangesDetected() {
    NotificationService.info("Aucun changement n'a été repéré.");
  }

  notifyFormHasErrors() {
    NotificationService.error(
      "Le formulaire semble incohérent, merci de vérifier son contenu."
    );
  }

  notifyIsSaving() {
    this.savingNotification = NotificationService.notify(
      "Enregistrement en cours",
      {
        variant: "info",
        persist: true,
        action: null,
      }
    );
  }

  notifySaveSuccessful(message) {
    setTimeout(() => this.removeSavingNotification(), 1500); // add a little delay for smoothing
    NotificationService.notify(message, {
      variant: "success",
      autoHideDuration: 5000,
    });
  }

  removeSavingNotification() {
    if (this.savingNotification) {
      NotificationService.closeNotification(this.savingNotification);
      this.savingNotification = null;
    }
  }
}

export default EditorManager;
