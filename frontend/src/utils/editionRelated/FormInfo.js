/**
 * Class to easily share information between form/fields/editors, etc.
 */
class FormInfo {
  /**
   * @param {string} route - api route to be used
   * @param {function} Form - Form component containing the fields
   * @param {array.<FormLevelError>} [formLevelErrors] - Errors that can only be computed at the form level (by compbining multiple fields)
   * @param {string} [license] - license of the data in the form
   */
  constructor(route, Form, formLevelErrors = [], license = "REX-DRI—BY") {
    this.route = route;
    this.Form = Form;
    this.formLevelErrors = formLevelErrors;
    this.license = license;
  }
}

export default FormInfo;
