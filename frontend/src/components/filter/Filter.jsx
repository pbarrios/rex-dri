import React, { useEffect } from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import { v4 as uuidv4 } from "uuid";
import { makeStyles } from "@material-ui/styles";
import { Checkbox } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import DownshiftMultiple from "../common/DownshiftMultiple";
import useGlobalState from "../../hooks/useGlobalState";
import FilterService from "../../services/FilterService";
import FilterStatus from "./FilterStatus";
import { useSetSelectedUniversities } from "../../hooks/wrappers/useSelectedUniversities";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium,
  },
  input: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(0.5),
  },
  spacer1: {
    marginTop: theme.spacing(1),
  },
  spacer2: {
    marginTop: theme.spacing(2),
  },
}));

const DOWNSHIFT_COUNTRIES_ID = uuidv4();
const DOWNSHIFT_SEMESTERS_ID = uuidv4();
const DOWNSHIFT_MAJORS_ID = uuidv4();
const MINIMUM_NUMBER_OPEN_DESTINATIONS = 100;

/**
 * Implementation of a filter component
 */
function Filter() {
  const classes = useStyles();
  const saveSelectedUniversities = useSetSelectedUniversities();

  const [isOpened, setIsOpened] = useGlobalState("filter-open", false);
  const [countries, setCountries] = useGlobalState("filter-countries", []);
  const [semesters, setSemesters] = useGlobalState(
    "filter-semesters",
    [...FilterService.defaultSemesters].reverse()
  );

  const [majorMinors, setMajorMinors] = useGlobalState(
    "filter-major-minors",
    []
  );

  const enoughDestinationsAreOpen =
    FilterService.destinationOpenCount >= MINIMUM_NUMBER_OPEN_DESTINATIONS;

  const [onlyOpenDestinations, setOnlyOpenDestinations] = useGlobalState(
    "filter-only-open-destinations",
    enoughDestinationsAreOpen
  );

  useEffect(() => {
    const selectedUniversities = FilterService.getSelection(
      countries,
      semesters,
      majorMinors,
      onlyOpenDestinations
    );

    const hasSelection =
      [countries, semesters, majorMinors].some((arr) => arr.length !== 0) ||
      onlyOpenDestinations === true;
    saveSelectedUniversities(hasSelection ? selectedUniversities : null);
  }, [countries, semesters, majorMinors, onlyOpenDestinations]);

  const {
    countriesOptions,
    majorMinorOptions,
    semesterOptions,
  } = FilterService;

  return (
    <ExpansionPanel
      expanded={isOpened}
      onChange={() => {
        setIsOpened(!isOpened);
      }}
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className={classes.heading}>
          Appliquer des filtres
        </Typography>
        <FilterStatus />
      </ExpansionPanelSummary>
      <ExpansionPanelDetails style={{ display: "block" }}>
        <Typography variant="caption">
          Le options internes des filtres sont composées avec un « ou » logique.
          Les filtres sont composés entre eux avec un « et » logique.
        </Typography>
        <div className={classes.spacer1} />
        <div className={classes.input}>
          <DownshiftMultiple
            fieldPlaceholder="Filter par pays"
            options={countriesOptions}
            onChange={setCountries}
            cacheId={DOWNSHIFT_COUNTRIES_ID}
          />
        </div>
        <div className={classes.spacer2} />
        <Typography>Filtres avancés</Typography>
        <Typography variant="caption">
          REX-DRI s'efforce d'être à jour avec l'ENT. Toutefois, seul l'ENT fait
          foi à 100% concernant les possibilités passées et actuelles
          d'échanges.
        </Typography>
        <div className={classes.input}>
          <DownshiftMultiple
            fieldPlaceholder={
              "Filter par semestre (lorsqu'un échange est/était possible)"
            }
            options={semesterOptions}
            onChange={setSemesters}
            cacheId={DOWNSHIFT_SEMESTERS_ID}
            value={semesters}
          />
        </div>
        <div className={classes.input}>
          <DownshiftMultiple
            fieldPlaceholder={
              "Filter par branches et filières (lorsqu'un échange est/était possible)"
            }
            options={majorMinorOptions}
            onChange={setMajorMinors}
            cacheId={DOWNSHIFT_MAJORS_ID}
          />
          <Typography variant="caption">
            Ce filtre prend en compte les échanges effectués depuis le semestre
            d'automne 2008, et les possibilités d'échanges enregistrées par la
            DRI depuis juillet 2019.
            <br />
            Attention, lorsqu'un filtre comportant une filière est appliqué,
            seuls les échanges déjà effectués sont pris en compte.
          </Typography>
        </div>
        <div className={classes.input}>
          <FormControl>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={onlyOpenDestinations}
                  onChange={() =>
                    setOnlyOpenDestinations(!onlyOpenDestinations)
                  }
                />
              }
              label="Filtrer par destinations disponibles actuellement"
              labelPlacement="end"
            />
          </FormControl>
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default Filter;
