import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import AddIcon from "@material-ui/icons/Add";
import green from "@material-ui/core/colors/green";
import makeStyles from "@material-ui/core/styles/makeStyles";
import useEditor from "../../../../hooks/useEditor";
import FormInfo from "../../../../utils/editionRelated/FormInfo";
import { getLatestApiReadData } from "../../../../hooks/useGlobalState";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    // marginBottom: theme.spacing(2),
    flexGrow: 1,
  },
  button: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    boxShadow: "none",
    marginBottom: theme.spacing(1),
  },
}));

/**
 * Component for wrapping modules that consist of multiple similar stuff.
 *
 * It basically enables add feature.
 */
function ModuleGroupWrapper({
  formInfo,
  groupTitle,
  children,
  onSave,
  defaultModelData,
}) {
  const classes = useStyles();

  const openEditor = useEditor(formInfo, (somethingWasSaved) => {
    if (somethingWasSaved) onSave();
  });

  const userCanPostTo = getLatestApiReadData("userData-one").owner_can_post_to;
  const disabled = userCanPostTo.indexOf(formInfo.route) < 0;

  return (
    <Paper className={classes.root}>
      <Grid container spacing={1} alignItems="center">
        <Grid item xs>
          <Typography variant="h6" align="center" color="textSecondary">
            <em>{groupTitle}</em>
          </Typography>
        </Grid>
        <Grid item xs={2} style={{ textAlign: "right" }}>
          <Tooltip
            title={
              disabled
                ? "Vous ne pouvez pas ajouter d'élément"
                : "Ajouter un élément"
            }
            placement="top"
          >
            <div style={{ display: "inline-block" }}>
              &nbsp;
              {/* Needed to fire events for the tooltip when below is disabled! */}
              <Fab
                size="small"
                aria-label="Ajouter un élément"
                disabled={disabled}
                style={disabled ? {} : { backgroundColor: green.A700 }}
                className={classes.button}
                onClick={() => openEditor(defaultModelData)}
              >
                <AddIcon />
              </Fab>
            </div>
          </Tooltip>
        </Grid>
      </Grid>
      <Divider />
      {children}
    </Paper>
  );
}

ModuleGroupWrapper.propTypes = {
  groupTitle: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  formInfo: PropTypes.instanceOf(FormInfo).isRequired,
  defaultModelData: PropTypes.object.isRequired, // to populate the fields of the _form with default values
  onSave: PropTypes.func,
};

ModuleGroupWrapper.defaultProps = {
  onSave: () => {},
};

export default ModuleGroupWrapper;
