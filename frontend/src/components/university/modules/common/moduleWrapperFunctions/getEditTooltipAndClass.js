export default function getEditTooltipAndClass(userCanEdit, userCanModerate) {
  if (!userCanEdit) {
    return {
      editTooltip: "Vous ne pouvez pas modifier ce contenu.",
      editClass: "disabled",
    };
  }

  if (!userCanModerate) {
    return {
      editTooltip:
        "Vous pouvez éditer le contenu de ce module, mais votre contribution sera assujettie à la modération.",
      editClass: "orange",
    };
  }
  return {
    editTooltip: "Vous pouvez éditer le contenu de ce module.",
    editClass: "green",
  };
}
