import React from "react";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import dateTimeStrToStr from "../../../../../utils/dateTimeStrToStr";
import LinkToUser from "../../../../common/LinkToUser";

function UpdateInfo({ rawModelData }) {
  const { updated_by, updated_on } = rawModelData;
  const updaterId = updated_by.user_id;
  const updaterPseudo = updated_by.user_goes_by;
  const { user_can_edit: userCanEdit } = rawModelData.obj_info;

  if (!userCanEdit) {
    return (
      <Typography variant="caption">
        Vous n'avez pas le droit de modifier ce module.
      </Typography>
    );
  }
  const info = <LinkToUser userId={updaterId} pseudo={updaterPseudo} />;

  if (updated_on) {
    const { date, time } = dateTimeStrToStr(updated_on);
    return (
      <Typography variant="caption">
        Mis à jour par&nbsp;{info}&nbsp;le&nbsp;{date}&nbsp;à&nbsp;{time}
      </Typography>
    );
  }
  return (
    <Typography variant="caption">
      Mis à jour par&nbsp;{info}&nbsp;à une date inconnue.
    </Typography>
  );
}

UpdateInfo.propTypes = {
  rawModelData: PropTypes.object.isRequired,
};

export default React.memo(UpdateInfo);
