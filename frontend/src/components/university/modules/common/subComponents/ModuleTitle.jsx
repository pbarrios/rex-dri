import React from "react";
import NotificationImportantIcon from "@material-ui/icons/NotificationImportant";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import green from "@material-ui/core/colors/green";
import orange from "@material-ui/core/colors/orange";
import red from "@material-ui/core/colors/red";
import PropTypes from "prop-types";
import classNames from "../../../../../utils/classNames";

const useStyles = makeStyles((theme) => ({
  titleContainer: {
    display: "flex",
    alignItems: "center",
  },
  titleIcon: {
    paddingRight: theme.spacing(1),
    ...theme.typography.h4,
  },
  green: {
    color: green.A700,
  },
  orange: {
    color: orange.A700,
  },
  red: {
    color: red.A700,
  },
}));

function ModuleTitle({ rawModelData, buildTitle, Icon }) {
  const classes = useStyles();

  const importanceLevel = rawModelData.importance_level;
  const title = buildTitle(rawModelData);

  if (title) {
    if (Icon) {
      return (
        <div className={classes.titleContainer}>
          <Typography variant="h4">
            <Icon color="action" />
            &nbsp;
            {title}
          </Typography>
        </div>
      );
    }
    if (importanceLevel && importanceLevel !== "-") {
      let c = classNames(classes.titleIcon);
      if (importanceLevel === "++") {
        c = classNames(classes.titleIcon, classes.red);
      } else if (importanceLevel === "+") {
        c = classNames(classes.titleIcon, classes.orange);
      }
      return (
        <div className={classes.titleContainer}>
          <NotificationImportantIcon className={c} />
          <Typography variant="h4">{title}</Typography>
        </div>
      );
    }
    return <Typography variant="h4">{title}</Typography>;
  }
  return <></>;
}

ModuleTitle.propTypes = {
  rawModelData: PropTypes.object.isRequired,
  buildTitle: PropTypes.func.isRequired,
  Icon: PropTypes.object,
};

ModuleTitle.defaultProps = {
  Icon: undefined,
};

export default React.memo(ModuleTitle);
