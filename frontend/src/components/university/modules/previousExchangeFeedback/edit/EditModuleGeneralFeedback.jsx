import React from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import GeneralFeedbackForm from "./GeneralFeedbackForm";
import RequestParams from "../../../../../utils/api/RequestParams";
import ModuleWrapper from "../../common/ModuleWrapper";
import { GeneralFeedbackCore } from "../PreviousExchange";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../../../hoc/withNetworkWrapper";

function CoreComponent({ rawModelData }) {
  const {
    academical_level_appreciation,
    foreign_student_welcome,
    cultural_interest,
    general_comment,
    untouched,
  } = rawModelData;

  return (
    <GeneralFeedbackCore
      academicalLevel={academical_level_appreciation}
      foreignStudentWelcome={foreign_student_welcome}
      culturalInterest={cultural_interest}
      generalComment={general_comment}
      untouched={untouched}
    />
  );
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    academical_level_appreciation: PropTypes.number,
    foreign_student_welcome: PropTypes.number,
    cultural_interest: PropTypes.number,
    general_comment: PropTypes.string,
    untouched: PropTypes.bool.isRequired,
  }).isRequired,
};

function EditModuleGeneralFeedback({ generalFeedback }) {
  return (
    <ModuleWrapper
      buildTitle={() => "Avis général"}
      rawModelData={generalFeedback}
      formInfo={GeneralFeedbackForm}
      CoreComponent={CoreComponent}
    />
  );
}

EditModuleGeneralFeedback.propTypes = {
  generalFeedback: PropTypes.object.isRequired,
};

const buildParams = (exchangeId) =>
  RequestParams.Builder.withId(exchangeId).build();

export default compose(
  withNetworkWrapper([
    new NetWrapParam("exchangeFeedbacks", "one", {
      addDataToProp: "generalFeedback",
      params: (props) => buildParams(props.exchangeId),
      propTypes: {
        exchangeId: PropTypes.number.isRequired,
      },
    }),
  ])
)(EditModuleGeneralFeedback);
