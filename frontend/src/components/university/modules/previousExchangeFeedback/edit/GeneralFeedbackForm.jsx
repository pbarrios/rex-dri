import React from "react";
import MarkdownField from "../../../../edition/fields/MarkdownField";
import NumberField from "../../../../edition/fields/NumberField";
import FormInfo from "../../../../../utils/editionRelated/FormInfo";

const defaultNumberProps = {
  minValue: -5,
  maxValue: 5,
  required: true,
};

function GeneralFeedbackForm() {
  return (
    <>
      <NumberField
        label={
          "Votre appréciation du niveau académique générale, comparé à l'UTC"
        }
        comment="Particulièrement moins bon (-5) < similaire (0) < un gros cran au-dessus (5)"
        fieldMapping="academical_level_appreciation"
        {...defaultNumberProps}
      />

      <NumberField
        label={
          "Votre appréciation de l'accueil réservé aux étudiants étranger (administration et vie étudiante)"
        }
        comment="Strictement rien de prévu (-5) < suffisant (0) < vraiment super (5)"
        fieldMapping="foreign_student_welcome"
        {...defaultNumberProps}
      />

      <NumberField
        label={"Votre appréciation de l'intérêt culturel de votre échanges"}
        comment="Il n'y a rien a faire (-5) < suffisant (0) < vraiment super (5)"
        fieldMapping="cultural_interest"
        {...defaultNumberProps}
      />

      <MarkdownField
        label="Commentaire générale sur votre échange"
        fieldMapping="general_comment"
        maxLength={1500}
        required
      />
    </>
  );
}

export default new FormInfo("exchangeFeedbacks", GeneralFeedbackForm);
