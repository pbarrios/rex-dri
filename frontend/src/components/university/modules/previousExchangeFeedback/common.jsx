import PropTypes from "prop-types";
import React from "react";
import { makeStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";

export const useStyle = makeStyles((theme) => ({
  paper: {
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
  },
  chip: {
    marginRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium,
  },
  expansionPanel: {
    margin: `${theme.spacing(1)}px auto`,
  },
  spacer: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  leftCol: {
    width: 150,
  },
  leftColExtra: {
    width: 150 + 60,
  },
  feedbackHeaderCell: {
    [theme.breakpoints.up("xs")]: {
      margin: theme.spacing(0, 2),
    },
  },
  feedbackHeaderText: {
    textAlign: "center",
  },
  editIcon: {
    padding: 0,
  },
}));

export function GridColumn(props) {
  const classes = useStyle();

  return (
    <Grid container direction="column" justify="flex-start" alignItems="center">
      {props.children.map((c, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <Grid key={idx} item className={classes.feedbackHeaderCell}>
          {c}
        </Grid>
      ))}
    </Grid>
  );
}

GridColumn.propTypes = {
  children: PropTypes.array.isRequired,
};

export function GridLine(props) {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        flexWrap: "wrap",
      }}
    >
      {props.children.map((c, idx) => (
        <div
          // eslint-disable-next-line react/no-array-index-key
          key={idx}
          style={{
            flexGrow: 1,
            flexShrink: 1,
            flexBasis: 0,
            minWidth: "fit-content",
          }}
        >
          {c}
        </div>
      ))}
    </div>
  );
}

GridLine.propTypes = {
  children: PropTypes.array.isRequired,
};
