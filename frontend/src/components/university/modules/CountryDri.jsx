import React, { useMemo } from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";

import Markdown from "../../common/markdown/Markdown";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import CountryDriForm from "../forms/CountryDriForm";
import withUnivInfo from "../../../hoc/withUnivInfo";
import RequestParams from "../../../utils/api/RequestParams";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

function CoreComponent({ rawModelData }) {
  const { comment } = rawModelData;
  return <Markdown source={comment} />;
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
  }),
};

CoreComponent.defaultProps = {
  rawModelData: { comment: "" },
};

function CountryDri({ countryId, countryDriItems }) {
  const defaultModelData = useMemo(
    () => ({
      countries: [countryId],
      importance_level: "-",
    }),
    [countryId]
  );
  return (
    <ModuleGroupWrapper
      groupTitle="Informations émanant de la DRI liées au pays"
      formInfo={CountryDriForm}
      defaultModelData={defaultModelData}
    >
      {countryDriItems.map((rawModelData, idx) => (
        <ModuleWrapper
          // eslint-disable-next-line react/no-array-index-key
          key={idx}
          buildTitle={(modelData) => modelData.title}
          rawModelData={rawModelData}
          formInfo={CountryDriForm}
          CoreComponent={CoreComponent}
        />
      ))}
    </ModuleGroupWrapper>
  );
}

CountryDri.propTypes = {
  countryId: PropTypes.string.isRequired,
  countryDriItems: PropTypes.array.isRequired,
};

const buildParams = (countryId) =>
  RequestParams.Builder.withQueryParam("countries", countryId).build();

export default compose(
  withUnivInfo(["countryId"]),
  withNetworkWrapper([
    new NetWrapParam("countryDri", "all", {
      addDataToProp: "countryDriItems",
      params: (props) => buildParams(props.countryId),
      propTypes: {
        countryId: PropTypes.string.isRequired,
      },
    }),
  ])
)(CountryDri);
