import React from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import CloudQueueIcon from "@material-ui/icons/CloudQueue";
import LocalFloristIcon from "@material-ui/icons/LocalFlorist";
import { makeStyles } from "@material-ui/styles";
import Markdown from "../../common/markdown/Markdown";

import ModuleWrapper from "./common/ModuleWrapper";

import dateStrToStr from "../../../utils/dateStrToStr";

import UniversitySemestersDatesForm from "../forms/UniversitySemestersDatesForm";
import withUnivInfo from "../../../hoc/withUnivInfo";
import RequestParams from "../../../utils/api/RequestParams";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    overflowX: "auto",
  },
  tableCell: {
    padding: "2px",
  },
  content: {
    display: "flex",
    alignItems: "center",
  },
  icon: {
    paddingRight: theme.spacing(1),
  },
}));

function convertDateStrToStr(date) {
  if (date) {
    return dateStrToStr(date);
  }
  return "Non connue.";
}

function CoreComponent({ rawModelData }) {
  const classes = useStyles();

  const { comment } = rawModelData;
  let { autumn_begin, autumn_end, spring_begin, spring_end } = rawModelData;

  autumn_begin = convertDateStrToStr(autumn_begin);
  autumn_end = convertDateStrToStr(autumn_end);
  spring_begin = convertDateStrToStr(spring_begin);
  spring_end = convertDateStrToStr(spring_end);

  return (
    <>
      <div className={classes.root}>
        <Table>
          <TableHead>
            <TableRow key={0}>
              <TableCell className={classes.tableCell} component="th">
                &nbsp;Semestre&nbsp;
              </TableCell>
              <TableCell className={classes.tableCell} component="th">
                &nbsp;Date de début&nbsp;
              </TableCell>
              <TableCell className={classes.tableCell} component="th">
                &nbsp;Date de fin&nbsp;
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow key={1}>
              <TableCell className={classes.tableCell} component="td">
                <div className={classes.content}>
                  <LocalFloristIcon color="disabled" className={classes.icon} />
                  <div>Printemps</div>
                </div>
              </TableCell>
              <TableCell className={classes.tableCell} component="td">
                {spring_begin}
              </TableCell>
              <TableCell className={classes.tableCell} component="td">
                {spring_end}
              </TableCell>
            </TableRow>

            <TableRow key={2}>
              <TableCell className={classes.tableCell} component="td">
                <div className={classes.content}>
                  <CloudQueueIcon color="disabled" className={classes.icon} />
                  <div>Automne</div>
                </div>
              </TableCell>
              <TableCell className={classes.tableCell} component="td">
                {autumn_begin}
              </TableCell>
              <TableCell className={classes.tableCell} component="td">
                {autumn_end}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
      <Markdown source={comment} />
    </>
  );
}

CoreComponent.propTypes = {
  rawModelData: PropTypes.shape({
    comment: PropTypes.string,
    autumn_begin: PropTypes.string,
    autumn_end: PropTypes.string,
    spring_begin: PropTypes.string,
    spring_end: PropTypes.string,
  }).isRequired,
};

function UniversitySemestersDates({ semestersDates }) {
  return (
    <ModuleWrapper
      buildTitle={() => "Date des semestres"}
      rawModelData={semestersDates}
      formInfo={UniversitySemestersDatesForm}
      CoreComponent={CoreComponent}
    />
  );
}

UniversitySemestersDates.propTypes = {
  semestersDates: PropTypes.object.isRequired,
};

const buildParams = (univId) => RequestParams.Builder.withId(univId).build();

export default compose(
  withUnivInfo(),
  withNetworkWrapper([
    new NetWrapParam("universitiesSemestersDates", "one", {
      addDataToProp: "semestersDates",
      params: (props) => buildParams(props.univId),
      propTypes: {
        univId: PropTypes.number.isRequired,
      },
    }),
  ])
)(UniversitySemestersDates);
