import React from "react";
import PropTypes from "prop-types";
import FormLevelError from "../../../../utils/editionRelated/FormLevelError";
import NumberField from "../../../edition/fields/NumberField";
import TextField from "../../../edition/fields/TextField";
import SelectField from "../../../edition/fields/SelectField";
import MarkdownField from "../../../edition/fields/MarkdownField";
import {
  CommentField,
  CurrencyField,
  TitleField,
} from "../../../edition/fields/wrappedFields";
import UsefulLinksField from "../../../edition/fields/UsefulLinksField";

const frequencyOptions = [
  {
    value: "w",
    label: "Il s'agit du montant hebdomadaire",
  },
  {
    value: "m",
    label: "Il s'agit du montant mensuel",
  },
  {
    value: "s",
    label: "Il s'agit du montant semestriel",
  },
  {
    value: "y",
    label: "Il s'agit du montant annuel",
  },
  {
    value: "o",
    label: "Il s'agit d'un montant donné une seule fois",
  },
];

export function ScholarshipFields({ LinkedField }) {
  return (
    <>
      <TitleField />
      <LinkedField />
      <TextField
        fieldMapping="short_description"
        maxLength={200}
        label="Présentation succincte"
        required
      />
      <CurrencyField />
      <NumberField
        fieldMapping="amount_min"
        label="Borne inférieure du montant de la bourse"
        minValue={0}
      />
      <NumberField
        fieldMapping="amount_max"
        label="Borne supérieure du montant de la bourse"
        minValue={0}
      />
      <SelectField
        label="Fréquence de la bourse"
        options={frequencyOptions}
        fieldMapping="frequency"
      />
      <MarkdownField
        fieldMapping="other_advantages"
        maxLength={500}
        label="Autres avantages"
      />
      <CommentField />
      <UsefulLinksField />
    </>
  );
}

ScholarshipFields.propTypes = {
  LinkedField: PropTypes.func.isRequired,
};

export const scholarshipFormLevelErrors = [
  new FormLevelError(
    ["amount_min", "amount_max"],
    (amountMin, amountMax) =>
      amountMin !== "" &&
      amountMax !== "" &&
      amountMin !== null &&
      amountMax !== null &&
      amountMax < amountMin,
    "La logique voudrait que la borne supérieure de la bourse soit... supérieure à la borne inférieure."
  ),
  new FormLevelError(
    ["currency", "amount_min", "amount_max"],
    (currency, amountMin, amountMax) =>
      (amountMin || amountMax || amountMin === 0 || amountMax === 0) &&
      !currency,
    "Veuillez saisir la devise associée."
  ),
];
