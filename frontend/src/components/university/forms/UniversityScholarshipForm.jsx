import React from "react";
import {
  ScholarshipFields,
  scholarshipFormLevelErrors,
} from "./common/Scholarship";
import { UniversitiesField } from "../../edition/fields/wrappedFields";
import HiddenField from "../../edition/fields/HiddenField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

function UniversityScholarshipForm() {
  return (
    <>
      <HiddenField fieldMapping="university" />
      <ScholarshipFields LinkedField={UniversitiesField} />
    </>
  );
}

export default new FormInfo(
  "universityScholarships",
  UniversityScholarshipForm,
  scholarshipFormLevelErrors
);
