import React from "react";
import HiddenField from "../../edition/fields/HiddenField";
import UsefulLinksField from "../../edition/fields/UsefulLinksField";
import MarkdownField from "../../edition/fields/MarkdownField";
import FormInfo from "../../../utils/editionRelated/FormInfo";

function TaggedItemForm() {
  return (
    <>
      <MarkdownField
        fieldMapping="comment"
        label="Commentaire"
        maxLength={5000}
      />
      <UsefulLinksField />
      <HiddenField fieldMapping="tag" />
      {/* Small hack by probiding two hidden fields, but will work with our API */}
      <HiddenField fieldMapping="country" />
      <HiddenField fieldMapping="university" />
    </>
  );
}

export const CountryTaggedItemForm = new FormInfo(
  "countryTaggedItems",
  TaggedItemForm
);

export const UniversityTaggedItemForm = new FormInfo(
  "universityTaggedItems",
  TaggedItemForm
);
