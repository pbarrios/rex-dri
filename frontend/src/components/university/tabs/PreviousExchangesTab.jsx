/* eslint-disable no-shadow */
import React, { useEffect, useState, useCallback } from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import { v4 as uuidv4 } from "uuid";
import PreviousExchange from "../modules/previousExchangeFeedback/PreviousExchange";
import PaginatedData from "../../common/PaginatedData";
import RequestParams from "../../../utils/api/RequestParams";
import withUnivInfo from "../../../hoc/withUnivInfo";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";
import {
  getAllMajors,
  getAllMinors,
  getUpdatedUnivMajorMinors,
} from "../../../utils/majorMinorMappings";

const undefinedVal = uuidv4();

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 150,
  },
}));

/**
 * Tab on the university page containing information related to previous exchange
 */
function PreviousExchangesTab({
  exchanges,
  major,
  minor,
  setMajor,
  setMinor,
  goToPage,
  univMajorMinors,
  showUntouched,
  setShowUntouched,
  hasFilters,
  resetFilters,
}) {
  const classes = useStyles();

  const univMajorMinorsUpdated = getUpdatedUnivMajorMinors(univMajorMinors);

  const displayMinorSelect = major !== undefinedVal;
  let minors;
  if (displayMinorSelect) {
    minors = univMajorMinorsUpdated.find((el) => el.major === major).minors;
  }

  return (
    <>
      <form autoComplete="off" className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="major-select">Branche</InputLabel>
          <Select
            value={major}
            onChange={(e) => setMajor(e.target.value)}
            inputProps={{
              id: "major-select",
            }}
          >
            <MenuItem value={undefinedVal}>Pas de filtre</MenuItem>
            {univMajorMinorsUpdated
              .filter((el) => el.major !== null)
              .map((el) => (
                <MenuItem key={el.major} value={el.major}>
                  {el.major}
                </MenuItem>
              ))}
          </Select>
        </FormControl>

        {displayMinorSelect && (
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="minor-filière">Filière</InputLabel>
            <Select
              className={classes.select}
              value={minor}
              onChange={(e) => setMinor(e.target.value)}
              inputProps={{
                id: "minor-filière",
              }}
            >
              <MenuItem value={undefinedVal}>Pas de filtre</MenuItem>
              {minors
                .filter((el) => el !== null)
                .map((el) => (
                  <MenuItem key={el} value={el}>
                    {el}
                  </MenuItem>
                ))}
            </Select>
          </FormControl>
        )}

        <FormControl className={classes.formControl}>
          <FormControlLabel
            control={
              <Checkbox
                checked={!showUntouched}
                onChange={() => setShowUntouched(!showUntouched)}
              />
            }
            label="Avec feedbacks uniquement"
            labelPlacement="start"
          />
        </FormControl>

        {hasFilters && (
          <FormControl className={classes.formControl}>
            <Button
              onClick={resetFilters}
              variant="outlined"
              color="inherit"
              size="small"
            >
              Réinitialiser les filtres
            </Button>
          </FormControl>
        )}
      </form>

      <PaginatedData
        data={exchanges}
        goToPage={goToPage}
        render={(dataEl) => (
          <PreviousExchange key={dataEl.id} rawModelData={dataEl} />
        )}
        stepperOnBottom
        stepperOnTop
        EmptyMessageComponent={
          <Typography>
            <em>
              {hasFilters
                ? "Aucun échange ne correspond au(x) filtre(s)."
                : "Aucun échange n'est à ce jour répertorié pour cette université."}
            </em>
          </Typography>
        }
      />
    </>
  );
}

PreviousExchangesTab.propTypes = {
  exchanges: PropTypes.object.isRequired,
  univMajorMinors: PropTypes.array.isRequired,
  major: PropTypes.string.isRequired,
  minor: PropTypes.string.isRequired,
  goToPage: PropTypes.func.isRequired,
  setMinor: PropTypes.func.isRequired,
  setMajor: PropTypes.func.isRequired,
  resetFilters: PropTypes.func.isRequired,
  hasFilters: PropTypes.bool.isRequired,
  showUntouched: PropTypes.bool.isRequired,
  setShowUntouched: PropTypes.func.isRequired,
};

const buildExchangesParams = (
  univId,
  page = 1,
  major = undefinedVal,
  minor = undefinedVal,
  showUntouched = true
) => {
  const params = RequestParams.Builder.withQueryParam("university", univId)
    .withQueryParam("page_size", 8)
    .withQueryParam("page", page);
  if (typeof major !== "undefined" && major !== undefinedVal) {
    const finalMajors = getAllMajors(major);
    params.withQueryParam("exchange__student_major_in", finalMajors.join(","));
  }
  if (typeof minor !== "undefined" && minor !== undefinedVal) {
    const finalMinors = getAllMinors(major, minor);
    params.withQueryParam("exchange__student_minor_in", finalMinors.join(","));
  }
  if (showUntouched === false) params.withQueryParam("untouched", false);
  return params.build();
};

const buildUnivMajorMinorsParams = (univId) =>
  RequestParams.Builder.withQueryParam("university", univId).build();

const ConnectedComp = compose(
  withUnivInfo(),
  withNetworkWrapper([
    new NetWrapParam("exchangeFeedbacks", "all", {
      addDataToProp: "exchanges",
      params: (props) =>
        buildExchangesParams(
          props.univId,
          props.page,
          props.major,
          props.minor,
          props.showUntouched
        ),
      propTypes: {
        univId: PropTypes.number.isRequired,
        page: PropTypes.number.isRequired,
        major: PropTypes.string.isRequired,
        minor: PropTypes.string.isRequired,
        showUntouched: PropTypes.bool.isRequired,
      },
    }),
    new NetWrapParam("univMajorMinors", "all", {
      addDataToProp: "univMajorMinors",
      params: (props) => buildUnivMajorMinorsParams(props.univId),
      propTypes: {
        univId: PropTypes.number.isRequired,
      },
    }),
  ])
)(PreviousExchangesTab);

export default () => {
  // Lifting state up to work around NetworkWrapper limitations
  const [page, goToPage] = useState(1);

  const [major, setMajor] = useState(undefinedVal);
  const [minor, setMinor] = useState(undefinedVal);
  const [showUntouched, setShowUntouched] = useState(true);

  const resetFilters = useCallback(() => {
    setMajor(undefinedVal);
    setMinor(undefinedVal);
    setShowUntouched(true);
  }, []);

  // Reset minors on majors change
  useEffect(() => {
    setMinor(undefinedVal);
  }, [major]);

  useEffect(() => {
    goToPage(1);
  }, [major, minor, showUntouched]);

  const hasFilters =
    major !== undefinedVal || minor !== undefinedVal || showUntouched === false;

  return (
    <ConnectedComp
      page={page}
      goToPage={goToPage}
      major={major}
      setMajor={setMajor}
      minor={minor}
      setMinor={setMinor}
      showUntouched={showUntouched}
      setShowUntouched={setShowUntouched}
      resetFilters={resetFilters}
      hasFilters={hasFilters}
    />
  );
};
