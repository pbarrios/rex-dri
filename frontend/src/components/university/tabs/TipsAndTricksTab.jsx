import React, { useMemo } from "react";
import compose from "recompose/compose";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import TrainIcon from "@material-ui/icons/Train";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import BeachAccessIcon from "@material-ui/icons/BeachAccess";
import HomeIcon from "@material-ui/icons/Home";
import GavelIcon from "@material-ui/icons/Gavel";
import AllInclusiveIcon from "@material-ui/icons/AllInclusive";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import DirectionsRunIcon from "@material-ui/icons/DirectionsRun";
import { makeStyles } from "@material-ui/styles";
import TaggedItem from "../modules/TaggedItem";
import RequestParams from "../../../utils/api/RequestParams";
import withUnivInfo from "../../../hoc/withUnivInfo";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

const UNIVERSITY_TAGS = [
  ["accommodation", "Logement", HomeIcon],
  ["student_life", "Vie étudiante", DirectionsRunIcon],
  ["transport", "Transport", TrainIcon],
  ["administrative", "Administration", AccountBalanceIcon],
  ["other", "Autre chose ?", MoreHorizIcon],
];

const COUNTRY_TAGS = [
  ["immigration", "Immigration & VISA", GavelIcon],
  ["administrative", "Administration", AccountBalanceIcon],
  ["culture", "Culture", AllInclusiveIcon],
  ["tourism", "Tourisme", BeachAccessIcon],
  ["transport", "Transport", TrainIcon],
  ["other", "Autre chose ?", MoreHorizIcon],
];

const useStyles = makeStyles((theme) => ({
  chip: {
    margin: theme.spacing(1),
  },
  item: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Tab containing tips and tricks about the university and the country
 *
 * BUGGY SINCE SWITCH TO hooks
 */
function TipsAndTricksTab({ country, univ, countryTI, universityTI }) {
  const classes = useStyles();

  const countryItems = useMemo(() => {
    return COUNTRY_TAGS.map(([tagName, title, Icon]) => ({
      title,
      Icon,
      data: countryTI.find((el) => el.tag === tagName),
    })).filter((el) => typeof el.data !== "undefined");
  }, [countryTI]);

  const universityItems = useMemo(
    () =>
      UNIVERSITY_TAGS.map(([tagName, title, Icon]) => ({
        title,
        Icon,
        data: universityTI.find((el) => el.tag === tagName),
      })).filter((el) => typeof el.data !== "undefined"),
    [universityTI]
  );

  return (
    <>
      <Grid container spacing={3}>
        <Grid item md={6} xs={12}>
          <Chip
            label={<b>{univ.name.toUpperCase()}</b>}
            className={classes.chip}
            color="primary"
          />
          {universityItems.map(({ data, title, Icon }) => (
            <div key={data.id} className={classes.item}>
              <TaggedItem
                data={data}
                title={title}
                variant="university"
                Icon={Icon}
                invalidateGroup={() => {}}
              />
            </div>
          ))}
        </Grid>
        <Grid item md={6} xs={12}>
          <Chip
            label={<b>{country.name.toUpperCase()}</b>}
            className={classes.chip}
            color="secondary"
          />
          {countryItems.map(({ data, title, Icon }) => (
            <div key={data.id} className={classes.item}>
              <TaggedItem
                data={data}
                title={title}
                variant="country"
                Icon={Icon}
                invalidateGroup={() => {}}
              />
            </div>
          ))}
        </Grid>
      </Grid>
    </>
  );
}

TipsAndTricksTab.propTypes = {
  country: PropTypes.object.isRequired,
  univ: PropTypes.object.isRequired,
  countryTI: PropTypes.array.isRequired,
  universityTI: PropTypes.array.isRequired,
};

const buildParamsCountryTI = (countryId) =>
  RequestParams.Builder.withQueryParam("country", countryId).build();

const buildParamsUniversityTI = (univId) =>
  RequestParams.Builder.withQueryParam("university", univId).build();

export default compose(
  withUnivInfo(["countryId", "univId", "country", "univ"]),
  withNetworkWrapper([
    new NetWrapParam("countryTaggedItems", "all", {
      addDataToProp: "countryTI",
      params: (props) => buildParamsCountryTI(props.countryId),
      propTypes: {
        countryId: PropTypes.string.isRequired,
      },
    }),
    new NetWrapParam("universityTaggedItems", "all", {
      addDataToProp: "universityTI",
      params: (props) => buildParamsUniversityTI(props.univId),
      propTypes: {
        univId: PropTypes.number.isRequired,
      },
    }),
  ])
)(TipsAndTricksTab);
