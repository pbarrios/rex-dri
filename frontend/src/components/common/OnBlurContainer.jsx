import React, { useCallback } from "react";
import PropTypes from "prop-types";

/**
 * Component to trigger onBlur only if the blurred element is outside of the children:
 *
 * inspired by: https://gist.github.com/pstoica/4323d3e6e37e8a23dd59
 * @param props
 * @constructor
 */
export default function OnBlurContainer(props) {
  const onBlur = useCallback((e) => {
    const { currentTarget } = e;

    setTimeout(() => {
      if (!currentTarget.contains(document.activeElement)) {
        props.onBlur();
      }
    }, 0);
  });

  return <div onBlur={onBlur}>{props.children}</div>;
}

OnBlurContainer.propTypes = {
  children: PropTypes.node.isRequired,
  onBlur: PropTypes.func.isRequired,
};
