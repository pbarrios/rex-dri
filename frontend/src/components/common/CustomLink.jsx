import { Link } from "react-router-dom";
import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { styles } from "./TextLink";

/**
 * Renders an app internal link with correct styling.
 */
function CustomLink({ classes, to, children }) {
  return (
    <Link to={to} className={classes.link}>
      {children}
    </Link>
  );
}

CustomLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(CustomLink);
