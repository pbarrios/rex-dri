import React from "react";
import PropTypes from "prop-types";
import CustomLink from "./CustomLink";
import APP_ROUTES from "../../config/appRoutes";

export default function LinkToUser(props) {
  if (props.userId) {
    return (
      <CustomLink to={APP_ROUTES.forUser(props.userId)}>
        <em>
          {props.pseudo}&nbsp;(#{props.userId})
        </em>
      </CustomLink>
    );
  }
  return "(utilisateur inconnu)";
}

LinkToUser.propTypes = {
  userId: PropTypes.number,
  pseudo: PropTypes.string,
};
