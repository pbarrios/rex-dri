import React from "react";
import PropTypes from "prop-types";
import ThumbsUp from "@material-ui/icons/ThumbUp";
import ThumbsDown from "@material-ui/icons/ThumbDown";
import Close from "@material-ui/icons/Close";
import Warning from "@material-ui/icons/Warning";
import Done from "@material-ui/icons/Done";
import DoneAll from "@material-ui/icons/DoneAll";
import Favorite from "@material-ui/icons/Favorite";
import { makeStyles } from "@material-ui/styles";
import classNames from "../../utils/classNames";
import { getGradient, Spectral, viridis } from "../../utils/colormaps";

const possibleIcons = [Close, Warning, Done, DoneAll, Favorite];
const breakPoints = [20, 40, 60, 80, 100];
const possibleIconsOpposite = [
  Close,
  Warning,
  Done,
  DoneAll,
  Favorite,
  DoneAll,
  Done,
  Warning,
  Close,
];
const breakPointsOpposite = [11, 22, 33, 44, 55, 66, 77, 88, 100];

const indicatorWidth = 14;
const indicatorExtraHeight = 10;

const useStyles = makeStyles((theme) => ({
  barContainer: {
    flexWrap: "nowrap",
    display: "flex",
    alignItems: "center",
    paddingTop: 25,
    paddingBottom: 10,
  },
  barContent: {},
  linearBackground: {
    background: getGradient([...viridis].reverse()),
  },
  divergentBackground: {
    background: getGradient(Spectral),
  },
  oppositeBackground: {
    background: getGradient([...Spectral, ...[...Spectral].reverse()]),
  },
  colorBar: {
    height: (props) => props.height,
    borderRadius: (props) => props.height / 2,
    marginLeft: 10,
    marginRight: 10,
  },
  indicator: {
    height: (props) => props.height + indicatorExtraHeight,
    width: indicatorWidth,
    top: -(indicatorExtraHeight / 2),
    borderRadius: indicatorExtraHeight,
    position: "relative",
    background: theme.palette.background.default,
    borderStyle: "solid",
    borderColor: theme.palette.text.primary,
  },
  indicatorIcon: {
    left: "50%",
    transform: "translateX(-50%)",
    position: "relative",
    top: -29,
  },
}));

/**
 *
 * @param {number} percent
 * @param {array} breakPointsToUse
 * @param {array<node>} possibleIconsToUse
 * @returns {node}
 */
function getIcon(
  percent,
  breakPointsToUse = breakPoints,
  possibleIconsToUse = possibleIcons
) {
  let i = 0;

  // eslint-disable-next-line no-restricted-syntax
  for (const p of breakPointsToUse) {
    if (percent <= p) break;
    i += 1;
  }

  return possibleIconsToUse[i];
}

function getIconOpposite(percent) {
  return getIcon(percent, breakPointsOpposite, possibleIconsOpposite);
}

function MetricFeedback(props) {
  const classes = useStyles(props);
  const {
    min,
    max,
    value,
    width,
    showBarIcons,
    type,
    LeftBarIcon,
    RightBarIcon,
  } = props;
  const minValue = parseFloat(min);
  const maxValue = parseFloat(max);
  const percent =
    (100 * Math.abs(minValue - value)) / Math.abs(maxValue - minValue);
  const Icon =
    type === "two-negatives" ? getIconOpposite(percent) : getIcon(percent);

  let backgroundClass = "";
  switch (type) {
    case "divergent":
      backgroundClass = classes.divergentBackground;
      break;
    case "linear":
      backgroundClass = classes.linearBackground;
      break;
    case "two-negatives":
      backgroundClass = classes.oppositeBackground;
      break;
    default:
      break;
  }

  const colorBarClasses = classNames(classes.colorBar, backgroundClass);

  return (
    <>
      <div className={classes.barContainer}>
        {showBarIcons && <LeftBarIcon color="disabled" />}
        <div className={classes.barContent}>
          <div className={colorBarClasses} style={{ width }}>
            <div
              className={classes.indicator}
              style={{
                left: `${percent}%`,
                transform: `translateX(-${percent}%)`,
              }}
            >
              <Icon color="secondary" className={classes.indicatorIcon} />
            </div>
          </div>
        </div>
        {showBarIcons && <RightBarIcon color="disabled" />}
      </div>
    </>
  );
}

MetricFeedback.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  type: PropTypes.oneOf(["divergent", "linear", "two-negatives"]),
  min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  // eslint-disable-next-line react/no-unused-prop-types
  height: PropTypes.number,
  value: PropTypes.number,
  showBarIcons: PropTypes.bool,
  LeftBarIcon: PropTypes.object,
  RightBarIcon: PropTypes.object,
};

MetricFeedback.defaultProps = {
  type: "divergent",
  min: "-5",
  max: "+5",
  value: 0,
  width: 150,
  height: 20,
  showBarIcons: false,
  LeftBarIcon: ThumbsDown,
  RightBarIcon: ThumbsUp,
};

export default MetricFeedback;
