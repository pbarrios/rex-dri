import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import React from "react";
import defaultTheme from "../../../config/defaultTheme.json";
import { getTheme, updatePhoneStatusBarColor } from "./utils";

/**
 * Base Theme provider without connecting to the API
 * @constructor
 */
export default function OfflineThemeProvider(props) {
  const theme = getTheme(defaultTheme);
  updatePhoneStatusBarColor(theme.palette.primary.main);

  return <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>;
}

OfflineThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
