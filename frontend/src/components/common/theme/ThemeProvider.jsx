import React, { useEffect } from "react";
import PropTypes from "prop-types";

import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import { getTheme, updatePhoneStatusBarColor } from "./utils";
import RequestParams from "../../../utils/api/RequestParams";
import { CURRENT_USER_ID } from "../../../config/user";
import withNetworkWrapper, {
  NetWrapParam,
} from "../../../hoc/withNetworkWrapper";

/**
 * Component that handles theme customization and saving at the scale of the entire app
 */
function ThemeProvider({ children, userData }) {
  const themeData = userData.theme;
  const theme = getTheme(themeData);

  useEffect(() => {
    updatePhoneStatusBarColor(theme.palette.primary.main);
  }, [themeData]);

  return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}

ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
  userData: PropTypes.shape({
    theme: PropTypes.object.isRequired,
  }).isRequired,
};

export default withNetworkWrapper([
  new NetWrapParam("userData", "one", {
    addDataToProp: "userData",
    params: RequestParams.Builder.withId(CURRENT_USER_ID).build(),
  }),
])(ThemeProvider);
