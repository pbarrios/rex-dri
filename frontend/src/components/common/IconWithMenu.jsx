import React, { useCallback, useState } from "react";
import Menu from "@material-ui/core/Menu";
import PropTypes from "prop-types";
import Fab from "@material-ui/core/Fab";
import { MenuItem } from "@material-ui/core";
import { NavLink } from "react-router-dom";

/**
 * There were errors during migrations to material-ui v4 as Navlink couldn't hold a ref...
 */
// eslint-disable-next-line react/display-name
const ForwardedNavLink = React.forwardRef((props, ref) => (
  <div ref={ref}>
    <NavLink {...props} />
  </div>
));

function IconWithMenu({ fabProps, Icon, menuItems, iconProps }) {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleOpenMenu = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleCloseMenu = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const open = anchorEl !== null;
  return (
    <>
      <Fab size="medium" color="inherit" onClick={handleOpenMenu} {...fabProps}>
        <Icon {...iconProps} />
      </Fab>
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={open}
        onClose={handleCloseMenu}
      >
        {menuItems.map(({ label, route, hardRedirect }, idx) =>
          hardRedirect ? (
            <MenuItem
              // eslint-disable-next-line react/no-array-index-key
              key={idx}
              component="a"
              href={route}
              onClick={handleCloseMenu}
            >
              {label}
            </MenuItem>
          ) : (
            <MenuItem
              // eslint-disable-next-line react/no-array-index-key
              key={idx}
              component={ForwardedNavLink}
              to={route}
              onClick={handleCloseMenu}
            >
              {label}
            </MenuItem>
          )
        )}
      </Menu>
    </>
  );
}

IconWithMenu.defaultProps = {
  fabProps: {},
  iconProps: {},
};

IconWithMenu.propTypes = {
  Icon: PropTypes.object.isRequired, // should be a react component
  menuItems: PropTypes.array.isRequired, // should be an array of menu items
  fabProps: PropTypes.object,
  iconProps: PropTypes.object,
};

export default React.memo(IconWithMenu);
