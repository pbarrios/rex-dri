import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";

export function getLinkColor(theme) {
  return theme.palette.type === "dark"
    ? theme.palette.secondary.dark
    : theme.palette.secondary.light;
}

export const styles = (theme) => ({
  link: {
    color: theme.palette.text.primary,
    textDecoration: `underline ${getLinkColor(theme)}`,
    // boxShadow: `0px 0.08em 0.01em -0.01em ${getLinkColor(
    //   theme
    // )}, inset 0px -0.1em 0.03em -0.03em ${getLinkColor(theme)}`,
    // borderTopRightRadius: "100%",
    // borderTopLeftRadius: "100%",
    // borderBottomLeftRadius: "10%",
    // borderBottomRightRadius: "30%"
  },
});

const useStyles = makeStyles(styles);

/**
 * Component to render the links in custom manner
 *
 */
function TextLink({ href, children }) {
  const classes = useStyles();
  return (
    <a
      href={href}
      className={classes.link}
      target="_blank"
      rel="noopener noreferrer"
    >
      {children}
    </a>
  );
}

TextLink.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default TextLink;
