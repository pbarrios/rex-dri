import React, { useState } from "react";
import PropTypes from "prop-types";
import { compose } from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";
import Markdown from "./Markdown";

function TruncatedMarkdown(props) {
  const [truncated, setTruncated] = useState(false);
  const { source, truncateFromLength, classes } = props;
  const sourceAsString = source === null ? "" : source;
  const truncatable = sourceAsString.length > 3 * truncateFromLength;

  if (!truncatable) {
    return <Markdown source={sourceAsString} />;
  }

  return (
    <>
      <Collapse in={truncated} collapsedHeight="4rem">
        <Markdown source={sourceAsString} />
      </Collapse>
      {!truncated && <div className={classes.gradientBorder} />}
      <Button
        variant="outlined"
        className={classes.moreButton}
        onClick={() => setTruncated(!truncated)}
        size="small"
      >
        {!truncated ? "En lire plus..." : "En lire moins..."}
      </Button>
    </>
  );
}

TruncatedMarkdown.propTypes = {
  classes: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
  truncateFromLength: PropTypes.number,
};

TruncatedMarkdown.defaultProps = {
  truncateFromLength: 100,
};

const styles = (theme) => ({
  truncated: {
    maxHeight: "6rem",
    overflow: "hidden",
  },
  gradientBorder: {
    position: "relative",
    marginTop: "-2rem",
    bottom: 0,
    width: "100%",
    height: "3rem",
    background: `linear-gradient(to bottom, transparent, ${theme.palette.background.paper})`,
  },
  moreButton: {
    display: "block",
    margin: "0 auto",
  },
});

export default compose(withStyles(styles))(TruncatedMarkdown);
