/**
 * Class to handle app errors (such as _form errors) in a nice wrapped way
 * It's for non fatal errors.
 */
export default class CustomError {
  /**
   *Creates an instance of CustomError.
   *
   * @param {Array[string] = []} messages
   */
  constructor(messages = []) {
    this.messages = messages;
    this.status = messages.length > 0;
  }

  /**
   * Combines an array of CustomError
   *
   * @static
   * @param {Array[CustomError]} arrOfCustomErrors
   * @returns
   */
  static superCombine(arrOfCustomErrors) {
    return new CustomError(
      Array.prototype.concat(arrOfCustomErrors.flatMap((el) => el.messages))
    );
  }

  /**
   * Method to combine to error class
   *
   * @param {CustomError} other
   * @returns {CustomError}
   */
  combine(other) {
    return new CustomError(
      Array.prototype.concat(this.messages, other.messages)
    );
  }
}
