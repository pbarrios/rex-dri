import React from "react";
import PropTypes from "prop-types";
import { compose } from "recompose";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import RequestParams from "../../utils/api/RequestParams";
import List from "./view/View";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import NavigationService from "../../services/NavigationService";

/**
 * Component to display one recommendation list
 */
function ViewListSubPage({ list }) {
  return (
    <>
      <Button onClick={NavigationService.goToLists}>
        <ArrowBackIcon />
      </Button>
      <List list={list} />
    </>
  );
}

ViewListSubPage.propTypes = {
  list: PropTypes.object.isRequired,
};

const buildParams = (listId) => RequestParams.Builder.withId(listId).build();

export default compose(
  withNetworkWrapper([
    new NetWrapParam("recommendationLists", "one", {
      addDataToProp: "list",
      params: (props) => buildParams(props.listId),
      propTypes: {
        listId: PropTypes.number.isRequired,
      },
    }),
  ])
)(ViewListSubPage);
