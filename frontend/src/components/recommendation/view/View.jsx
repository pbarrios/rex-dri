/* eslint-disable react/sort-comp */
/**
 * Nasty huge components to handle list display and edition.
 *
 * A lot of function to handle adding/reordering of cells/block.
 *
 * Some hacks to optimize performances and keep performances on the top
 */
import React, { useCallback, useEffect, useState } from "react";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import { Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import DeleteIcon from "@material-ui/icons/DeleteForever";
import StartIcon from "@material-ui/icons/StarBorder";
import CheckIcon from "@material-ui/icons/CheckCircle";
import ShareIcon from "@material-ui/icons/Share";
import { v4 as uuidv4 } from "uuid";
import { withStyles } from "@material-ui/styles";
import isEqual from "lodash/isEqual";
import cloneDeep from "lodash/cloneDeep";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import { Prompt } from "react-router-dom";
import Divider from "@material-ui/core/Divider";
import CopyToClipboard from "../../common/CopyToClipboard";
import SimplePopupMenu from "../../common/SimplePopupMenu";
import { appBarHeight } from "../../../config/sharedStyles";
import SaveButton from "../../common/SaveButton";
import UnivBlock from "./UnivBlock";
import TextBlock from "./TextBlock";
import LinkToUser from "../../common/LinkToUser";
import LicenseNotice from "../../common/LicenseNotice";
import {
  useApiDelete,
  useApiInvalidateAll,
  useApiInvalidateOne,
  useApiUpdate,
} from "../../../hooks/wrappers/api";

const FORGET_SAVE_MESSAGE = "Des changements n'ont pas été enregistré !";

function preventWindowLeave() {
  window.onbeforeunload = () => FORGET_SAVE_MESSAGE;
}

function canExitWindow() {
  window.onbeforeunload = undefined;
}

class IdMaker {
  value = -1;

  get next() {
    this.value += 1;
    return this.value;
  }
}

/**
 * Optimize performances
 *
 * Choices:
 * no autosave: at this point would saving triggers a rerender
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function ImperativeBtn(props) {
  const [disabled, setDisabled] = useState(props.disabled);
  props.subscribe(setDisabled);

  useEffect(() => {
    setDisabled(props.disabled);
  }, [props.disabled]);

  return (
    <IconButton
      color="secondary"
      disabled={disabled}
      onClick={() => props.onClick()}
    >
      {props.type === "up" ? (
        <KeyboardArrowUpIcon />
      ) : (
        <KeyboardArrowDownIcon />
      )}
    </IconButton>
  );
}

ImperativeBtn.propTypes = {
  disabled: PropTypes.bool.isRequired,
  type: PropTypes.oneOf(["up", "down"]).isRequired,
  onClick: PropTypes.func.isRequired,
  subscribe: PropTypes.func.isRequired,
};

/**
 * Component to view and edit (if possible) a recommendation list
 *
 * Not using hooks to optimize performance with silent state update.
 *
 * Nasty direct DOM manipulations and directe state update to reduce the need to rerender.
 */
class View extends React.Component {
  baseId = uuidv4();

  idMaker = new IdMaker();

  __selectedIdx = 0;

  lastSelectedStyledKey = 0;
  /**
   * More optimization
   */

  _mappingIdxKey = new Map();

  constructor(props) {
    super(props);

    this.state = {
      isPublic: props.list.is_public,
      title: props.list.title,
      description: props.list.description,
      // clone deep to prevent nasty modifications propagation
      content: cloneDeep(props.list.content).map((obj) => ({
        obj,
        key: this.idMaker.next,
      })),
      savingSuccess: false,
    };
  }

  get selectedIdx() {
    return this.__selectedIdx;
  }

  set selectedIdx(idx) {
    // Direct DOM manipulation to optimize performances
    this.__selectedIdx = idx;
    this.swapSelectedStyle(this.getKeyAtIdx(idx));

    this.setDisabledMoveUp(this.cannotMoveUp(idx));
    this.setDisabledMoveDown(this.cannotMoveDown(idx));
  }

  componentDidMount() {
    // Make sure to visually select the first element if it's appropriate
    const { content } = this.state;
    if (content.length !== 0) {
      this.selectedIdx = 0;
    }
  }

  // eslint-disable-next-line no-unused-vars
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (nextProps !== this.props) {
      return !isEqual(nextProps, this.props);
    }
    return true;
  }

  /**
   * Will be set with the imperative button
   */
  setDisabledMoveUp = () => {};

  setDisabledMoveDown = () => {};

  getKeyAtIdx(idx) {
    if (idx >= this.state.content.length) return null;
    return this.state.content[idx].key;
  }

  componentDidUpdate() {
    const newKey = this.getKeyAtIdx(this.selectedIdx);
    if (this.lastSelectedStyledKey !== newKey) {
      this.swapSelectedStyle(newKey);
    }
  }

  clearSelectedStyle() {
    try {
      document
        .getElementById(this.baseId + this.lastSelectedStyledKey)
        .classList.remove(this.props.classes.selected);
    } catch (e) {
      // If the key doesn't exist this would crash
    }
  }

  swapSelectedStyle(toKey) {
    if (toKey !== null) {
      this.clearSelectedStyle();
      document
        .getElementById(this.baseId + toKey)
        .classList.add(this.props.classes.selected);
      this.lastSelectedStyledKey = toKey;
    }
  }

  /**
   *
   *
   * Content array manipulations
   *
   *
   */

  insertAtIndex(obj, idx) {
    this.setState(({ content }) => {
      const copy = content.slice();
      const key = this.idMaker.next;
      copy.splice(idx, 0, {
        obj,
        key,
      });

      return { content: copy };
    });
  }

  addBlock(type) {
    const insertIndex =
      this.state.content.length === 0 ? 0 : this.selectedIdx + 1;

    if (type === "univ-block") {
      this.insertAtIndex(
        {
          type: "univ-block",
          content: {
            university: null,
            appreciation: null,
          },
        },
        insertIndex
      );
    } else {
      this.insertAtIndex(
        {
          type: "text-block",
          content: "",
        },
        insertIndex
      );
    }
    this.selectedIdx = insertIndex;
  }

  move(from, to) {
    this.setState(({ content }) => {
      const copy = content.slice();
      copy.splice(to < 0 ? copy.length + to : to, 0, copy.splice(from, 1)[0]);
      return { content: copy };
    });
  }

  moveUp() {
    this.move(this.selectedIdx, this.selectedIdx - 1);
    this.selectedIdx -= 1;
  }

  moveDown() {
    this.move(this.selectedIdx, this.selectedIdx + 1);
    this.selectedIdx += 1;
  }

  cannotMoveUp(idx) {
    return idx === 0;
  }

  cannotMoveDown(idx) {
    return idx >= this.state.content.length - 1;
  }

  removeAtIndex(idx) {
    this.setState(({ content }) => ({
      content: content.slice().splice(idx, 1),
    }));
  }

  deleteBlock() {
    this.removeAtIndex(this.selectedIdx);
  }

  handleSilentUpdateBlock(type, key, newObjContent) {
    preventWindowLeave();
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.content[this.getIdxOfKey(key)].obj.content = newObjContent;
  }

  getIdxOfKey(key) {
    if (!this._mappingIdxKey.has(key)) {
      this.state.content.forEach(({ key: k }, idx) =>
        this._mappingIdxKey.set(k, idx)
      );
    }
    return this._mappingIdxKey.get(key);
  }

  /**
   *
   *
   * End of content array manipulations
   *
   *
   */

  updateTitle(newTitle) {
    preventWindowLeave();
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.title = newTitle;
  }

  updateDescription(newDescription) {
    preventWindowLeave();
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.description = newDescription;
  }

  hasChanges() {
    const { list } = this.props;
    const { title, description, content, isPublic } = this.state;

    if (title !== list.title) {
      return true;
    }
    if (description !== list.description) {
      return true;
    }
    if (isPublic !== list.is_public) {
      return true;
    }
    return !isEqual(
      content.map(({ obj }) => obj),
      list.content
    );
  }

  // eslint-disable-next-line consistent-return
  handleSave() {
    if (!this.hasChanges()) {
      return false;
    }
    const { list } = this.props;
    const data = { ...list };
    data.content = this.state.content.map(({ obj }) => obj);
    data.title = this.state.title;
    data.description = this.state.description;
    data.is_public = this.state.isPublic;

    this.setState({ savingSuccess: false });
    this.props.updateListOnServer(data, () => {
      canExitWindow();
      this.setState({ savingSuccess: true });
      this.props.invalidateListsSummary();
    });
  }

  renderTopBlock() {
    const { list, classes } = this.props;
    const { title, description, isPublic } = this.state;
    const isOwner = list.is_user_owner;
    const { is_user_follower: isFollower } = list;

    return (
      <div className={classes.topBlock}>
        <LicenseNotice variant={isPublic ? "REX-DRI—BY" : "REX-DRI—PRIVATE"} />
        <TextBlock
          multiline={false}
          maxLength={99}
          text={title}
          placeHolder="Saisissez l'intitulé de la liste."
          render={(titleEl) => <Typography variant="h2">{titleEl}</Typography>}
          onChange={(newTitle) => this.updateTitle(newTitle)}
          readOnly={!isOwner}
        />

        <TextBlock
          multiline
          text={description}
          placeHolder="Saisissez la description de la liste."
          maxLength={299}
          render={(descr) => <Typography variant="h5">{descr}</Typography>}
          onChange={(newDescription) => this.updateDescription(newDescription)}
          readOnly={!isOwner}
        />

        {!isOwner && (
          <Typography variant="caption">
            Liste créée par :&nbsp;
            <LinkToUser userId={list.owner} pseudo={list.owner_pseudo} />
          </Typography>
        )}
        <Typography color="secondary" variant="caption" display="block">
          {`La liste est ${isPublic ? "publique" : "privée"}.`}
        </Typography>
        {isOwner && (
          <>
            <FormControlLabel
              control={
                <Switch
                  checked={isPublic}
                  onChange={(event) => {
                    this.setState({ isPublic: event.target.checked });
                  }}
                  color="primary"
                />
              }
              label={
                isPublic ? "Rendre la liste privée" : "Rendre la liste publique"
              }
            />
            <br />
          </>
        )}

        {!isOwner && isFollower && (
          <Button
            variant="contained"
            color="secondary"
            className={classes.bigButton}
            onClick={() => {
              this.props.unFollowList(this.props.list.id, () => {
                this.props.invalidateList();
                this.props.invalidateListsSummary(); // make sure the list of lists will be consistent
              });
            }}
          >
            Liste suivie
            <CheckIcon />
          </Button>
        )}

        {!isOwner && !isFollower && (
          <Button
            variant="outlined"
            color="primary"
            className={classes.bigButton}
            onClick={() => {
              this.props.followList(this.props.list.id, () => {
                this.props.invalidateList();
                this.props.invalidateListsSummary(); // make sure the list of lists will be consistent
              });
            }}
          >
            Suivre la liste
            <StartIcon />
          </Button>
        )}

        {isPublic && (
          <div
            className={classes.bigButton}
            style={{ display: "inline-block" }}
          >
            <CopyToClipboard
              message="Le lien de la liste a été copié 😁"
              render={(copy) => (
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => copy(window.location.href)}
                >
                  Partager la liste
                  <ShareIcon />
                </Button>
              )}
            />
          </div>
        )}

        <Divider className={classes.divider} />
        {isOwner && this.renderMainEditTools()}
      </div>
    );
  }

  renderMainEditTools() {
    const { classes } = this.props;
    return (
      <div className={classes.editToolsBlock}>
        <ImperativeBtn
          disabled={this.cannotMoveUp(this.selectedIdx)}
          type="up"
          onClick={() => this.moveUp()}
          subscribe={(func) => {
            this.setDisabledMoveUp = func;
          }}
        />
        <ImperativeBtn
          disabled={this.cannotMoveDown(this.selectedIdx)}
          type="down"
          onClick={() => this.moveDown()}
          subscribe={(func) => {
            this.setDisabledMoveDown = func;
          }}
        />

        <SimplePopupMenu
          items={[
            {
              disabled: false,
              label: "Confirmer",
              onClick: () => this.deleteBlock(),
            },
          ]}
          renderHolder={({ onClick }) => (
            <IconButton
              color="secondary"
              disabled={this.state.content.length === 0}
              onClick={onClick}
            >
              <DeleteIcon />
            </IconButton>
          )}
        />

        <SimplePopupMenu
          items={[
            {
              label: "Markdown",
              onClick: () => this.addBlock("text-block"),
              disabled: false,
            },
            {
              label: "Université",
              onClick: () => this.addBlock("univ-block"),
              disabled: false,
            },
          ]}
          renderHolder={({ onClick }) => (
            <Button
              variant="contained"
              color="secondary"
              onClick={onClick}
              disabled={this.state.content.length > 100}
            >
              <AddIcon className={classes.leftIcon} />
              &nbsp; Ajouter un bloc
            </Button>
          )}
        />

        <div style={{ display: "inline-block" }}>
          <SaveButton
            label="Enregistrer"
            successLabel="Enregistrée"
            disabled={false}
            handleSaveRequested={() => {
              this.handleSave();
            }}
            success={this.state.savingSuccess}
            autoResetSuccessTimeout={1500}
          />
        </div>
        <Divider className={classes.divider} />
        <div className={classes.editSpacer} />
      </div>
    );
  }

  render() {
    this._mappingIdxKey = new Map(); // we reset this one on each render to make sure it is up to date.

    const { classes, list } = this.props;
    const { content } = this.state;
    const isOwner = list.is_user_owner;

    return (
      <>
        <Prompt
          // eslint-disable-next-line consistent-return
          message={() => {
            if (this.hasChanges()) {
              return FORGET_SAVE_MESSAGE;
            }
          }}
        />
        {this.renderTopBlock()}
        <div>
          {content.map(({ obj, key }) => (
            <div
              key={key}
              id={this.baseId + key}
              onClick={() => {
                this.selectedIdx = this.getIdxOfKey(key);
              }}
              className={classes.item}
              role="presentation"
            >
              {obj.type === "univ-block" ? (
                <UnivBlock
                  readOnly={!isOwner}
                  university={obj.content.university}
                  appreciation={obj.content.appreciation}
                  onChange={(newItem) =>
                    this.handleSilentUpdateBlock("univ-block", key, newItem)
                  }
                />
              ) : (
                <TextBlock
                  readOnly={!isOwner}
                  text={obj.content}
                  onChange={(value) =>
                    this.handleSilentUpdateBlock("text-block", key, value)
                  }
                />
              )}
            </div>
          ))}
        </div>
      </>
    );
  }
}

View.propTypes = {
  classes: PropTypes.object.isRequired,
  list: PropTypes.object.isRequired,
  updateListOnServer: PropTypes.func.isRequired,
  followList: PropTypes.func.isRequired,
  invalidateListsSummary: PropTypes.func.isRequired,
  invalidateList: PropTypes.func.isRequired,
  unFollowList: PropTypes.func.isRequired,
};

View.defaultProps = {};

const styles = (theme) => ({
  item: {
    borderLeftWidth: theme.spacing(0.7),
    borderLeftStyle: "solid",
    borderLeftColor: "transparent",
    paddingLeft: theme.spacing(1),
  },
  selected: {
    borderLeftColor: `${theme.palette.secondary.light}!important`,
  },
  topBlock: {
    backgroundColor: theme.palette.background.paper,
    [theme.breakpoints.up("md")]: {
      position: "sticky",
      top: appBarHeight(theme) - 1,
      paddingTop: theme.spacing(2),
      zIndex: 1000,
    },
  },
  editToolsBlock: {},
  leftIcon: {
    marginRight: theme.spacing(0.5),
  },
  divider: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  editSpacer: {
    height: theme.spacing(1),
  },
  bigButton: {
    margin: theme.spacing(1),
  },
});

const StyledView = withStyles(styles)(View);

// eslint-disable-next-line react/prop-types
export default ({ list }) => {
  const invalidateList = useApiInvalidateOne("recommendationLists");
  const invalidateListsSummary = useApiInvalidateAll("recommendationLists");

  const updateListOnServerInt = useApiUpdate("recommendationLists");
  const updateListOnServer = useCallback(
    (data, onSuccess) => updateListOnServerInt(data.id, data, onSuccess),
    []
  );

  const followListInt = useApiUpdate("recommendationListChangeFollower");
  const followList = useCallback(
    (listId, onSuccess) => followListInt(listId, {}, onSuccess),
    []
  );

  const unFollowList = useApiDelete("recommendationListChangeFollower");

  return (
    <StyledView
      list={list}
      invalidateList={invalidateList}
      invalidateListsSummary={invalidateListsSummary}
      updateListOnServer={updateListOnServer}
      followList={followList}
      unFollowList={unFollowList}
    />
  );
};
