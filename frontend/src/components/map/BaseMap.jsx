import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";

import ReactMapboxGl, { Feature, Layer, Popup } from "react-mapbox-gl";
import { makeStyles, useTheme } from "@material-ui/styles";
import UnivMapPopup from "./UnivMapPopup";
import Legend, { getLegendColorOpacity } from "./Legend";

const Map = ReactMapboxGl({
  dragRotate: false,
  pitchWithRotate: false,
  maxZoom: 12,
});

const useStyles = makeStyles((theme) => ({
  mapContainer: {
    width: "100%",
    [theme.breakpoints.up("lg")]: {
      height: "45vh",
      minHeight: 600,
    },
    [theme.breakpoints.down("md")]: {
      height: "60vh",
    },
  },
  legend: {
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1,
    padding: theme.spacing(1),
  },
}));

/**
 * @constant
 * @type {{center: number[], zoom: [number]}}
 */
const DEFAULT_STATUS = {
  center: [53.94, 41.13],
  zoom: [0.86],
};

// variable to hold the map center in a generic way without globalState
const ALL_MAPS = {};

/**
 * Custom react-wrapper on top of MapBoxGlJs to handle our maps in a generic manner.
 *
 * If an id is provided, the state of the map will be automatically saved and regenerated.
 */
function BaseMap({ id, universities }) {
  const theme = useTheme();
  const classes = useStyles();

  const [popup, setPopup] = useState(undefined);

  const saveStatus = useCallback(
    (map) => {
      if (typeof id !== "undefined") {
        const center = map.getCenter();
        ALL_MAPS[id] = {
          center: [center.lng, center.lat],
          zoom: [map.getZoom()],
        };
      }
    },
    [id, ALL_MAPS]
  );

  const openPopup = useCallback(
    (universityInfo) => setPopup(universityInfo),
    []
  );
  const closePopup = useCallback(() => setPopup(undefined), []);

  const toggleHover = useCallback((mapInstance, cursor) => {
    // eslint-disable-next-line no-param-reassign
    mapInstance.getCanvas().style.cursor = cursor;
  }, []);

  const renderLayer = useCallback(
    (selected) => {
      return (
        <Layer
          type="circle"
          id={`universities${selected ? "selected" : "notSelected"}`}
          paint={{
            "circle-color": getLegendColorOpacity(theme, selected).color,
            "circle-opacity": getLegendColorOpacity(theme, selected).opacity,
            "circle-radius": 8,
          }}
        >
          {universities
            .filter((c) => c.selected === selected)
            .map((universityInfo, idx) => (
              <Feature
                // eslint-disable-next-line react/no-array-index-key
                key={idx}
                coordinates={[
                  universityInfo.main_campus_lon,
                  universityInfo.main_campus_lat,
                ]}
                onClick={() => openPopup(universityInfo)}
                onMouseEnter={({ map }) => toggleHover(map, "pointer")}
                onMouseLeave={({ map }) => toggleHover(map, "")}
              />
            ))}
        </Layer>
      );
    },
    [universities, theme]
  );

  const style = theme.palette.type === "light" ? "light" : "dark";

  const mapStatus = { ...DEFAULT_STATUS };

  if (typeof id !== "undefined") {
    const previousData = ALL_MAPS[id];
    if (typeof previousData !== "undefined") {
      Object.assign(mapStatus, previousData);
    }
  }

  return (
    <div style={{ position: "relative" }}>
      <Map
        style={`/map-server/styles/${style}/style.json`}
        className={classes.mapContainer}
        zoom={mapStatus.zoom}
        center={mapStatus.center}
        onMoveEnd={saveStatus}
      >
        {universities && (
          <>
            {renderLayer(true)}
            {renderLayer(false)}
          </>
        )}
        {popup && (
          <Popup
            key={popup.univId}
            coordinates={[popup.main_campus_lon, popup.main_campus_lat]}
          >
            <UnivMapPopup {...popup} handleClose={closePopup} />
          </Popup>
        )}
      </Map>
      <div className={classes.legend}>
        <Legend />
      </div>
    </div>
  );
}

BaseMap.propTypes = {
  id: PropTypes.string,
  universities: PropTypes.array,
};

BaseMap.defaultProps = {
  id: undefined,
  universities: undefined,
};

export default BaseMap;
