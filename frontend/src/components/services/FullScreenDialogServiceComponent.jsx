import React, { useEffect, useMemo, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import FullScreenDialogService from "../../services/FullScreenDialogService";

/**
 * Component to enable the FullScreenDialogServiceComponent to have nice transitions
 * @returns
 */
// eslint-disable-next-line react/display-name
const Transition = React.forwardRef((props, ref) => (
  <Slide direction="up" ref={ref} {...props} />
));

/**
 * Component to display a full screen dialog.
 */
function FullScreenDialogServiceComponent() {
  const [children, setChildren] = useState(undefined);
  useEffect(() => {
    FullScreenDialogService.setSetChildrenComponent(setChildren);

    return () => FullScreenDialogService.setSetChildrenComponent(undefined);
  }, []);

  const open = useMemo(() => typeof children !== "undefined", [children]);

  return (
    <Dialog fullScreen open={open} TransitionComponent={Transition}>
      {open && children}
    </Dialog>
  );
}

export default React.memo(FullScreenDialogServiceComponent);
