import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import TableFromData from "./Table";
import PlotResults from "./PlotResults";
import ExportToCsvButton from "./ExportToCsvButton";
import ShareRequestButton from "./ShareRequestButton";
import { currentDatasetName, getDatasetExampleRequests } from "./utils";

const useStyles = makeStyles((theme) => ({
  exportBtn: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  exportBtnContainer: {
    display: "flex",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    justifyContent: "center",
  },
  spacer: {
    marginTop: theme.spacing(2),
  },
}));

/**
 * Component to display the result of a request
 */
function RequestResults({ results, rawDataset, request }) {
  const classes = useStyles();

  const exampleRequests = getDatasetExampleRequests(currentDatasetName);
  const exampleMatch = exampleRequests.filter((el) => el.request === request);

  // Recover label for title if it's an example request
  const title =
    exampleMatch.length === 0 ? "y = f(x)" : exampleMatch[0].graphTitle;

  return (
    <>
      <ShareRequestButton />
      <div className={classes.spacer} />
      <Typography variant="h6">Résultats de la requête</Typography>
      <div className={classes.spacer} />
      <TableFromData data={results} />
      <div className={classes.exportBtnContainer}>
        <div className={classes.exportBtn}>
          <ExportToCsvButton
            label="Le résultat de la requête (CSV)"
            data={results}
          />
        </div>
        <div className={classes.exportBtn}>
          <ExportToCsvButton
            label="Les données brutes (CSV)"
            data={rawDataset}
          />
        </div>
      </div>

      <div className={classes.spacer} />
      <Typography variant="h6">Graphique correspondant</Typography>
      <PlotResults results={results} title={title} />
    </>
  );
}

RequestResults.propTypes = {
  results: PropTypes.array.isRequired,
  rawDataset: PropTypes.array.isRequired,
  request: PropTypes.string.isRequired,
};

export default RequestResults;
