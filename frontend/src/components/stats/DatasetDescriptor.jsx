import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";

/**
 * Component to display information about the dataset
 */
function DatasetDescriptor({ columns, info }) {
  return (
    <>
      {info && <Typography variant="body1">{info}</Typography>}
      <Typography variant="h6">
        Description des colonnes du jeu de données
      </Typography>
      {columns.map(({ name, description }) => (
        <Typography variant="body2" key={name}>
          <b>{name}</b> — {description}
        </Typography>
      ))}
    </>
  );
}

DatasetDescriptor.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
  info: PropTypes.string,
};

DatasetDescriptor.defaultProps = {
  info: "",
};

export default DatasetDescriptor;
