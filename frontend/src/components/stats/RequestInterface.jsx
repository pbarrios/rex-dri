import React, { useCallback, useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/styles";
import Divider from "@material-ui/core/Divider";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import {
  currentDatasetData,
  currentDatasetName,
  executeSqlRequest,
  getDatasetExampleRequests,
  getRequestFromUrl,
  setRequestInUrl,
} from "./utils";
import ExampleRequests from "./ExampleRequests";
import useOnBeforeComponentMount from "../../hooks/useOnBeforeComponentMount";
import SqlEditor from "./SqlEditor";
import RequestResults from "./RequestResults";

const useStyles = makeStyles((theme) => ({
  fullWidth: {
    width: "100%",
  },
  spacer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

const requestFromUrl = getRequestFromUrl();

const exampleRequests = getDatasetExampleRequests(currentDatasetName);
const isPredefinedRequest =
  exampleRequests.filter((el) => el.request === requestFromUrl).length === 1;

function RequestInterface() {
  const classes = useStyles();

  const [sqlRequest, setSqlRequest] = useState(requestFromUrl);

  const [requestError, setRequestError] = useState("");
  const [requestResults, setRequestResults] = useState([]);

  useEffect(() => {
    setRequestResults([]);
  }, [sqlRequest]);

  const performRequest = useCallback((request) => {
    // make sure to update the sql request in the text area
    setSqlRequest(request);
    setRequestError("");
    setRequestResults([]);
    executeSqlRequest(request)
      .then((res) => {
        setRequestResults(res);
        setRequestInUrl(request);
      })
      .catch((err) => {
        setRequestError(err);
      });
  }, []);

  // Execute request on first mount (better when sharing)
  useOnBeforeComponentMount(() => {
    performRequest(sqlRequest);
  });

  const [
    exampleRequestsPanelExpanded,
    setExampleRequestsPanelExpanded,
  ] = useState(isPredefinedRequest);
  const [sqlEditorPanelExpanded, setSqlEditorPanelExpanded] = useState(
    !isPredefinedRequest
  );

  return (
    <div>
      <ExpansionPanel
        onChange={(e, expanded) => setExampleRequestsPanelExpanded(expanded)}
        expanded={exampleRequestsPanelExpanded}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">
            Éxécuter une des requêtes pré-définies
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div>
            <ExampleRequests
              performRequest={performRequest}
              requests={exampleRequests}
            />
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        onChange={(e, expanded) => setSqlEditorPanelExpanded(expanded)}
        expanded={sqlEditorPanelExpanded}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="h6">
            Personnaliser la requête (usage avancé)
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className={classes.fullWidth}>
            <SqlEditor
              request={sqlRequest}
              setRequest={setSqlRequest}
              performRequest={performRequest}
            />
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>

      <Divider className={classes.spacer} />

      {requestError !== "" && (
        <>
          <br />
          <Typography variant="caption">{requestError.toString()}</Typography>
          <br />
        </>
      )}
      {requestResults.length !== 0 && (
        <RequestResults
          results={requestResults}
          rawDataset={currentDatasetData}
          request={sqlRequest}
        />
      )}
    </div>
  );
}

export default RequestInterface;
