import React from "react";
import { compose } from "recompose";
import Typography from "@material-ui/core/Typography";
import { withErrorBoundary } from "../common/ErrorBoundary";
import Markdown from "../common/markdown/Markdown";
import { withPaddedPaper } from "./shared";
import InformationList from "../app/InformationList";
import CustomLink from "../common/CustomLink";
import APP_ROUTES from "../../config/appRoutes";
import ExternalDataUpdateInfo from "../app/ExternalDataUpdateInfo";
import UnlinkedPartners from "../app/UnlinkedPartners";

const sourceIntro = `
**REX-DRI** c'est LA plateforme de capitalisation sur les départs à l'étranger à l'UTC.

Vous y retrouvez:
* Les destinations disponibles,
* Les précédents départs effectués,
* Une carte des universités partenaires,
* Toutes les bonnes informations contribuées par les uns et les autres,
* Des listes mêlant universités et commentaires à votre guise,
* etc.


Les valeurs de la plateforme sont : 
**bienveillance**, **partage**, **contribution** et **collaboration**. Aidez-nous à les diffuser ! 😉

**Le bon réflexe** : penser à [donner les autorisations de partage d'informations (**cours et login**) avec
la plateforme REX-DRI depuis l'ENT](https://webapplis.utc.fr/etudiant/suiviOutgoing/autorisationsREX.faces)
pour enrichir automatiquement la plateforme.
`;

const sourceFocusMarkdown = `
De nombreux éléments de saisie supportent la syntaxe [markdown](https://www.markdownguide.org/basic-syntax/)
pour un rendu plus nuancé, faites-en bon usage 😌 (vive le **gras**, l'*italique*, etc.).

**Dès que vous parlez d'💰, nous vous invitons à utiliser la syntaxe dédiée** \`:1100.10USD:\` 
(deux-points, suivi du montant et du code ISO de la monnaie, puis de nouveau deux-points) dans votre markdown.
Cette dernière sera automatiquement reconnue et la valeur après application du taux de change du jour 
sera affichée en euro : :1100.10USD: 🎉
`;

/**
 * Component corresponding to the landing page of the site
 */
function PageHome() {
  return (
    <>
      <Typography variant="h3">
        Bienvenue sur&nbsp;
        <em>
          <b>REX-DRI</b>
        </em>
      </Typography>
      <Markdown source={sourceIntro} />
      <Typography variant="h5">Focus sur le markdown</Typography>
      <Markdown source={sourceFocusMarkdown} />
      <Typography variant="h4">Amélioration continue</Typography>
      <Typography>
        Un bug 🐇 ? Une typo ? Une fonctionnalité qui vous manque ? Envie de
        contribuer (bénévolement/PR) 😍 ? Plus d'informations &nbsp;
        <CustomLink to={APP_ROUTES.aboutProject}>ici</CustomLink>.
      </Typography>
      <Typography paragraph />
      <Typography variant="h4">Informations dynamiques</Typography>
      <Typography variant="h5">Actualités</Typography>
      <InformationList />
      <Typography variant="h5" display="inline">
        Status des données importées
      </Typography>
      <Typography variant="caption" display="inline">
        &nbsp; (Dernières mise à jour)
      </Typography>
      <ExternalDataUpdateInfo />
      <UnlinkedPartners variant="summary" />
    </>
  );
}

PageHome.propTypes = {};

export default compose(withPaddedPaper(), withErrorBoundary())(PageHome);
