import React from "react";
import { makeStyles } from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import { compose } from "recompose";
import { withErrorBoundary } from "../common/ErrorBoundary";
import Filter from "../filter/Filter";
import MainMap from "../map/MainMap";
import { withPaddedPaper } from "./shared";
import UnlinkedPartners from "../app/UnlinkedPartners";

const useStyles = makeStyles((theme) => ({
  filter: {
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Component corresponding to the page with the map of the universities
 */
// eslint-disable-next-line no-unused-vars
function PageMap() {
  const classes = useStyles();

  return (
    <>
      <Typography variant="h4" gutterBottom>
        Exploration Cartographique
      </Typography>
      <UnlinkedPartners variant="summary" />
      <div className={classes.filter}>
        <Filter />
      </div>
      <MainMap />
    </>
  );
}

export default compose(withPaddedPaper(), withErrorBoundary())(PageMap);
