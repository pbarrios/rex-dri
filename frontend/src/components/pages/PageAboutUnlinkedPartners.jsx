import React from "react";
import { compose } from "recompose";
import Markdown from "../common/markdown/Markdown";
import { withErrorBoundary } from "../common/ErrorBoundary";
import { withPaddedPaper } from "./shared";
import UnlinkedPartners from "../app/UnlinkedPartners";

const source = `
Pour tendre vers l'expérience la plus à jour **REX-DRI** récupère tous les jours
automatiquement les données de l'UTC. Parmis ces données sont les partenaires de l'UTC.

Lorsque de nouveaux partenaires sont ajoutés par l'UTC, il est nécessaire de l'associer
à différentes informations (localisation, site internet, etc.) du côté de la plateforme
**REX-DRI**. Cette opération doit se faire manuellement.
`;

/**
 * Component corresponding to page about the project.
 */
function PageAboutProject() {
  return (
    <>
      <Markdown source={source} />
      <UnlinkedPartners variant="detailed" />
    </>
  );
}

export default compose(
  withPaddedPaper(),
  withErrorBoundary()
)(PageAboutProject);
