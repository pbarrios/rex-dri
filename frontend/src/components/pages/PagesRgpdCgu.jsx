import React from "react";
import { PaddedPaper } from "./shared";
import { CGU_MARKDOWN_SOURCE, RGPD_MARKDOWN_SOURCE } from "../../config/other";
import BaseMarkdown from "../common/markdown/BaseMarkdown";

export function PageRgpd() {
  /**
   * If you modify this components, make sure the page http://localhost:8000/rgpd-raw/ is accessible
   */
  return (
    <PaddedPaper>
      <BaseMarkdown source={RGPD_MARKDOWN_SOURCE} headingOffset={2} />
    </PaddedPaper>
  );
}

export function PageCgu() {
  return (
    <PaddedPaper>
      <BaseMarkdown source={CGU_MARKDOWN_SOURCE} headingOffset={2} />
    </PaddedPaper>
  );
}
