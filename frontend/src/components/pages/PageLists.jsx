import React from "react";
import compose from "recompose/compose";
import PropTypes from "prop-types";
import { withErrorBoundary } from "../common/ErrorBoundary";
import SelectList from "../recommendation/SelectListSubPage";
import ViewList from "../recommendation/ViewListSubPage";

import { withPaddedPaper } from "./shared";

/**
 * Component to render the page associated with recommendation lists
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function PageLists(props) {
  const { listId } = props.match.params;
  const parsedListId = parseInt(listId, 10);
  return (
    <>
      {isNaN(parsedListId) ? (
        <SelectList />
      ) : (
        <ViewList listId={parsedListId} />
      )}
    </>
  );
}

PageLists.propTypes = {
  match: PropTypes.object.isRequired,
};

export default compose(withPaddedPaper(), withErrorBoundary())(PageLists);
