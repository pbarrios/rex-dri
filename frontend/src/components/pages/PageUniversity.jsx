import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import compose from "recompose/compose";
import UniversityTemplate from "../university/UniversityTemplate";
import UnivInfoProvider from "../university/UnivInfoProvider";
import { withErrorBoundary } from "../common/ErrorBoundary";
import APP_ROUTES from "../../config/appRoutes";
import CustomNavLink from "../common/CustomNavLink";
import UniversityService from "../../services/data/UniversityService";
import { useApiCreate } from "../../hooks/wrappers/api";

function UniversityNotFound() {
  return (
    <Dialog
      open
      // onClose={this.handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        L'université demandée n'est pas reconnue !
      </DialogTitle>
      <DialogActions>
        <CustomNavLink to={APP_ROUTES.university}>
          <Button variant="contained" color="primary">
            C'est noté, ramenez-moi sur le droit chemin.
          </Button>
        </CustomNavLink>
      </DialogActions>
    </Dialog>
  );
}

let lastVisitedUniv = -1;

/**
 * Component holding the page with the university details
 */
function PageUniversity({ match }) {
  const { univId, tabName } = match.params;

  const createLastVisited = useApiCreate("lastVisitedUniversities");

  useEffect(() => {
    if (lastVisitedUniv !== univId)
      createLastVisited({ university: univId }, () => {
        lastVisitedUniv = univId;
      });
  }, [univId]);

  const parsedUnivId = parseInt(univId, 10);

  if (UniversityService.hasUniversity(parsedUnivId)) {
    return (
      <UnivInfoProvider univId={parsedUnivId}>
        <UniversityTemplate tabName={tabName} univId={parsedUnivId} />
      </UnivInfoProvider>
    );
  }

  return <UniversityNotFound />;
}

PageUniversity.propTypes = {
  // eslint-disable-next-line react/no-unused-prop-types
  match: PropTypes.object.isRequired,
};

export default compose(
  // volontarly no withPaddedPaper(), here
  withErrorBoundary()
)(PageUniversity);
