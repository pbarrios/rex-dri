import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import MainAppFrame from "../../app/MainAppFrame";
import Markdown from "../../common/markdown/Markdown";

const source = `
# Démo du thème

Un petit texte en markdown avec un [lien](https://material-ui.com/style/color/#color-tool)
`;

/**
 * React component for demo purposes
 *
 * @param {object} props
 * @returns
 */
function ColorDemo(props) {
  const { classes, theme } = props;

  return (
    <div className={classes.root}>
      <MainAppFrame>
        <Paper style={theme.myPaper}>
          <Typography variant="h3">
            Bienvenue sur &nbsp;
            <em>
              <b>REX-DRI</b>
            </em>
          </Typography>
          <Markdown source={source} />
        </Paper>
      </MainAppFrame>
    </div>
  );
}

ColorDemo.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const styles = (theme) => ({
  root: {
    position: "relative",
    overflow: "hidden",
    boxShadow: theme.shadows[3],
    zIndex: 0,
  },
});

export default withStyles(styles, { withTheme: true })(ColorDemo);
