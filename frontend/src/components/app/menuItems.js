import MapIcon from "@material-ui/icons/Map";
import LocationCityIcon from "@material-ui/icons/LocationCity";
import HomeIcon from "@material-ui/icons/Home";
import SearchIcon from "@material-ui/icons/Search";
import AssignmentIcon from "@material-ui/icons/Assignment";
import APP_ROUTES from "../../config/appRoutes";

function item(label, route, Icon, hardRedirect = false) {
  return {
    label,
    route,
    Icon,
    hardRedirect,
  };
}

export const mainMenuItems = [
  item("Carte", APP_ROUTES.map, MapIcon),
  item("Recherche", APP_ROUTES.search, SearchIcon),
  item("Listes", APP_ROUTES.lists, AssignmentIcon),
];

export const mainMenuHome = item("Accueil", APP_ROUTES.base, HomeIcon);

export const secondaryMenuItems = [
  item("Mes échanges", APP_ROUTES.myExchanges, LocationCityIcon),
];

export const infoMenuItems = [
  // item("Mes fichiers", APP_ROUTES.userFilesCurrent, null), // WARNING BETA
  item("Le projet REX-DRI", APP_ROUTES.aboutProject, null),
  item("Conditions d'utilisations", APP_ROUTES.aboutCgu, null),
  item("Information RGPD", APP_ROUTES.aboutRgpd, null),
  item("Statistiques du site", APP_ROUTES.stats, null, true),
];

export const settingsMenuItems = [
  item("Mes informations", APP_ROUTES.userCurrent, null),
  item("Personnalisation du thème", APP_ROUTES.themeSettings, null),
  item("Se déconnecter", APP_ROUTES.logout, null, true),
];
