import React from "react";
import { compose } from "recompose";
import PropTypes from "prop-types";
import { withErrorBoundary } from "../common/ErrorBoundary";
import RequestParams from "../../utils/api/RequestParams";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import NotificationService from "../../services/NotificationService";

function NotifierImportantInformation({ informationList }) {
  informationList
    .filter((el) => el.should_notify)
    .forEach((el) =>
      NotificationService.notify(el.message, {
        variant: el.variant,
        autoHideDuration: null,
      })
    );

  return <></>;
}

NotifierImportantInformation.propTypes = {
  informationList: PropTypes.array.isRequired,
};

export default compose(
  withNetworkWrapper(
    [
      new NetWrapParam("information", "all", {
        addDataToProp: "informationList",
        params: RequestParams.Builder.withQueryParam("now", "true").build(),
      }),
    ],
    React.Fragment
  ),
  withErrorBoundary()
)(NotifierImportantInformation);
