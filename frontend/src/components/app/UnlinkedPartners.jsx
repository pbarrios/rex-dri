import React from "react";

import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import APP_ROUTES from "../../config/appRoutes";
import CustomLink from "../common/CustomLink";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";

/**
 * Class to render notifications for important stuff
 */
function UnlinkedPartners({ unlinkedPartners, variant }) {
  const nUnlinked = unlinkedPartners.length;

  if (variant === "detailed") {
    return (
      <>
        {nUnlinked > 0 ? (
          <>
            <Typography>
              Actuellement, {nUnlinked}
              &nbsp; partenaires sont dans ce cas. En voici la liste :
            </Typography>
            <List aria-label="List des partenaires qui ne sont pas encore disponibles sur la plateforme REX-DRI">
              {unlinkedPartners.map((p, idx) => (
                // eslint-disable-next-line react/no-array-index-key
                <ListItem key={idx}>
                  <ListItemText
                    primary={`${
                      p.partner_id
                    } ${p.partner_univ_name.toLowerCase()}`}
                  />
                </ListItem>
              ))}
            </List>
          </>
        ) : (
          <Typography>
            Actuellement, tous les partenaires de l'UTC sont disponibles sur la
            plateforme ! 🎉
          </Typography>
        )}
      </>
    );
  }
  if (variant === "summary") {
    return (
      <>
        {nUnlinked > 0 && (
          <>
            <Typography variant="caption" color="primary">
              ⚠&nbsp;
              {nUnlinked === 1
                ? "1 université partenaire de l'UTC n'est pas encore pleinement disponible"
                : `${nUnlinked} universités partenaires de l'UTC ne sont pas encore pleinement disponibles`}
              sur la plateforme. Plus d'informations&nbsp;
              <CustomLink to={APP_ROUTES.aboutUnlinkedPartners}>
                <Typography variant="caption" color="primary">
                  ici
                </Typography>
              </CustomLink>
              .
            </Typography>
          </>
        )}
      </>
    );
  }

  return <></>;
}

UnlinkedPartners.propTypes = {
  variant: PropTypes.oneOf(["summary", "detailed"]).isRequired,
  unlinkedPartners: PropTypes.array.isRequired,
};

export default withNetworkWrapper([
  new NetWrapParam("unlinkedUtcPartners", "all", {
    addDataToProp: "unlinkedPartners",
  }),
])(UnlinkedPartners);
