import React from "react";
import { compose } from "recompose";
import { makeStyles } from "@material-ui/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import APP_ROUTES from "../../config/appRoutes";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import UniversityService from "../../services/data/UniversityService";
import NavigationService from "../../services/NavigationService";
import LicenseNotice from "../common/LicenseNotice";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginLeft: "auto",
    marginRight: theme.spacing(2),
    padding: theme.spacing(1),
    maxWidth: "80%",
    position: "sticky",
    bottom: theme.spacing(2),
    border: "solid",
    borderWidth: 2,
    borderRadius: theme.shape.borderRadius,
    borderColor: theme.palette.primary.main,
    borderStyle: "dashed",
    backgroundColor: theme.palette.background.paper,
    zIndex: 1000,
  },
}));

function LastVisitedUniversity({ location, lastVisitedUniversities }) {
  const classes = useStyles();

  if (
    location.pathname === APP_ROUTES.themeSettings ||
    location.pathname === APP_ROUTES.myExchanges ||
    location.pathname === APP_ROUTES.editPreviousExchangeWithParams ||
    location.pathname === APP_ROUTES.userWithParams ||
    location.pathname === APP_ROUTES.aboutProject ||
    location.pathname === APP_ROUTES.aboutRgpd ||
    location.pathname === APP_ROUTES.aboutCgu ||
    location.pathname === APP_ROUTES.aboutUnlinkedPartners ||
    location.pathname === APP_ROUTES.logout
  ) {
    return <></>;
  }

  const handleChange = (event) => {
    const univId = event.target.value;
    if (univId !== "") NavigationService.goToUniversity(univId);
  };

  return (
    <Box className={classes.formControl} boxShadow={1}>
      <FormControl>
        <InputLabel shrink id="select-last-visitied-university-label">
          Dernières universités visitées
        </InputLabel>
        <Select
          labelId="select-last-visitied-university-label"
          id="select-last-visitied-university"
          value=""
          onChange={handleChange}
        >
          {lastVisitedUniversities.map(({ university }) => (
            <MenuItem key={university} value={university}>
              {UniversityService.getUnivName(university)}
            </MenuItem>
          ))}
        </Select>
        <LicenseNotice variant="REX-DRI—PRIVATE" spacer={false} />
      </FormControl>
    </Box>
  );
}

LastVisitedUniversity.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  lastVisitedUniversities: PropTypes.arrayOf(
    PropTypes.shape({
      university: PropTypes.number.isRequired,
      ts: PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("lastVisitedUniversities", "all", {
      addDataToProp: "lastVisitedUniversities",
    }),
  ]),
  withRouter
)(LastVisitedUniversity);
