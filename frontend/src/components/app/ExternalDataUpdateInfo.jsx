import React from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import toDateFr from "../../utils/dateToFr";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";

function getLabel(source) {
  if (source === "fixer") return "Données concernant les taux de change";
  if (source === "utc") return "Données issues de l'ENT";
  return "Donnée externe non connue";
}

/**
 * Component to render notifications for important stuff
 */
function ExternalDataUpdateInfo({ updates }) {
  updates.sort((a, b) => a.source.localeCompare(b.source));

  if (updates.length > 0) {
    return (
      <List aria-label="List of updates">
        {updates.map((el) => (
          <ListItem key={el.source}>
            <ListItemText
              primary={getLabel(el.source)}
              secondary={toDateFr(el.timestamp)}
            />
          </ListItem>
        ))}
      </List>
    );
  }

  return (
    <Typography variant="caption" display="block">
      Aucune mise-à-jour ne semble avoir été réalisée.
    </Typography>
  );
}

ExternalDataUpdateInfo.propTypes = {
  updates: PropTypes.array.isRequired,
};

ExternalDataUpdateInfo.defaultProps = {};

export default withNetworkWrapper([
  new NetWrapParam("externalDataUpdateInfo", "all", {
    addDataToProp: "updates",
  }),
])(ExternalDataUpdateInfo);
