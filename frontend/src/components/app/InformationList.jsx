import React from "react";
import { compose } from "recompose";
import ErrorIcon from "@material-ui/icons/Error";
import StartIcon from "@material-ui/icons/StarTwoTone";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PropTypes from "prop-types";
import RequestParams from "../../utils/api/RequestParams";
import { withErrorBoundary } from "../common/ErrorBoundary";
import toDateFr from "../../utils/dateToFr";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";

const INFORMATION_ICONS = {
  success: <StartIcon />,
  info: <InfoIcon />,
  error: <ErrorIcon color="error" />,
  warning: <WarningIcon color="error" />,
};

/**
 * Component to render notifications for important stuff
 */
function InformationList({ includeVariants, informationList }) {
  const informationListFiltered = informationList.filter(({ variant }) =>
    includeVariants.includes(variant)
  );

  informationListFiltered.sort((a, b) => -a.start.localeCompare(b.start));

  return (
    <List aria-label="List of information">
      {informationListFiltered.map((el) => (
        <ListItem key={el.id}>
          <ListItemIcon>{INFORMATION_ICONS[el.variant]}</ListItemIcon>
          <ListItemText primary={el.message} secondary={toDateFr(el.start)} />
        </ListItem>
      ))}
    </List>
  );
}

InformationList.propTypes = {
  includeVariants: PropTypes.arrayOf(PropTypes.string.isRequired),
  informationList: PropTypes.array.isRequired,
};

InformationList.defaultProps = {
  includeVariants: ["warning", "error", "info", "success"],
};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("information", "all", {
      addDataToProp: "informationList",
      params: RequestParams.Builder.withQueryParam("now", "true").build(),
    }),
  ]),
  withErrorBoundary()
)(InformationList);
