import React from "react";

import { makeStyles } from "@material-ui/styles";
import PropTypes from "prop-types";
import CustomNavLink from "../common/CustomNavLink";
import classNames from "../../utils/classNames";

const useStyle = makeStyles((theme) => ({
  logoContainer: {
    position: "relative",
    width: "fit-content",
  },
  innerLogoContainer: {
    display: "flex",
    alignItems: "center",
    borderRadius: 0,
    paddingLeft: 8,
    paddingRight: 8,
    flexWrap: "nowrap",
    "&:hover": {
      cursor: "pointer",
    },
  },
  siteName: {
    fontWeight: 900,
    fontSize: "1.75rem",
    whiteSpace: "nowrap",
    color: theme.palette.getContrastText(theme.palette.primary.main),
  },
  icon: {
    position: "relative",
    width: "3.6em",
    height: "3.6em",
    overflow: "initial",
    backgroundColor: "transparent",
    right: "0.2em",
    bottom: "-0.1em",
  },
  logoUnderline: {
    position: "absolute",
    left: 0,
    bottom: 14,
    height: 9,
    backgroundColor: theme.palette.secondary.main,
    width: "100%",
    zIndex: -1,
  },
}));

function Core() {
  const classes = useStyle();

  return (
    <>
      <div className={classes.innerLogoContainer}>
        <img
          className={classes.icon}
          alt="icon"
          src="/static/base_app/favicon/favicon.svg"
        />
        <span className={classes.siteName}>REX-DRI</span>
        <div className={classes.logoUnderline} />
      </div>
    </>
  );
}

function Logo({ linkTo }) {
  const classes = useStyle();
  return (
    <div className={classNames(classes.logoContainer)}>
      {linkTo ? (
        <CustomNavLink to={linkTo}>
          <Core />
        </CustomNavLink>
      ) : (
        <Core />
      )}
    </div>
  );
}

Logo.propTypes = {
  linkTo: PropTypes.string,
};

Logo.defaultProps = {
  linkTo: "",
};

export default Logo;
