import React from "react";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import SettingsIcon from "@material-ui/icons/Settings";
import InfoIcon from "@material-ui/icons/Info";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import Fab from "@material-ui/core/Fab";
import { makeStyles } from "@material-ui/styles";
import {
  infoMenuItems,
  mainMenuItems,
  secondaryMenuItems,
  settingsMenuItems,
} from "./menuItems";
import IconWithMenu from "../common/IconWithMenu";
import DrawerMenu from "./DrawerMenu";
import APP_ROUTES from "../../config/appRoutes";
import CustomNavLink from "../common/CustomNavLink";
import classNames from "../../utils/classNames";
import Logo from "./Logo";
import BaseTemplate from "./BaseTemplate";
import useOpenClose from "../../hooks/useOpenClose";

const useStyles = makeStyles((theme) => {
  const onDesktops = "@media (min-width:1375px)";
  const onMobiles = "@media (max-width:1375px)";

  return {
    menuButton: {
      [onDesktops]: {
        display: "none",
      },
      marginLeft: -12,
      marginRight: 20,
    },
    menuButtonIcon: {
      fontSize: "1.5em",
    },
    mainMenuButton: {
      fontSize: "1.2rem",
      fontWeight: 700,
      [onMobiles]: {
        display: "none",
      },
      [onDesktops]: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
      },
    },
    mainMenuButtonIcon: {
      marginLeft: theme.spacing(1),
      fontSize: "1.2em",
      [onMobiles]: {
        display: "none",
      },
    },
    contrastTextPrimary: {
      color: theme.palette.getContrastText(theme.palette.primary.main),
    },
    contrastTextSecondary: {
      color: theme.palette.getContrastText(theme.palette.secondary.main),
    },
    mobileIcon: {
      color: theme.palette.getContrastText(theme.palette.primary.main),
    },
    mobileIconContainer: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    primaryDark: {
      backgroundColor: theme.palette.primary.dark,
      boxShadow: theme.shadows[1],
    },
    desktopOnly: {
      [onMobiles]: {
        display: "none",
      },
    },
    mobileOnly: {
      [onDesktops]: {
        display: "none",
      },
    },
    ifNotTooSmallAndInlined: {
      "@media (max-width:790px)": {
        display: "none",
      },
      display: "inline",
    },
    centered: {
      margin: "0 auto",
    },
    leftBlock: {
      flex: 2,
    },
    middleBlock: {
      flex: 1,
    },
    rightBlock: {
      flex: 2,
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      textAlign: "center",
    },
    widthFitContent: {
      width: "fit-content",
    },
    filler: {
      width: "100%",
    },
  };
});

function SecondaryIcons() {
  const classes = useStyles();
  return (
    <div className={classes.ifNotTooSmallAndInlined}>
      <IconWithMenu
        Icon={InfoIcon}
        iconProps={{ className: classes.mobileIcon }}
        fabProps={{
          color: "primary",
          className: classes.mobileIconContainer,
          classes: { primary: classes.primaryDark },
        }}
        menuItems={infoMenuItems}
      />

      <IconWithMenu
        Icon={SettingsIcon}
        iconProps={{ className: classes.mobileIcon }}
        fabProps={{
          color: "primary",
          className: classes.mobileIconContainer,
          classes: { primary: classes.primaryDark },
        }}
        menuItems={settingsMenuItems}
      />
    </div>
  );
}

/**
 * Helper to render the full size versions of the menu items
 *
 * @param items
 * @returns {*}
 * @constructor
 */
function MenuItems({ items }) {
  const classes = useStyles();
  return (
    <div
      className={classNames(
        classes.desktopOnly,
        classes.centered,
        classes.widthFitContent
      )}
    >
      {items.map(({ label, Icon, route }, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <CustomNavLink to={route} key={idx}>
          <Button
            color="default"
            className={classNames(
              classes.mainMenuButton,
              classes.contrastTextPrimary
            )}
          >
            {label}
            {<Icon className={classes.mainMenuButtonIcon} />}
          </Button>
        </CustomNavLink>
      ))}
    </div>
  );
}

MenuItems.propTypes = {
  items: PropTypes.array.isRequired,
};

/**
 * Helper to render simplified versions (icon based) of menu
 * @param items
 */
function SimplifiedItems({ items }) {
  const classes = useStyles();
  return (
    <div className={classes.ifNotTooSmallAndInlined}>
      {items.map(({ label, Icon, route }, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <CustomNavLink to={route} key={idx}>
          <Fab
            size="medium"
            color="primary"
            classes={{ primary: classes.primaryDark }}
            aria-label={label}
            className={classes.mobileIconContainer}
          >
            <Icon
              className={classNames(
                classes.mobileIcon,
                classes.contrastTextSecondary
              )}
            />
          </Fab>
        </CustomNavLink>
      ))}
    </div>
  );
}

SimplifiedItems.propTypes = {
  items: PropTypes.array.isRequired,
};

function SimplifiedLeft({ openDrawer }) {
  const classes = useStyles();
  return (
    <div className={classes.mobileOnly}>
      <IconButton
        className={classes.menuButton}
        color="inherit"
        aria-label="Menu"
        onClick={openDrawer}
      >
        <MenuIcon className={classes.menuButtonIcon} />
      </IconButton>
      <SimplifiedItems items={mainMenuItems} />
    </div>
  );
}

SimplifiedLeft.propTypes = {
  openDrawer: PropTypes.func.isRequired,
};

function SimplifiedRight() {
  const classes = useStyles();
  return (
    <div className={classes.mobileOnly}>
      <SimplifiedItems items={secondaryMenuItems} />
    </div>
  );
}

function MainAppFrame({ children }) {
  const classes = useStyles();
  const [drawerOpened, openDrawer, closeDrawer] = useOpenClose(false);

  const inBetween = (
    <DrawerMenu open={drawerOpened} closeDrawer={closeDrawer} />
  );

  const toolbarContent = (
    <>
      <div className={classes.leftBlock}>
        <MenuItems items={mainMenuItems} />
        <SimplifiedLeft openDrawer={openDrawer} />
      </div>

      <div className={classes.middleBlock}>
        <Logo linkTo={APP_ROUTES.base} />
      </div>

      <div className={classes.rightBlock}>
        <div style={{ flex: 2 }} />
        <div
          style={{
            flex: 3,
            flexBasis: "auto",
          }}
        >
          <MenuItems items={secondaryMenuItems} />
          <SimplifiedRight />
        </div>
        <div
          style={{
            flex: 4,
            flexBasis: "auto",
          }}
        >
          <SecondaryIcons />
        </div>
      </div>
    </>
  );

  return (
    <BaseTemplate toolbarContent={toolbarContent} inBetween={inBetween}>
      {children}
    </BaseTemplate>
  );
}

MainAppFrame.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainAppFrame;
