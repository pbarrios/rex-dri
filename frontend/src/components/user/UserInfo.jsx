import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import SendIcon from "@material-ui/icons/Send";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CreateIcon from "@material-ui/icons/Create";
import { makeStyles } from "@material-ui/styles";
import SameLine from "../common/SameLine";
import UserInfoForm from "./UserInfoForm";
import classNames from "../../utils/classNames";
import RequestParams from "../../utils/api/RequestParams";
import DeleteAccount from "./DeleteAccount";
import { CURRENT_USER_ID } from "../../config/user";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import useEditor from "../../hooks/useEditor";

function TypographyWithIcon({ variant, typographyClass, text, isPublic }) {
  return (
    <SameLine>
      <Typography variant={variant} className={typographyClass}>
        {text}
      </Typography>
      {isPublic ? (
        <CheckCircleIcon color="primary" />
      ) : (
        <CancelIcon color="disabled" />
      )}
    </SameLine>
  );
}

TypographyWithIcon.defaultProps = {
  typographyClass: "",
};

TypographyWithIcon.propTypes = {
  isPublic: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  typographyClass: PropTypes.string,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  wrapper: {
    maxWidth: "800px",
  },
  button: {
    margin: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  login: {
    fontFamily: "monospace",
  },
  deleteContainer: {
    margin: theme.spacing(2),
  },
  spacer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  centered: {
    margin: "0 auto",
    display: "block",
  },
}));

/**
 * Component used to display the user information
 */
function UserInfo({ user }) {
  const openEditor = useEditor(UserInfoForm);

  const classes = useStyles();

  const {
    allow_sharing_personal_info: userSharesInfo,
    email,
    first_name: firstName,
    last_name: lastName,
    pseudo,
    secondary_email: secondaryEmail,
    username: login,
    id,
    delete_next_time: deleteProgrammed,
  } = user;
  const isOwner = CURRENT_USER_ID === parseInt(id, 10);
  const displayData = isOwner || userSharesInfo;

  return (
    <div className={classes.root}>
      <div className={classNames(classes.wrapper, classes.centered)}>
        <TypographyWithIcon
          isPublic={userSharesInfo}
          variant="h3"
          text={
            displayData && firstName && lastName
              ? `${firstName} ${lastName.toUpperCase()}`
              : "Prénom et Nom"
          }
        />

        <TypographyWithIcon
          isPublic={userSharesInfo}
          variant="h5"
          text={displayData ? login : "Login UTC"}
          typographyClass={classes.login}
        />

        <Typography variant="caption">
          Les prénom, nom et login sont fournis par le CAS de l'UTC.
        </Typography>

        <div className={classes.spacer} />

        <TypographyWithIcon isPublic variant="h6" text="Pseudo" />

        <Typography variant="body1">
          <em>{pseudo}</em>
        </Typography>

        <div className={classes.spacer} />

        <TypographyWithIcon
          isPublic={userSharesInfo}
          variant="h6"
          text="Adresse mail UTC"
        />
        {displayData && email ? (
          <>
            <Button
              variant="outlined"
              color="secondary"
              className={classes.button}
              href={`mailto:${email}`}
            >
              {email}
              &nbsp;
              <SendIcon className={classes.rightIcon} />
            </Button>
            <Typography variant="caption">
              Cette adresse mail a été fournie par le CAS de l'UTC lors de la
              connexion de la personne. Si la personne n"est plus à l"UTC, il se
              peut que cette adresse ne soit plus d'actualité.
            </Typography>
          </>
        ) : (
          <Typography variant="caption">
            Aucune adresse mail rattachée à ce compte (compte créé hors CAS).
          </Typography>
        )}

        <div className={classes.spacer} />

        <TypographyWithIcon
          isPublic={userSharesInfo}
          variant="h6"
          text="Autre adresse de contact"
        />
        {displayData && secondaryEmail !== null && secondaryEmail !== "" ? (
          <>
            <Button
              variant="outlined"
              color="secondary"
              className={classes.button}
              href={`mailto:${secondaryEmail}`}
            >
              {secondaryEmail}&nbsp;
              <SendIcon className={classes.rightIcon} />
            </Button>
            <Typography variant="caption">
              Cette adresse mail a été mise à disposition par la personne comme
              autre point de contact.
            </Typography>
          </>
        ) : (
          <Typography variant="caption">
            Aucune adresse email secondaire n'est disponible.
          </Typography>
        )}

        <div className={classes.spacer} />

        {isOwner && (
          <>
            {!deleteProgrammed && (
              <>
                <div style={{ width: "100%" }}>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    onClick={() => openEditor(user)}
                  >
                    Éditer
                    <CreateIcon className={classes.rightIcon} />
                  </Button>
                </div>
              </>
            )}
            <div className={classes.deleteContainer}>
              <DeleteAccount deleteProgrammed={deleteProgrammed} />
            </div>
          </>
        )}
      </div>
    </div>
  );
}

UserInfo.propTypes = {
  user: PropTypes.object.isRequired,
};

const buildParams = (userId) => RequestParams.Builder.withId(userId).build();

export default withNetworkWrapper([
  new NetWrapParam("users", "one", {
    addDataToProp: "user",
    params: (props) => buildParams(props.userId),
    propTypes: {
      userId: PropTypes.string.isRequired,
    },
  }),
])(UserInfo);
