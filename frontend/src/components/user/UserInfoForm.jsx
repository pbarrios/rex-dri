import React from "react";
import TextField from "../edition/fields/TextField";
import BooleanField from "../edition/fields/BooleanField";
import FormLevelError from "../../utils/editionRelated/FormLevelError";
import FormInfo from "../../utils/editionRelated/FormInfo";
import { getLatestApiReadData } from "../../hooks/useGlobalState";

const userInfoFormLevelErrors = [
  new FormLevelError(
    ["allow_sharing_personal_info"],
    (sharingAllowed) => {
      const userData = getLatestApiReadData("userData-one");
      const { owner_level: userLevel } = userData;
      return userLevel > 0 && !sharingAllowed;
    },
    "Les modérateurs, membres de la DRI ainsi que les administrateurs du site sont forcés d'avoir un profil publique."
  ),
];

function UserInfoForm() {
  return (
    <>
      <BooleanField
        label="Autorisation de partage"
        fieldMapping="allow_sharing_personal_info"
        required
        labelIfTrue="(Le partage est activé)"
        labelIfFalse="(Le partage est désactivé)"
      />

      <TextField label="Pseudo" maxLength={12} fieldMapping="pseudo" required />

      <TextField
        label="Adresse email secondaire"
        maxLength={300}
        fieldMapping="secondary_email"
        required={false}
        // TODO check that it is an email
      />
    </>
  );
}

export default new FormInfo(
  "users",
  UserInfoForm,
  userInfoFormLevelErrors,
  "REX-DRI—PRIVATE"
);
