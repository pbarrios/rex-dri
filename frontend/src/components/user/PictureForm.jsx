import React from "react";
import TextField from "../edition/fields/TextField";
import FileField from "../edition/fields/FileField";
import FormInfo from "../../utils/editionRelated/FormInfo";

function PictureForm() {
  return (
    <>
      <FileField type="picture" label="Image" fieldMapping="file" required />
      <TextField label="Titre" maxLength={200} fieldMapping="title" required />

      <TextField
        label="Description"
        maxLength={500}
        fieldMapping="description"
        required={false}
      />

      <TextField
        label="License"
        maxLength={100}
        fieldMapping="licence"
        required={false}
      />
    </>
  );
}

export default new FormInfo("pictures", PictureForm);
