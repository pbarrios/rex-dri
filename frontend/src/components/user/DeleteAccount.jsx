import React, { useCallback } from "react";
import PropTypes from "prop-types";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import Button from "@material-ui/core/Button";
import BaseMarkdown from "../common/markdown/BaseMarkdown";
import {
  useApiCreate,
  useApiDelete,
  useApiInvalidateOne,
} from "../../hooks/wrappers/api";

const deleteMessage = `
# Attention

* Cette action entrainera la suppression de toute vos données sous licence « REX-DRI Private » 
(login UTC, adresses mails, pseudos, listes privées, réglages personnels, etc.),
* Afin de satisfaire les exigences de la licence « REX-DRI BY », votre nom et prénom seront conservés,
* Pour toute demande supplémentaire, merci de contacter le SIMDE.
* La suppression de votre compte sera effective **sous 48h**.

*La suppression d'un compte consiste donc au vidage des informations personnelles associées.*

# Rappel

* Cette action sera de toute manière automatiquement réalisée après 5 ans d'inactivité de votre compte.

*Si vous souhaitez définitivement supprimer votre compte, cliquez sur le bouton ci-dessous.*
`;

// TODO test if still works
function DeleteAccount(props) {
  const { deleteProgrammed } = props;

  const invalidateUser = useApiInvalidateOne("users");

  const deleteAccountInt = useApiCreate("emptyUserAccount");
  const deleteAccount = useCallback(() => {
    deleteAccountInt({}, () => invalidateUser());
  }, []);

  const undoDeleteInt = useApiDelete("emptyUserAccount");
  const undoDelete = useCallback(() => {
    undoDeleteInt("osef", () => {
      invalidateUser();
    });
  }, []);

  if (deleteProgrammed) {
    return (
      <>
        <Button
          fullWidth
          color="primary"
          variant="contained"
          onClick={undoDelete}
        >
          La suppression de votre compte interviendra sous 48h. Pour annuler le
          processus, cliquez sur ce bouton.
        </Button>
      </>
    );
  }
  return (
    <>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="caption">Supprimer mon compte</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{ display: "block" }}>
          <BaseMarkdown source={deleteMessage} headingOffset={3} />
          <Button fullWidth variant="outlined" onClick={deleteAccount}>
            Je demande la suppression de mon compte
          </Button>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </>
  );
}

DeleteAccount.propTypes = {
  deleteProgrammed: PropTypes.bool.isRequired,
};

export default DeleteAccount;
