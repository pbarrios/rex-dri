import React, { useCallback, useMemo } from "react";
import PropTypes from "prop-types";

import TextField from "@material-ui/core/TextField";

import Field from "./Field";
import CustomError from "../../common/CustomError";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

/**
 * Form field for numbers.
 * You can specify a `minValue` and a `maxValue` through the props.
 */
function NumberField({
  fieldMapping,
  required,
  label,
  comment,
  minValue,
  maxValue,
}) {
  const getError = useCallback((v) => {
    const messages = [];
    if (required && (v === "" || v === null)) {
      messages.push("Ce champ est requis mais il est vide.");
    }
    if (maxValue && v > maxValue) {
      messages.push(`Le nombre inscrit est trop grand (>${maxValue}).`);
    }
    if (minValue && v < minValue) {
      messages.push(`Le nombre inscrit est trop petit (<${minValue}).`);
    }
    return new CustomError(messages);
  }, []);

  const serializeFromField = useCallback((v) => {
    const parsed = parseFloat(v);
    if (isNaN(parsed)) return null;
    return parsed;
  }, []);

  const [number, setNumberInt, error] = useField(fieldMapping, {
    defaultNullValue: null,
    serializeFromField,
    getError,
  });

  const setNumber = useCallback((e) => {
    const v = e.target.value;
    const regex = /[^\d.-]*/gi;
    setNumberInt(v.replace(regex, ""));
  }, []);

  const extraInputProps = useMemo(() => {
    const out = {};
    if (minValue || minValue === 0) out.min = minValue;
    if (maxValue || maxValue === 0) out.max = maxValue;
    return out;
  }, []);

  return (
    <>
      <FieldWrapper
        required={required}
        errorObj={error}
        label={label}
        commentText={comment}
      >
        <TextField
          placeholder="Le champ est vide"
          fullWidth
          multiline={false}
          value={number}
          onChange={setNumber}
          type="number"
          inputProps={extraInputProps}
        />
      </FieldWrapper>
    </>
  );
}

NumberField.defaultProps = {
  ...Field.defaultProps,
  maxValue: null,
  minValue: null,
};

NumberField.propTypes = {
  ...Field.propTypes,
  maxValue: PropTypes.number,
  minValue: PropTypes.number,
};

export default NumberField;
