import React, { useCallback } from "react";

import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

import frLocale from "date-fns/locale/fr";
import format from "date-fns/format";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import dateToDateStr from "../../../utils/dateToDateStr";
import Field from "./Field";
import CustomError from "../../common/CustomError";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

/**
 * Class to customize the header of the date selection box
 */
class LocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date) {
    return format(date, "d MMM yyyy", { locale: this.locale });
  }
}

/**
 * Form field for dates
 */
function DateField({ fieldMapping, required, label, comment }) {
  const getError = useCallback((v) => {
    const messages = [];
    if (required && !v) {
      messages.push("Date requise.");
    }

    return new CustomError(messages);
  });
  const [date, setDateInt, error] = useField(fieldMapping, {
    serializeFromField: dateToDateStr,
    getError,
  });

  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
        <DatePicker
          clearable
          format="d MMM yyyy"
          value={date}
          onChange={setDateInt}
          clearLabel="vider"
          cancelLabel="annuler"
          leftArrowIcon={<KeyboardArrowLeftIcon />}
          rightArrowIcon={<KeyboardArrowRightIcon />}
        />
      </MuiPickersUtilsProvider>
    </FieldWrapper>
  );
}

DateField.defaultProps = {
  ...Field.defaultProps,
};

DateField.propTypes = {
  ...Field.propTypes,
};

export default DateField;
