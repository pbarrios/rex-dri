import React, { useCallback } from "react";
import PropTypes from "prop-types";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Field from "./Field";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

/**
 * Form field for a simple boolean switch field
 */
function BooleanField({
  fieldMapping,
  required,
  label,
  comment,
  labelIfTrue,
  labelIfFalse,
}) {
  const [checked, setCheckedInt, error] = useField(fieldMapping);
  const setChecked = useCallback((e) => {
    setCheckedInt(e.target.checked);
  }, []);

  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      <FormControlLabel
        control={
          <Switch checked={checked} onChange={setChecked} color="primary" />
        }
        label={checked ? labelIfTrue : labelIfFalse}
      />
    </FieldWrapper>
  );
}

BooleanField.defaultProps = {
  ...Field.defaultProps,
  labelIfTrue: "",
  labelIfFalse: "",
};

BooleanField.propTypes = {
  ...Field.propTypes,
  labelIfTrue: PropTypes.string,
  labelIfFalse: PropTypes.string,
};

export default BooleanField;
