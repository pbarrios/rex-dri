import React, { useCallback, useMemo } from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";

import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";

import DeleteIcon from "@material-ui/icons/Delete";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Typography from "@material-ui/core/Typography";
import Field from "./Field";

import stringIsUrl from "../../../utils/stringIsUrl";
import CustomError from "../../common/CustomError";
import useField from "../../../hooks/useField";
import FieldWrapper from "./FieldWrapper";

/**
 * Form field to render the useFullLinks field that is present in many models.
 *
 * Has to props parameters: `urlMaxLength` and `descriptionMaxLength`
 */
function UsefulLinksField({
  fieldMapping,
  required,
  label,
  comment,
  urlMaxLength,
  descriptionMaxLength,
}) {
  const getError = useCallback((usefulLinks) => {
    const messages = [];

    if (required && usefulLinks.length === 0) {
      messages.push("Au moins un lien doit être présent");
    }

    const hasEmpty = usefulLinks.some(
      (el) =>
        (el.url !== "" && el.description === "") ||
        (el.url === "" && el.description !== "")
    );
    if (hasEmpty) {
      messages.push("Les deux champs url et description doivent être saisis.");
    }

    const hasInvalidUrl = usefulLinks.some((el) => !stringIsUrl(el.url));
    if (hasInvalidUrl) {
      messages.push("Une URL n'est pas reconnu dans le formulaire.");
    }

    const hasTooLongUrlOrDesc = usefulLinks.some(
      (el) =>
        (el.url && el.url.length > urlMaxLength) ||
        (el.description && el.description.length > descriptionMaxLength)
    );
    if (hasTooLongUrlOrDesc) {
      messages.push("Une URL ou une description est trop longue.");
    }

    return new CustomError(messages);
  }, []);

  const [value, setValue, error] = useField(fieldMapping, {
    getError,
  });

  const allUseFulLinksAreUsed = useMemo(
    () => value.every((el) => el.url !== "" && el.description !== ""),
    [value]
  );

  /**
   *
   * User interaction handlers
   *
   */

  const handleUsefulLinkUrlChange = useCallback(
    (idx, evt) => {
      const newUsefulLinks = value.map((usefulLink, indexOfChange) => {
        if (idx !== indexOfChange) {
          return usefulLink;
        }
        return {
          ...usefulLink,
          url: evt.target.value,
        };
      });

      setValue(newUsefulLinks);
    },
    [value]
  );

  const handleUsefulLinkDescriptionChange = useCallback(
    (idx, evt) => {
      const newUsefulLinks = value.map((usefulLink, indexOfChange) => {
        if (idx !== indexOfChange) {
          return usefulLink;
        }
        return {
          ...usefulLink,
          description: evt.target.value,
        };
      });

      setValue(newUsefulLinks);
    },
    [value]
  );

  const handleAddUsefulLink = useCallback(() => {
    const newValue = [
      ...value,
      {
        url: "",
        description: "",
      },
    ];
    setValue(newValue);
  }, [value]);

  const handleSwapUsefulLink = useCallback(
    (idx, swapIndexDiff) => {
      const newUsefulLinks = value.slice();

      const b = value[idx + swapIndexDiff];
      newUsefulLinks[idx + swapIndexDiff] = newUsefulLinks[idx];
      newUsefulLinks[idx] = b;
      setValue(newUsefulLinks);
    },
    [value]
  );

  const handleRemoveUsefulLink = useCallback(
    (idx) => {
      setValue(value.filter((s, idxToRemove) => idx !== idxToRemove));
    },
    [value]
  );

  /**
   *
   * Enf of User interaction handlers
   *
   */

  const nbOfUsefulLinks = value.length;
  return (
    <FieldWrapper
      required={required}
      errorObj={error}
      label={label}
      commentText={comment}
    >
      <Typography variant="caption">
        Tous les champs doivent être remplis et les URLs doivent commencer par
        'http' ou 'https' ou 'ftp'.
      </Typography>
      <Grid
        container
        spacing={2}
        direction="column"
        justify="flex-start"
        alignItems="flex-start"
      >
        {value.map((usefulLink, idx) => (
          // eslint-disable-next-line react/no-array-index-key
          <Grid item key={idx}>
            <Grid container spacing={1} alignItems="center">
              <Grid item>
                <TextField
                  placeholder="URL"
                  value={usefulLink.url}
                  onChange={(e) => handleUsefulLinkUrlChange(idx, e)}
                />
              </Grid>

              <Grid item>
                <TextField
                  placeholder="Description"
                  value={usefulLink.description}
                  onChange={(e) => handleUsefulLinkDescriptionChange(idx, e)}
                />
              </Grid>

              <Grid item>
                <IconButton
                  onClick={() => handleSwapUsefulLink(idx, -1)}
                  disabled={idx === 0}
                  color="secondary"
                >
                  <KeyboardArrowUpIcon />
                </IconButton>
                <IconButton
                  onClick={() => handleSwapUsefulLink(idx, 1)}
                  disabled={idx === nbOfUsefulLinks - 1}
                  color="secondary"
                >
                  <KeyboardArrowDownIcon />
                </IconButton>
                <IconButton
                  onClick={() => handleRemoveUsefulLink(idx)}
                  color="secondary"
                >
                  <DeleteIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        ))}
        <Grid item>
          <Button
            disabled={!allUseFulLinksAreUsed}
            size="small"
            variant="outlined"
            color="secondary"
            onClick={handleAddUsefulLink}
          >
            Ajouter un lien
          </Button>
        </Grid>
      </Grid>
    </FieldWrapper>
  );
}

UsefulLinksField.defaultProps = {
  ...Field.defaultProps,
  urlMaxLength: 300,
  descriptionMaxLength: 50,
  label: "Lien(s) utile(s) (ex : vers ces informations)",
  fieldMapping: "useful_links",
};

UsefulLinksField.propTypes = {
  ...Field.propTypes,
  urlMaxLength: PropTypes.number.isRequired,
  descriptionMaxLength: PropTypes.number.isRequired,
};

export default UsefulLinksField;
