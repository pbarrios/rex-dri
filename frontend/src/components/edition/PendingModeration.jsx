import React, { useCallback, useMemo } from "react";

import PropTypes from "prop-types";
import compose from "recompose/compose";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/styles";
import RequestParams from "../../utils/api/RequestParams";
import FullScreenDialogService from "../../services/FullScreenDialogService";
import FullScreenDialogFrame from "../common/FullScreenDialogFrame";
import withNetworkWrapper, { NetWrapParam } from "../../hoc/withNetworkWrapper";
import { useApiInvalidateAll } from "../../hooks/wrappers/api";
import useOnComponentUnMount from "../../hooks/useOnComponentUnMount";

const useStyles = makeStyles((theme) => ({
  editButton: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

function PendingModerationInfo({ userCanModerate, moderate, editFromPending }) {
  const classes = useStyles();

  return (
    <>
      <Button
        variant="contained"
        color="primary"
        className={classes.editButton}
        onClick={editFromPending}
      >
        Éditer à partir de cette version
      </Button>
      {!userCanModerate && (
        <Typography variant="caption" align="center">
          Vous n'avez pas les droits nécessaires pour modérer cet élément.
        </Typography>
      )}
      <Button
        variant="outlined"
        color="primary"
        disabled={!userCanModerate}
        className={classes.editButton}
        onClick={moderate}
      >
        Valider cette version
      </Button>
    </>
  );
}

PendingModerationInfo.propTypes = {
  userCanModerate: PropTypes.bool.isRequired,
  moderate: PropTypes.func.isRequired,
  editFromPending: PropTypes.func.isRequired,
};

function PendingModeration({
  pendingModeration,
  editFromPendingModeration,
  renderTitle,
  renderCore,
  userCanModerate,
}) {
  const resetData = useApiInvalidateAll("pendingModerationObj");

  useOnComponentUnMount(() => {
    resetData();
  });

  const pendingModelData = useMemo(() => {
    const pending = pendingModeration[0];
    return {
      ...pending.new_object,
      id: pending.object_id,
    };
  }, [pendingModeration]);

  const moderate = useCallback(() => {
    editFromPendingModeration(pendingModelData, true); // open editor and auto save
  }, [pendingModelData]);

  const editFromPending = useCallback(() => {
    editFromPendingModeration(pendingModelData);
  }, [pendingModelData]);

  return (
    <FullScreenDialogFrame
      handleCloseRequest={FullScreenDialogService.closeDialog}
      title="Version en attente de modération"
    >
      <PendingModerationInfo
        userCanModerate={userCanModerate}
        moderate={moderate}
        editFromPending={editFromPending}
      />
      <Divider />
      <br />
      {renderTitle(pendingModelData)}
      {renderCore(pendingModelData)}
    </FullScreenDialogFrame>
  );
}

PendingModeration.propTypes = {
  modelInfo: PropTypes.shape({
    contentTypeId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  editFromPendingModeration: PropTypes.func.isRequired,
  userCanModerate: PropTypes.bool.isRequired,
  pendingModeration: PropTypes.array.isRequired,
  renderTitle: PropTypes.func.isRequired,
  renderCore: PropTypes.func.isRequired,
};

const buildParams = (modelInfo) => {
  const { contentTypeId, id } = modelInfo;
  return RequestParams.Builder.withEndPointAttrs([contentTypeId, id]).build();
};

export default compose(
  withNetworkWrapper([
    new NetWrapParam("pendingModerationObj", "all", {
      addDataToProp: "pendingModeration",
      params: (props) => buildParams(props.modelInfo),
      propTypes: {
        modelInfo: PropTypes.shape({
          contentTypeId: PropTypes.number.isRequired,
          id: PropTypes.number.isRequired,
        }).isRequired,
      },
    }),
  ])
)(PendingModeration);
