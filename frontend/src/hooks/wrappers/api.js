import { useCallback, useMemo } from "react";
import useGlobalReducer from "../useGlobalReducer";
import CrudReducers from "../../utils/api/CrudReducers";
import CrudActions from "../../utils/api/CrudActions";
import RequestParams from "../../utils/api/RequestParams";

/**
 * Hook to use the API, provides several actions
 *
 * @param {string} route
 * @param {"one"|"all"} variant
 * @returns {[*, function, CrudActions]}
 */
export function useApi(route, variant) {
  const [reducer, defaultState] = useMemo(() => {
    const internal = new CrudReducers(route);
    if (variant === "all")
      return [internal.getForAll(), CrudReducers.defaultAllState];
    return [internal.getForOne(), CrudReducers.defaultOneState];
  }, []);

  const [state, dispatch] = useGlobalReducer(
    `api-${route}-${variant}`,
    reducer,
    defaultState
  );

  const actions = useMemo(() => new CrudActions(route), []);

  return [state, dispatch, actions];
}

/**
 * Wrapper around useApi: only to read data
 *
 * @param route
 * @param variant
 * @returns {[{}, function]}
 */
export function useApiRead(route, variant) {
  const [data, dispatch, actions] = useApi(route, variant);

  const action = useMemo(
    () => (variant === "all" ? actions.readAll : actions.readOne),
    []
  );

  const read = useCallback((newParams) => {
    dispatch(action(newParams));
  }, []);

  return useMemo(() => [data, read], [data]);
}

/**
 * Private helper to invalidate
 *
 * @private
 * @param route
 * @param variant
 * @returns {Function}
 */
function _useApiInvalidate(route, variant) {
  const [, dispatch, actions] = useApi(route, variant);

  const action = useMemo(
    () => (variant === "all" ? actions.invalidateAll : actions.invalidateOne),
    []
  );
  return useCallback(() => {
    dispatch(action());
  }, []);
}

/**
 * Wrapper to invalidate "one" endpoint
 * @param route
 * @returns {function(): void}
 */
export function useApiInvalidateOne(route) {
  return _useApiInvalidate(route, "one");
}

/**
 * Wrapper to invalidate "all" endpoint
 * @param route
 * @returns {function(): void}
 */
export function useApiInvalidateAll(route) {
  return _useApiInvalidate(route, "all");
}

/**
 * Private helper to delete
 * @param route
 * @private
 */
function _useDeleteAction(route) {
  const [, dispatch, actions] = useApi(route, "one");
  return useCallback((params) => dispatch(actions.delete(params)), []);
}

/**
 * Hook that can be used to delete api instance
 *
 * @param {string} route
 * @returns {Function}
 */
export function useApiDelete(route) {
  const performDelete = _useDeleteAction(route);
  const invalidateOne = useApiInvalidateOne(route);
  const invalidateAll = useApiInvalidateAll(route);

  const deleteOne = useCallback((id, onSuccess = () => {}) => {
    performDelete(
      RequestParams.Builder.withId(id)
        .withOnSuccessCallback((any) => {
          // auto invalidate data
          invalidateAll();
          invalidateOne();
          // Call on success callback
          onSuccess(any);
        })
        .build()
    );
  }, []);

  return useMemo(() => deleteOne, [deleteOne]);
}

/**
 * Hook that can be used to create api instance
 *
 * @param {string} route
 * @returns {Function}
 */
export function useApiCreate(route) {
  const [, dispatch, actions] = useApi(route, "one");
  const invalidateOne = useApiInvalidateOne(route);
  const invalidateAll = useApiInvalidateAll(route);

  const createOne = useCallback(
    (data, onSuccess = () => {}) => {
      dispatch(
        actions.create(
          RequestParams.Builder.withData(data)
            .withOnSuccessCallback((any) => {
              invalidateOne();
              invalidateAll();
              onSuccess(any);
            })
            .build()
        )
      );
    },
    [dispatch]
  );

  return useMemo(() => createOne, [createOne]);
}

/**
 * Hook that can be used to update an api instance
 *
 * @param {string} route
 * @returns {Function}
 */
export function useApiUpdate(route) {
  const [, dispatch, actions] = useApi(route, "one");
  const invalidateOne = useApiInvalidateOne(route);
  const invalidateAll = useApiInvalidateAll(route);

  const updateOne = useCallback(
    (id, data, onSuccess = () => {}) => {
      dispatch(
        actions.update(
          RequestParams.Builder.withData(data)
            .withId(id)
            .withOnSuccessCallback((any) => {
              invalidateOne();
              invalidateAll();
              onSuccess(any);
            })
            .build()
        )
      );
    },
    [dispatch]
  );

  return useMemo(() => updateOne, [updateOne]);
}
