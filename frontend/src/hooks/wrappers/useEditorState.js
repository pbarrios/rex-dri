import { useMemo } from "react";
import { getLatestRead } from "../../utils/api/utils";
import CrudReducers from "../../utils/api/CrudReducers";
import useGlobalState from "../useGlobalState";

/**
 * Function to create the mapStateToProps function for editor in a "generic way"
 *
 * @export
 * @param {string} route
 * @returns
 */
function useEditorState(route) {
  const subKey = `${route}-one`;

  const [state] = useGlobalState(`api-${subKey}`, CrudReducers.defaultOneState);

  let lastUpdateTimeInModel = null;
  let hasPendingModeration = null;

  // We make sure to get the latest data
  const latestRead = getLatestRead(state);
  if (latestRead.readAt !== 0) {
    lastUpdateTimeInModel = latestRead.data.updated_on;
    hasPendingModeration = latestRead.data.has_pending_moderation;
  }

  // simple way to retreive the save error
  const { updateFailed, createFailed } = state;

  let savingHasError = updateFailed;
  if (createFailed.failed) {
    savingHasError = createFailed;
  }

  return useMemo(
    () => ({
      savingHasError,
      lastUpdateTimeInModel,
      hasPendingModeration,
    }),
    [savingHasError, lastUpdateTimeInModel, hasPendingModeration]
  );
}

export default useEditorState;
