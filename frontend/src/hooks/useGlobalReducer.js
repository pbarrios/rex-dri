import { useCallback } from "react";
import useGlobalState, { reduceGlobalState } from "./useGlobalState";

/**
 * Hook that provides reducer based globalState access / modification
 *
 * @param {string} key - id for the state
 * @param {function} reducer - Reducer to be used
 * @param {*} initialState - initial state value (if not already stored)
 */
function useGlobalReducer(key, reducer, initialState) {
  // useGlobalState will automatically handle the subscription
  const [sharedState] = useGlobalState(key, initialState);

  const dispatchNotFunction = useCallback((action) => {
    // Will update the global state and trigger setUpdate only on the mounted component
    reduceGlobalState(key, reducer, action);
  }, []);

  const dispatchFunction = useCallback(
    (action) => {
      action(dispatchNotFunction);
    },
    [dispatchNotFunction]
  );

  const dispatchOut = useCallback(
    (action) => {
      // built-in support for action that take dispatch as argument
      if (typeof action === "function") {
        dispatchFunction(action);
      } else {
        dispatchNotFunction(action);
      }
    },
    [dispatchFunction, dispatchNotFunction]
  );

  return [sharedState, dispatchOut];
}

export default useGlobalReducer;
