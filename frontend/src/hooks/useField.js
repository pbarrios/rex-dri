import { useCallback, useContext, useMemo, useState } from "react";
import FormContext from "../contexts/FormContext";
import CustomError from "../components/common/CustomError";
import useOnBeforeComponentMount from "./useOnBeforeComponentMount";

/**
 *
 * @param {string} fieldMapping
 * @param {*} defaultNullValue - value used when the inner value is null
 * @param {function(*):*} serializeFromField - method used to transform the field value before setting it
 * @param {function(*):CustomError} getError - method that takes the serialized value and tells if there is an error with it
 * @returns {[*, function(*), CustomError]}
 */
function useField(
  fieldMapping,
  {
    defaultNullValue = undefined,
    serializeFromField = (v) => v,
    // eslint-disable-next-line no-unused-vars
    getError = (v) => new CustomError([]),
  } = {}
) {
  const { formManager } = useContext(FormContext);
  const [valueInt, setValueInt] = useState(
    formManager.getInitialValueForMapping(fieldMapping)
  );

  const [allErrors, setAllErrors] = useState(getError(valueInt));

  useOnBeforeComponentMount(() => {
    formManager.fieldSubscribe(fieldMapping, allErrors, setAllErrors);
  });

  const setValue = useCallback((v) => {
    let vToUse = serializeFromField(v);

    // Handle null properly
    if (typeof defaultNullValue !== "undefined") {
      if (v === null) vToUse = defaultNullValue;
    }

    // setTheValue
    setValueInt(vToUse);
    // Propagate to the form manager
    const fieldError = getError(vToUse);
    formManager.fieldUpdated(fieldMapping, vToUse, fieldError);
  }, []);

  // retransform the value if needed
  const outValue = useMemo(() => {
    if (typeof defaultNullValue !== "undefined") {
      if (valueInt === null) return defaultNullValue;
    }
    return valueInt;
  }, [valueInt]);

  return [outValue, setValue, allErrors];
}

export default useField;
