import React from "react";
import ReactDOM from "react-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import OfflineThemeProvider from "../components/common/theme/OfflineThemeProvider";
import Logo from "../components/app/Logo";
import BaseTemplate from "../components/app/BaseTemplate";
import { PageRgpd } from "../components/pages/PagesRgpdCgu";

function MainReactEntry() {
  const toolbarContent = (
    <div style={{ flex: 1 }}>
      <Logo />
    </div>
  );

  return (
    <OfflineThemeProvider>
      <>
        <CssBaseline />
        <main>
          <BaseTemplate toolbarContent={toolbarContent}>
            <PageRgpd />
          </BaseTemplate>
        </main>
      </>
    </OfflineThemeProvider>
  );
}

const wrapper = document.getElementById("app");
ReactDOM.render(<MainReactEntry />, wrapper);

// eslint-disable-next-line no-undef
if (module.hot) {
  // eslint-disable-next-line no-undef
  module.hot.accept();
}
