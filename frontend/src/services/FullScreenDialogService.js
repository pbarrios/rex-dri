/**
 * Manage the full screen dialog
 */
class FullScreenDialogService {
  /**
   * @type {function}
   * @private
   */
  _setChildren = undefined;

  /**
   * @param {function} setChildren
   */
  setSetChildrenComponent = (setChildren) => {
    this._setChildren = setChildren;
  };

  closeDialog = () => {
    if (typeof this._setChildren !== "undefined") {
      this._setChildren(undefined);
    }
  };

  openDialog = (children) => {
    if (typeof this._setChildren !== "undefined") {
      this._setChildren(children);
    }
  };
}

export default new FullScreenDialogService();
