/**
 * Manage the full screen dialog
 */
import APP_ROUTES from "../config/appRoutes";

class NavigationService {
  /**
   * @type {object}
   * @private
   */
  _history = undefined;

  /**
   * TO BE USED ONLY in entry to get the ref to the component
   * @param ref
   */
  setComponent = (ref) => {
    if (ref) this._history = ref.history;
  };

  goHome = () => this._history.push(APP_ROUTES.base);

  goToUniversity = (univId) =>
    this._history.push(APP_ROUTES.forUniversity(univId));

  goToUniversityTab = (univId, tabName) =>
    this._history.push(APP_ROUTES.forUniversityTab(univId, tabName));

  goToLists = () => this._history.push(APP_ROUTES.lists);

  goToRoute = (route) => this._history.push(route);
}

export default new NavigationService();
