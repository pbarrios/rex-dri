/**
 * Manage the alert element
 */
class AlertService {
  /**
   * @type {function}
   * @private
   */
  _setAlertProps = () => {};

  /**
   * @param {function} setAlertProps
   */
  setSetAlertProps = (setAlertProps) => {
    this._setAlertProps = setAlertProps;
  };

  close = () => this._setAlertProps({ open: false });

  open = (props) => {
    this._setAlertProps({ ...props, open: true });
  };
}

export default new AlertService();
