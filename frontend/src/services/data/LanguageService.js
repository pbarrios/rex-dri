import arrayOfInstancesToMap from "../../utils/arrayOfInstancesToMap";
import { getLatestApiReadData } from "../../hooks/useGlobalState";

class LanguageService {
  /**
   * Stores the languages by its id
   * @type {Map<number, object>}
   * @private
   */
  _languagesById = new Map();

  /**
   * @private
   * @type {array.<object>}
   */
  _options = [];

  /**
   * Must be called once before accessing other methods.
   */
  initialize() {
    const languages = this.getLanguages();
    this._languagesById = arrayOfInstancesToMap(languages);

    this._options = languages.map((el) => ({
      value: el.id,
      label: el.name,
    }));
  }

  /**
   * Get the country object for a country id
   *
   * @param {number} countryId
   * @returns {Object}
   */
  getLanguageForLanguageId(countryId) {
    return this._languagesById.get(countryId);
  }

  getLanguageName(code) {
    const language = this.getLanguageForLanguageId(code);
    if (language) return language.name;
    return null;
  }

  /**
   * @returns {Array.<Object>}
   */
  getLanguages() {
    return getLatestApiReadData("languages-all");
  }

  /**
   * @returns {Array<Object>}
   */
  getLanguagesOptions() {
    return this._options;
  }
}

export default new LanguageService();
