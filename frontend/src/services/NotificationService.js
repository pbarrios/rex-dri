/**
 * Manage the full screen dialog
 */

class NotificationService {
  /**
   * @type {function}
   * @private
   */
  _enqueueSnackbar = () => {};

  /**
   * @type {function}
   * @private
   */
  _closeSnackbar = () => {};

  /**
   * @param {function} enqueueSnackbar
   */
  setEnqueueSnackbar = (enqueueSnackbar) => {
    this._enqueueSnackbar = enqueueSnackbar;
  };

  /**
   * @param {function} closeSnackbar
   */
  setCloseSnackbar = (closeSnackbar) => {
    this._closeSnackbar = closeSnackbar;
  };

  notify = (message, option) => this._enqueueSnackbar(message, option);

  info = (message) => this.notify(message, { variant: "info" });

  error = (message) => this.notify(message, { variant: "error" });

  closeNotification = (notificationKey) => this._closeSnackbar(notificationKey);
}

export default new NotificationService();
