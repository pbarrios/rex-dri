#!/usr/bin/env python
import http.server

class MyHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):
    def end_headers(self):
        # Prevent annoying caching of the documentation
        self.send_header("Cache-Control", "no-cache, no-store, must-revalidate")
        self.send_header("Pragma", "no-cache")
        self.send_header("Expires", "0")
        http.server.SimpleHTTPRequestHandler.end_headers(self)


if __name__ == "__main__":
    httpd = http.server.HTTPServer(("0.0.0.0", 5000), MyHTTPRequestHandler)
    httpd.serve_forever()
