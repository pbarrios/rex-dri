# Feature checklist

In case of a big refactoring, here is a "feature checklist" to make sure everything works (this list is frontend centric, as this what the users will experience).

## Checklist

- [ ] Obviously you shouldn't have errors in your console for this entire list.

### IMPORTANT

- [ ] Check GDPR and guidelines need to be agreed

```bash
make shell_backend
# replace 'chehabfl' by your own UTC login
echo "from base_app.models import User; user = User.objects.get(username='chehabfl'); user.has_validated_cgu_rgpd = False; user.save();" | ./manage.py shell
```

_Goto to http://localhost:8000/, and check you need to aggree to the cgu / gdpr notice_.

### Website admin

- [ ] Can create site information / notifications here http://localhost:8000/admin/base_app/siteinformation/add/
- [ ] Notification popup on website login
- [ ] Information visible on all webpages bottom (if level > warning)

### Markdown

- [ ] Markdown renders well
- [ ] Exchange rates work fine in the text

### Home page

- [ ] Notification / information visible in dynamic information

### Map / Search filter

- [ ] All filters work
- [ ] Filter state is preserved when changing page
- [ ] The select multiple goes on top of the map

### Map page

- [ ] Map position and zoom are preserved on page change
- [ ] Popup work fine

### Search page

- [ ] Pagination works
- [ ] Item list works as expected

### List page

- [ ] Check you can delete a list
- [ ] Check you can edit a list
- [ ] Check you can save a list
- [ ] Check you can create a list

### University page

- [ ] Tabs works correctly
- [ ] Filtering on previous departure works as expected
- [ ] Pagination of previous departure works as expected

### Modules

- [ ] Displaying versions works fine
- [ ] Edit from this version works fine
- [ ] Closing editor works
- [ ] Saving works
- [ ] Modules with link edition works (reordering, etc.)

### Scholarships

- [ ] Check you edit scholarship with different settings
- [ ] Check you can scholarship

### Previous exchange

- [ ] Check you can edit your exchange

### Other pages

- [ ] check the info pages work correctly http://localhost:8000/app/about/rgpd/, http://localhost:8000/app/about/project/, http://localhost:8000/app/about/rgpd/
- [ ] Check the theme personalization works fine
- [ ] Check you can edit your personal information
- [ ] Check you can delete your account
- [ ] Check logout works correctly
