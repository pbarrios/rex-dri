# About this documentation

## docsify.js

This markdown based documentation is made available in a nice way using [`docsify`](https://docsify.js.org/).

## Source

The source of the documentation is available in the `documentation` folder from the root of the repository.

## Rendering

The documentation is automatically rendered locally when you run the `make up` command. It is then available on [http://localhost:5000](http://localhost:5000).

To generate or update the UML diagrams, you need to run the command (from the root of the repository): `make documentation`.

## Issues ?

!> If the `search` functionality is not working after you have updated the documentation, it might be because the `localStorage` is not up to to data. You can empty it from your browser developer settings (`F12`).
