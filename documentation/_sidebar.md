<!-- docs/_sidebar.md -->

- Getting started

  - [Introduction](GettingStarted/introduction.md)
  - [Set-up](GettingStarted/set-up.md)
  - [Initializing data](GettingStarted/init_data.md)
  - [IDE Setup](GettingStarted/IDE_setup.md)

* Application documentation

  - Backend

    - [Architecture](Application/Backend/architecture.md)
    - [Models, Serializers and ViewSets](Application/Backend/models_serializers_viewsets.md)
    - [Config files](Application/Backend/config_files.md)
    - [Moderation & versioning](Application/Backend/moderation_and_versioning.md)
    - [Data validation](Application/Backend/data_validation.md)
    - [API](Application/Backend/API.md)
    - [External data](Application/Backend/external_data.md)
    - [Tests](Application/Backend/tests.md)

  - Frontend

    - [React basics](Application/Frontend/react.md)
    - [Global State Handling](Application/Frontend/global_state.md)
    - [Interacting with the backend API](Application/Frontend/interacting_with_backend.md)
    - [Services](Application/Frontend/services.md)
    - [Tests](Application/Frontend/tests.md)
    - [Map](Application/Frontend/map.md)
    - [Troubleshooting](Application/Frontend/troubleshooting.md)

* Prod related

  - Going in prod

    - [Feature checklist](ProdRelated/feature_checklist.md)
    - [Releasing](ProdRelated/releasing.md)

  - [Architecture](ProdRelated/architecture.md)
  - [Dump database](ProdRelated/dump_database.md)

  - Maintenance
    - [Linking partners / universities](ProdRelated/maintenance/linking_universities.md)
    - [Update major minor](ProdRelated/maintenance/update_major_minor.md)

* Comments about technologies used

  - [Use of `Docker`](Technologies/docker.md)
  - [Debugger](Technologies/debugging.md)

* Other

  - [About this documentation](Other/this_doc.md)
  - [Contributions](Other/contributions.md)
  - [Credits](Other/credits.md)
