# Initializing data in the app

You have to options to initialize data in the app:

- From a production database backup,
- From dummy data.

## From a production database backup

If you have access to [a production database backup](ProdRelated/dump_database), follow these instructions:

- Download the dump, and move it to the location: `/tmp/rex-dri.dump.gpg`
- decrypt it (**You will need a passphrase for this.**):

```bash
cd /tmp && gpg-zip --decrypt rex-dri.dump.gpg
test -f /tmp/rex-dri.dump && echo "Database dump decrypted"
```

- Restore the backup (run the following inside the rex-dri root folder on your computer)

```bash
# Down everything and re-up only the database
docker-compose down
docker-compose up database
```

```bash
# wait for the database to be up and in another shell
# Delete the existing database and restore the database
docker-compose exec database dropdb -U postgres postgres
docker-compose exec database createdb -U postgres postgres
docker-compose exec database rm -rf /tmp/rex-dri.dump
docker cp /tmp/rex-dri.dump rex-dri_database_1:/tmp/rex-dri.dump

docker-compose exec database pg_restore -U postgres -d postgres /tmp/rex-dri.dump

# Down everything
docker-compose down
```

- If you (identified by your CAS account) are not a admin user on the website, do the following:

```bash
# Restart and set superuser
make dev
make shell_backend
# replace 'chehabfl' by your own UTC login
echo "from base_app.models import User; user = User.objects.get(username='chehabfl'); user.is_superuser = True; user.is_staff = True; user.save();" | ./manage.py shell
```

_N.B. If the last part fails, it's because you have never connected REX-DRI before the dump :crying:. Open [http://localhost:8000/app/](http://localhost:8000/app/), connect with the CAS, and retry the last command._

## With dummy data

To load some basic data in the app, you can use the python classes: `loadCountries`, `loadUniversities`, etc. available in `backend/backend_app/load_data/loading_scripts`.

You can load everything in one command:

```bash
make init_dev_data
```

or by connecting to the `Django` shell:

```bash
make django_shell
```

And by running:

```python
from backend_app.load_data.load_all import load_all
load_all()
```

_NB: Those scripts are tested so they should work._

By doing so, an admin user is created according to the environment variables `DJANGO_ADMIN_USERNAME` and `DJANGO_ADMIN_PASSWORD` that are set (in our case) in the `docker-compose` file.
