# IDE setup

The project was initially developed with VScode but can obviously be enhanced with the IDE of your choice. At the time of this writing, the use of JetBrains IDEs is highly recommended as they are really powerful IDEs (**if you are a student you can get their _ultimate_ versions for free**).

In this short documentation, some configurations "issues" regarding your choice of IDE is addressed. Also, don't miss the separate documentation about the configuration of your [IDE for debugging](Technologies/debugging.md).

?> :information_desk_person: JetBrains IDEs are cranked with features and plugin which can make them a bit slow on some system. More than 75% of those plugin are completely useless for the project so you should dive into the settings and deactivate them.

!> In JetBrain IDEs you should also deactivate _auto-save_ since this would cause useless recompilation of the frontend (or useless restart of the backend). Check [this link](https://intellij-support.jetbrains.com/hc/en-us/community/posts/207054215-Disabling-autosave).

!> In JetBrain IDEs **You must disable `Use "safe write"...` to make sure changes are detected and the project automatically recompiles.**

## For the backend

For the backend the use of [`PyCharm`](https://www.jetbrains.com/pycharm/?fromMenu) is recommended.

In this case you only need to configure the correct python interpreter:

- Open the settings (`CTRL + Alt + S`) go to `project: backend` and `Project interpreter`.
- Select `Docker` and put `registry.gitlab.utc.fr/rex-dri/rex-dri/backend:latest` for the image name.

This will give PyCharm access to the project python's dependencies.

Further configuration is required to run the project (optionally in debug mode) from the IDE. This is documented [here](Technologies/debugging.md).

## For the frontend

For the frontend the use of [`WebStorm`](https://www.jetbrains.com/webstorm/?fromMenu) is recommended.

No special configuration is required, except you might:

- Need to install `Node.js` on your system and `yarn` (`npm install -g yarn`)
- You should install the Node.js dependencies locally (they are in the frontend docker image, but they can't be used through the IDE this time). So run `yarn install`.

## For general purposes

`VS Code` is still a really nice general purposes IDE.
