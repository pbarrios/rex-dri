# Debugging

:hospital: Being able to debug easily a program is essential for a smooth developer experience. :hospital:

Using `console.log(...)` or `print(...)` should be a thing of the past as it makes us inefficient. Hopefully the `rex-dri` project is _debugger_-friendly and in this section we will see how to set-up those awesome tools! :tada:

## Backend

### Using PyCharm

#### Simple run

Before diving into the configuration for debugging we need to configure the project to be able to run it directly from the IDE.

- In the menu, click `run` -> `add configuration`
- Select `Django server`, and give the configuration a thoughtful name :smile:.
- Change the host to `0.0.0.0` and the port to `18000` (a port that should be free on the container).
- (The python interpreter should be a _remote_ docker one, configured in the project settings, see [here](GettingStarted/IDE_setup.md))
- Then you need to edit the `Docker container settings`:

  - Set the `network mode` to `rex-dri_backend-db` (the network that should be created by docker-compose, this is so that we can access be on the same virtual network of the databse)
  - Add one entry in `Port bindings`:
    - _Container port_: `18000`
    - _Protocol_: `tcp`
    - _Host IP_: _(nothing)_
    - _Host Port_: `18081` _(or the one you prefer and that is free)_
  - Finally set the volume binding as follow:
    - _Container path_: `/usr/src/app`
    - _Host path_: `/home/xxxxx/git/rex-dri/` (or wherever is the **root** of the project: the whole repository is needed for the backend to work)

  At the end the settings line should look like something like this: `--net="rex-dri_backend-db" -p :18081:18000/tcp -v /home/xxxxx/git/rex-dri/:/usr/src/app`

:tada:

?> :information_desk_person: So if you run this, the website will be accessible on url: [http://localhost:18081/](http://localhost:18081/). Make sure to have made a `make up` before (or at least a `docker-compose up database frontend` to have only the database and frontend services up and running). With `make up` you will have 2 backends running (one on port `8000` and one on port `18081`); but that's fine.

?> :information_desk_person: If for some reasons you have errors when starting the server from PyCharm, try to remove all your remote interpreters and add the docker-based interpreter again.

#### Debugging the app

Once you have followed the previous setup, you can just put breakpoints in the code and lunch a debugging session. :tada:

#### Debugging the tests

The backend tests uses `pytest` so first we need to deactivate the default Django test runner:

- Open the settings -> `Languages & Frameworks` -> `Django`
- Check `Do not use Django test runner`
- Also in `Tools` -> `Python intgrated tools` set the _default test runner_ to `pytest`.

Make sure you install the `EnvFile` plugin (from the IDE settings).

Then:

- In the menu, click `run` -> `add configuration`
- Edit the **default** pytest template in `templates` -> `Python tests` -> `pytest`.
- In the `EnvFile` tab add two entries for `db.env` and `django.env` that are in the `server/envs` directory in the repo.
- In the `Configuration` tab make sure the correct remote interpreter is selected, then set the `Docker container settings` as follow:
  - Set the `network mode` to `rex-dri_backend-db` (the network that should be created by docker-compose, this is so that we can access be on the same virtual network of the databse)
  - Finally set the volume binding as follow:
    - _Container path_: `/usr/src/app`
    - _Host path_: `/home/xxxxx/git/rex-dri/` (or wherever is the **root** of the project: the whole repository is needed for the backend to work)

At the end the settings line should look like something like this: `--net="rex-dri_backend-db" -v /home/xxxxx/git/rex-dri/:/usr/src/app`.

You can now add breakpoints and run the tests (or a specific one) in debug mode. :tada:

## Frontend app

Using a debugger for the frontend is extremely easy.

### Using (only) the debugger of your browser

Steps:

1. Hard-code a breakpoint (or multiple breakpoints) wherever you want with the `debugger;` keyword:

```js
const a = 2;
debugger;
```

_NB: the file should be automatically rebuilt if you have used the `make up` command._

2. Make sure to have your developer console opened `F12` in your browser.
3. As soon as the code with the breakpoint is run, the execution will stopped and you should be brought in the debugger with a lot of information such as the variables currently defined, the _callstack_, etc.

?> :information_desk_person: If you want to debbug what is going on in a `React` component, you should use the [`React Developer Tools`](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) Chrome/Chromium extension. This extension will add another panel in your browser's developer tools to easily navigate React's virtual DOM and your components `props`/`state`.

### Using your IDE

You may want to use your IDE debugging console too. Here are the step to follow depending on your IDE.

#### WebStorm

1. (In the top menu) click `Run` -> `Edit Configurations...` -> `+` (add) -> `JavaScript Debug`
2. Give it meaningful name and in the URL put: `http://localhost:8000/` (remember that our frontend is served through the backend)
3. Click `Apply` and `Ok`.
4. Add a breakpoint somewhere in the code (click next to the line number; you should see a dot :red_circle: next to it when it is added)
5. Then click on the green :bug: next to the play button in the top row (make sure the configuration you just defined is the one selected)
6. Your browser will open and once the breakpoint is hit, you will have access to all the debugging information directly in WebStorm. :tada:

_Don't forget to stop the debug session once you are done._

?> :information_desk_person: If your are using Chromium on linux, in the path of the Chrome browser config, you should be fine with `chromium-browser` value.

---

You may want to debug some tests at some point. Add breakpoints where you want and simply run the test in debug mode: you whould have a green play button on the left of the first line of your test. Click on it and click `Debug ...`. That's all.

#### Visual Studio Code

0. Install the [`Debugger For Chrome` extension](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
1. In the top menu, click `Debug` -> `Add configuration...` and select the `Chrome` environment.
1. Make sure the configuration for `url` points to `http://localhost:8000`. Bellow is an example configuration to be used with `chromium` (tested on Fedora 29).

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "chrome",
      "request": "launch",
      "name": "Launch Chrome against localhost",
      "url": "http://localhost:8000",
      "webRoot": "${workspaceFolder}",
      "runtimeExecutable": "/usr/bin/chromium-browser" // make sure this leads to your chrome installation
    }
  ]
}
```

3. Add breakpoints in your code
4. And lunch it. In your browser, once the code with the breakpoint is executed, the execution will stop and you will be able to debug in VSCode. :tada:
