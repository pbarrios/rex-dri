# Use of `Docker`

## General comment

As said in the introduction, this project makes use of `docker` and `docker-compose`. Their are several `Dockerfile` spraid across the project that creates the required `docker` image.

Then the `docker-compose.yml` file coordinates everything: environment variables, ports, volumes, etc.

You can have a look at those files for more comments.

## Use-case

The `Dockerfile` for the `backend` leads to 2 images (one for a dev environment and one for a production environment). The `Dockerfile` leads to 1 image (containing the node_modules for reproducible builds). All of those images are hosted on [https://gitlab.utc.fr/rex-dri/rex-dri/container_registry](https://gitlab.utc.fr/rex-dri/rex-dri/container_registry) to be easily used in development, CI and production.

## Updating the images on Gitlab

### The hard / standard way

If you are not connected to the registry yet, do it:

```bash
docker login registry.gitlab.utc.fr
```

To update the images stored on the Gitlab repository, run for instance:

```bash
docker build ./backend --compress --tag registry.gitlab.utc.fr/rex-dri/rex-dri/backend-dev:v0.0.0
```

And push it:

```bash
docker push registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.0.0
```

:warning: images should be versioned with meaningful tags.

### The automated way

You can find on [this repo](https://gitlab.utc.fr/rex-dri/ansible/tree/master/build-docker) an `ansible` script that automate the building/pushing of the images from a remote server. Very useful if you have a bad internet connection.

## Updating the images from Gitlab

To do so, run `docker-compose pull`. (this should be automatic on image tag version change)

## Troubleshooting

### Issues with `__pycache__`

If you have issues at some point with `__pycache__` files (or other files generated at runtime), you can :

- Re-own the repo folder (you don't have the right to modify the files generated from whithin a docker container):

```bash
chown -R ${whoami}:${whoami} .
```

- or delete them:

```bash
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | sudo xargs rm -rf
```

## Issues with Django migrations

With the docker setup, Django migrations are created from within the container and as a result those files are own by the docker user.

If you have issues with git regarding the migrations files (i.e. they don't disappear when you change branch), you need to become an owner of the files: `chown -R $(id -u):$(id -g) backend`.
