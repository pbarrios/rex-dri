# Backend tests

_Rex-DRI_ comes in with several _tests_ that you can perform locally using the commands `make test_backend` and `make test_frontend` once the project is up and running (`make up`).

## General words

Testing the backend is handled with a combination of `pytest` and `Django` (and `pytest-django` package).

The tests are found in the `backend_app/tests/` folder and they mostly test the custom behaviors that are implemented in the app such as moderation and versioning.

Also, there are tests regarding some custom validation.

Finally, the initial and example loading data scripts are tested.

## Documentation

Some useful links to get inspired:

- [General information about testing in Django](https://docs.djangoproject.com/fr/2.1/topics/testing/overview/)
- [`pytest` documentation](https://docs.pytest.org/en/latest/)
- [`pytest-django` documentation](https://pytest-django.readthedocs.io/en/latest/)
