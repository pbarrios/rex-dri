# Words relative to _config files_

There exist different kinds of _config files_ throughout the app and all of them are absolutely necessary, so it's good to understand what is their purpose.

## Django config files

Django's main config file is in `backend/base_app/settings/main.py`. This file is a standard Django one and defines many important aspects of the app such as:

- Which packages should be bundled with the app,
- What Middleware to use,
- Custom behaviors depending on if we are in testing, developing or production environment,
- Config to serve the frontend files from the backend,
- Database settings,
- Etc.

Another config file is loaded through the previous one: `backend/base_app/settings/app_settings.py`. It contains app specific settings such as the `CAS` configuration and how moderation is applied.

## Custom config files

### defaults.yaml

Contains default values use across the app (backend and frontend), nothing special to say here.
