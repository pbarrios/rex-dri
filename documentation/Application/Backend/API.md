# API

## Location

The backend `Django` app, with `Django Rest Framework` acts as an API. It is available at the URI `/api`.

An automated documentation is generated and available at the URI `/api-doc`.

## Authentication

Two authentication protocol are currently available and both lead to API response according to the rights of the corresponding user.

### Session

This is the default mode used once you are connected with the `CAS` by going to the URI `/user/login`.

### Token

A token may be associated with a user through the Django admin (`/admin`). To use it you could do:

```bash
curl -X GET http://127.0.0.1:8000/api/country/ -H 'Authorization: Token MyTokenRandomSuperLong'
```
