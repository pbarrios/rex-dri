# Quick intro to React

?> :information_desk_person: `React` is a JavaScript framework used to create rich and interactive user interface. It was introduced by Facebook.

## General idea

When doing web development, you should be familiar with the _DOM_ ([Document Object Model](https://fr.wikipedia.org/wiki/Document_Object_Model)) which is basically a tree translation of the HTML file you send to the browser for rendering.

React is built on a _virtual DOM_ (you can explore it with the [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) extension for chromium-based browsers -- [also available on Firefox](https://addons.mozilla.org/fr/firefox/addon/react-devtools/)). The elements of this _virtual DOM_ are no longer HTML tags; they are real _living_ objects called **components** that can be modified and that can perform multiple actions. Every time an update to is performed, the DOM in your browser is smartly updated by react based on its own internal _virtual DOM_.

Components being elements of tree-like structure, they must have a parent (or be the root node) and they can have as many _children_ components as they want.

## Tutorial

### Javascript

To get familiar with Javascript in general, you can have a look [here](https://gitlab.com/FloChehab/the-missing-semester/-/tree/master/js) and [here](https://gitlab.com/FloChehab/the-missing-semester/-/tree/master/exercices/3.Javascript) for some exercises and links to a good tutorial.

### React

To know more about `React` you should start by doing the official React tutorial [here](https://reactjs.org/tutorial/tutorial.html).
