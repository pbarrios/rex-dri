### Versions

## Next

###### _TBD_

## v2.10.0

###### _14 November 2020_

- [chore] Prevent usage of backfill script (will add inconsistencies if used again), but keep it in the repo.
- [docs] updated prod related content
- [docs] update the linking a partner to a university process

## v2.9.0

###### _14 June 2020_

- [feat] front / back: release the awesome statsPage (2 datasets, custom SQL query, etc.)
- [feat] ci: run black and prettier in ci
- [feat] backend: add feedback type to make differences between course and exchange feedback
- [chore] front: updated wording and information regarding previous exchanges and offers
- [refacto] front: use Material-ui pagination component
- [fix] front: datepickers were broken => new frontend image to prevent the crash (downgraded a dependency)

## v2.7.0

###### _7 June 2020_

- [feat] front / back: added stats view and frontend deps needed for the view
- [feat] Create a custom admin site containing a view to run cron tasks manually.
- [feat] back: add functions and tests to compute daily stats about the contributions
- [feat] back: add backfill of contributions and connections
- [feat] front: add select component for lastVisitedUniversities
- [fix] front: univ form was broken (updated backend serializer)

## v2.6.0

###### _24 May 2020_

- [feat] back: add functions and tests to compute daily stats about the connections to the website
- [feat] back: add a new model LastVisitedUniversity with viewset and related tests
- [feat] front: handle remapping of major and minors in the frontend (especially everything related to GM/GSM/IM and GSU/GU)

## v2.5.0

###### _17 May 2020_

- [feat] add filter to show only currently available destinations
- [feat] filter of available destinations is activated by default if there are enough open destinations
- [feat] front: updated docker image & deps => v2.0.0
- [feat] ci: a bit more performant
- [style] new prettier on all the repo :tada:
- [style] eslint: cleaned code after eslint update
- [fix] clarified cgu / rgpd
- [fix] front: corrected warnings
- [fix] front: remove CoverGallery
- [fix] front: align form in previousDepartureTab
- [fix] front: cleaned chip / avatar after deps update
- [fix] front: Cleaned Logo
- [fix] front deps: clean after removal of postcss
- [fix] main template: invalid HTML, script outside body
- [fix] Makefile: --renew-anon-volumes for docker-compose

## v2.3.0

###### _3 May 2020_

- [feat] add is_destination_open parameter in partner model and add transaction in utils.py
- [fix] only show Read More button when necessary
- [refacto] delete campus an city models
- [refacto] frontend modify API calls so that only the university model is used

## v2.2.0

###### _26 April 2020_

- [docs] updated README and contributors list
- [refacto] backend changed university model to integrate campus and city information
- [feat] add legend for the map
- [feat] add checkbox to only show exchanges with feedback

## v2.1.0

Major release going to production :tada:. Huge frontend changes since la prod release.

###### _17 April 2020_

- [fix] backend ordering on versions endpoint
- [fix] frontend versions display
- [fix] frontend pending moderation and moderate
- [fix] frontend moved services Component up in the tree
- [fix] frontend fullscreen dialog: no more memory leak / setState on unmounted component
- [feat] frontend added hooks: `useOnBeforeComponentMount`, `useOnComponentUnMount`
- [fix / refacto] frontend 'global state' hooks: no more setState on unmounted components.
- [feat] frontend added `Counter` helper class
- [fix] frontend previous departure filter major / minor
- [feat] frontend smarter `withNetworkWrapper` HOC (auto update params on props change)
- [refacto] frontend changed API of `withNetworkWrapper`
- [fix] frontend scholarship default value blocking create

## v2.0.0b

###### _12 Jan 2020_

Basically: big frontend refactoring.

- [refacto] dropped CustomComponentForAPI
- [feat/refacto] Notification Service, FullScreen dialog service (no more redux)
- [frontend linting] Config update, big refactoring to conform
- [prettier] added for frontend
- [update] Frontend dependancies and frontend docker image

## v1.0.2

###### _11 Jan 2020_

- [bug fix] Logout working on mobile ([#150](https://gitlab.utc.fr/rex-dri/rex-dri/issues/150))
- [bug fix] Broken pagination on previous departure page

## v1.0.1

###### _23 Aug 2019_

- [feature] Possible to go back to home on frontend crash
- [bug fix] Frontend was crashing when `majors` was `null`

## v1.0.0

###### _25 July 2019_

- Opening of the app
