from base_app.admin import admin_site
from stats_app.models import DailyConnections, DailyExchangeContributionsInfo


admin_site.register(DailyConnections)
admin_site.register(DailyExchangeContributionsInfo)
