from datetime import datetime, timedelta
from django.utils.timezone import make_aware
from dataclasses import dataclass
from typing import List

from base_app.models import User

from backend_app.models.exchangeFeedback import ExchangeFeedback
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.exchange import Exchange


def get_today_as_datetime() -> datetime:
    now = datetime.now()
    today = now.replace(hour=0, minute=0, second=0, microsecond=1)
    return make_aware(today)


def get_daily_connections(date: datetime) -> int:
    date_to = date + timedelta(days=1)
    nb_connections = User.objects.filter(
        last_login__gte=date, last_login__lt=date_to
    ).count()
    return nb_connections


@dataclass(eq=True, frozen=True)
class ContributionProfile:
    """
    Class for keeping track a profile.
    """

    type: str
    major: str
    minor: str
    exchange_semester: str
    university_pk: int


def _get_profile_from_exchange(exchange: Exchange, type: str) -> ContributionProfile:
    return ContributionProfile(
        type=type,
        major=exchange.student_major if exchange.student_major is not None else "",
        minor=exchange.student_minor if exchange.student_minor is not None else "",
        exchange_semester=f"{exchange.semester}{exchange.year}",
        university_pk=exchange.university.pk,
    )


def get_contributions_profiles(date: datetime) -> List[ContributionProfile]:
    """
    Return the contributions profiles from the 24 hours following the date arg.

    If no university if associated with a contribution we don't return it.
    """
    date_to = date + timedelta(days=1)

    contributions_profiles = []

    exchange_feedbacks = ExchangeFeedback.objects.filter(
        updated_on__gte=date,
        updated_on__lt=date_to,
        exchange__university__isnull=False,
        untouched=False,
    ).prefetch_related("exchange")

    for exchange_feedback in exchange_feedbacks:
        exchange = exchange_feedback.exchange
        contribution_profile = _get_profile_from_exchange(exchange, "exchange_feedback")
        contributions_profiles.append(contribution_profile)

    course_feedbacks = CourseFeedback.objects.filter(
        updated_on__gte=date,
        updated_on__lt=date_to,
        course__exchange__university__isnull=False,
        untouched=False,
    ).prefetch_related("course__exchange")
    for course_feedback in course_feedbacks:
        exchange = course_feedback.course.exchange
        contribution_profile = _get_profile_from_exchange(exchange, "course_feedback")
        contributions_profiles.append(contribution_profile)

    return contributions_profiles
