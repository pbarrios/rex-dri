import logging
from datetime import timedelta, datetime

from django.core.management.base import BaseCommand
from django.utils.timezone import make_aware

from stats_app.compute_stats import update_all_stats


logger = logging.getLogger("Backfill stats")


def backfill_stats_history(date_from: datetime, date_to: datetime):
    """
    update all stats in the time range delimited by datetimes in arguments

    :param date_from: beginning of the time range
    :param date_to: end of the time range
    """

    local_date_from = date_from

    while local_date_from.strftime("%Y-%m-%d") != date_to.strftime("%Y-%m-%d"):
        logger.info(local_date_from)
        update_all_stats(local_date_from)
        local_date_from = local_date_from + timedelta(days=1)


class Command(BaseCommand):
    help = "Backfill REX-DRI stats data"

    def add_arguments(self, parser):
        parser.add_argument(
            "--date-from",
            required=True,
            type=str,
            help="The start date for the backfill (YYYY-MM-DD).",
        )
        parser.add_argument(
            "--date-to",
            required=True,
            help="The end date for the backfill (YYYY-MM-DD)",
        )

    def handle(self, *args, **options):
        raise Exception("Backfill is not permetted anymore, use with caution.")
        date_from = make_aware(datetime.strptime(options["date_from"], "%Y-%m-%d"))
        date_to = make_aware(datetime.strptime(options["date_to"], "%Y-%m-%d"))
        logger.info(f"Backfill from {date_from} to {date_to}.")
        backfill_stats_history(date_from, date_to)
