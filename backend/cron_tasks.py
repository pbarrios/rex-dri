import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "base_app.settings.main")
import django  # noqa: E402

django.setup()

# END OF SETUP
# DON'T PUT DJANGO RELATED IMPORTS BEFORE THIS

import logging  # noqa: E402

from uwsgidecorators import harakiri, cron, timer  # noqa: E402

from _cron_tasks import (
    clear_and_clean_sessions,
    update_daily_stats,
    update_currencies,
    update_extra_denormalization,
    update_utc_ent,
)  # noqa: E402


logger = logging.getLogger("django")


# @timer(60, target="spooler")  # use this for tests
@timer(3 * 60 * 60, target="spooler")  # run it three hours
@harakiri(40)
def _update_currencies(num):
    update_currencies()


@cron(5, 0, -1, -1, -1, target="spooler")  # everyday at 5 past midnight
@harakiri(60 * 10)  # shouldn't take more than 10 minutes to run
def _update_utc_ent(num):
    update_utc_ent()


@timer(60 * 60, target="spooler")  # run it every hour
@harakiri(60)
def _update_extra_denormalization(num):
    update_extra_denormalization()


@cron(20, 0, -1, -1, -1, target="spooler")  # everyday at 20 past midnight
@harakiri(60 * 5)  # shouldn't take more than 5 minutes to run
def _clear_and_clean_sessions(num):
    clear_and_clean_sessions()


@cron(30, 0, -1, -1, -1, target="spooler")  # everyday at 30 past midnight
@harakiri(60 * 5)  # shouldn't take more than 5 minutes to run
def _update_daily_stats(num):
    update_daily_stats()
