import imghdr
import xml.etree.cElementTree as et

from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from jsonschema import FormatChecker
from jsonschema.exceptions import ValidationError as JsonValidationError
from jsonschema.validators import validator_for

from backend_app.validation.schemas import (
    USEFUL_LINKS_SCHEMA,
    PHOTOS_SCHEMA,
    THEME_SCHEMA,
    RECOMMENDATION_LIST_CONTENT_SCHEMA,
)
from backend_app.validation.utils import DEFINITIONS_RESOLVER

# Format checker to use with jsonschemas to make sure errors are thrown
FORMAT_CHECKER = FormatChecker(["uri"])


class JsonValidator(object):
    """
    Class to hold the similar logic between the classes that are used validate JSON
    """

    # Value to override
    schema = None

    def __init__(self):
        self.validator = validator_for(self.schema)(
            self.schema, resolver=DEFINITIONS_RESOLVER, format_checker=FORMAT_CHECKER
        )

    def __call__(self, value):
        """
        Perform validation

        :param value: Value to validate
        :type value: list, dict
        :raises: ValidationErrort
        """
        try:
            self.validator.validate(value)
        except JsonValidationError as e:
            raise ValidationError(e.message)


@deconstructible()
class UsefulLinksValidator(JsonValidator):
    """
    Validator to be used on a JSON field that is supposed to store Useful links
    """

    schema = USEFUL_LINKS_SCHEMA


@deconstructible()
class PhotosValidator(JsonValidator):
    """
    Validator to be used on a JSON field that is supposed to store photos
    """

    schema = PHOTOS_SCHEMA


@deconstructible()
class ThemeValidator(JsonValidator):
    """
    Validator to be used on a JSON field that is supposed to store the theme data
    """

    schema = THEME_SCHEMA


@deconstructible()
class RecommendationListJsonContentValidator(JsonValidator):
    """
    Validator to be used on a JSON field that is supposed to store recommendation list content
    """

    schema = RECOMMENDATION_LIST_CONTENT_SCHEMA


@deconstructible()
class RecommendationListUnivValidator(object):
    """
    Validator to check that all the universities exist in the json of recommendation list
    """

    def __init__(self):
        from backend_app.models.university import University

        self.University = University

    @staticmethod
    def get_universities_ids_from_content(content):
        univ_ids = set()
        for block in content:
            if block["type"] == "univ-block":
                univ_ids.add(block["content"]["university"])
        return univ_ids

    def __call__(self, value):
        univ_ids_in_content = self.get_universities_ids_from_content(value)
        all_universities_ids = set(
            map(lambda univ: univ.pk, self.University.objects.all())
        )

        for univ_id in univ_ids_in_content:
            if univ_id not in all_universities_ids:
                raise ValidationError("Unrecognized university id {}".format(univ_id))


@deconstructible()
class PathExtensionValidator(object):
    """
    Validator to be used to check if a string ends with .(ext) from a list
    of allowed extensions.
    """

    def __init__(self, allowed_extensions):
        """
        :param allowed_extensions:
        :type allowed_extensions: iterable
        """
        self.allowed_extensions = [ext.lower() for ext in allowed_extensions]

    def __call__(self, string):
        """
        Perform validation

        :param string: Value to validate
        :type string: str
        :raises: ValidationError
        """
        try:
            if string.split(".")[-1].lower() not in self.allowed_extensions:
                raise ValidationError(
                    "The file you submitted has an unauthorized extension"
                )
        except KeyError:
            raise ValidationError("File extension not recognized")


@deconstructible()
class ImageValidator(object):
    """
    Validator to be check that a file is a valid image.
    Can't be tricked, definitely bulletproof.
    """

    # Plus svg
    allowed_format = ["jpeg", "png", "webp"]

    def __call__(self, file):
        self.is_image(file)

    def is_image(self, fp):
        if imghdr.what(fp) not in self.allowed_format:
            if type(fp) is str:
                with open(fp, "r") as f:
                    if not self.is_svg(f):
                        raise ValidationError("Image not recognized")
            else:
                if not self.is_svg(fp):
                    raise ValidationError("Image not recognized")

    @staticmethod
    def is_svg(f):
        """
        Check if the provided file is svg
        """
        f.seek(0)
        tag = None
        try:
            for event, el in et.iterparse(f, ("start",)):
                tag = el.tag
                break
        except et.ParseError:
            pass
        return tag == "{http://www.w3.org/2000/svg}svg"
