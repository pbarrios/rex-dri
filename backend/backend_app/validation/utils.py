from jsonschema import RefResolver

from backend_app.validation.schemas import DEFINITIONS_SCHEMA

DEFINITIONS_RESOLVER = RefResolver.from_schema(DEFINITIONS_SCHEMA)
