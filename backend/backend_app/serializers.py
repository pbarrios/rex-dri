from rest_framework import serializers

from backend_app.models.abstract.base import BaseModelSerializer
from backend_app.models.abstract.essentialModule import EssentialModuleSerializer
from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.exchange import Exchange


class CourseFeedbackSerializer(EssentialModuleSerializer):
    course_code = serializers.SerializerMethodField()  # needed for the front
    course_title = serializers.SerializerMethodField()  # needed for the front

    def get_course_code(self, obj):
        return obj.course.code

    def get_course_title(self, obj):
        return obj.course.title

    def update(self, instance, validated_data):
        instance.untouched = False
        return super().update(instance, validated_data)

    class Meta:
        model = CourseFeedback
        fields = EssentialModuleSerializer.Meta.fields + (
            "course_code",
            "course_title",
            "language",
            "comment",
            "adequation",
            "would_recommend",
            "working_dose",
            "following_ease",
            "untouched",
        )


COURSE_FIELDS = BaseModelSerializer.Meta.fields + (
    "course_feedback",
    "code",
    "title",
    "link",
    "ects",
    "category",
    "profile",
    "tsh_profile",
)


class CourseSerializerSimple(BaseModelSerializer):
    class Meta:
        model = Course
        fields = COURSE_FIELDS
        read_only_fields = COURSE_FIELDS


class CourseSerializer(CourseSerializerSimple):
    course_feedback = CourseFeedbackSerializer(many=False, read_only=True)


EXCHANGE_FIELDS = BaseModelSerializer.Meta.fields + (
    "year",
    "semester",
    "duration",
    "double_degree",
    "master_obtained",
    "student_major_and_semester",
    "student_major",
    "student_minor",
    "student_option",
    "exchange_courses",
    "university",
    "student",
)


class ExchangeSerializerSimple(BaseModelSerializer):
    exchange_courses = CourseSerializerSimple(many=True, read_only=True)
    student = serializers.SerializerMethodField()

    def get_student(self, obj):
        return self.get_user_related_field(obj.student)

    class Meta:
        model = Exchange
        fields = EXCHANGE_FIELDS
        read_only_fields = EXCHANGE_FIELDS


class ExchangeSerializer(ExchangeSerializerSimple):
    exchange_courses = CourseSerializer(many=True, read_only=True)
