from django.apps import AppConfig


class BackendAppConfig(AppConfig):
    name = "backend_app"

    def ready(self):
        from backend_app.signals.auto_creation import enable_auto_create
        from backend_app.signals.squash_revisions import enable_squash_revisions
        from backend_app.signals.update_nb_version import enable_update_nb_version
        from backend_app.signals.coherence_external_data import (
            enable_external_data_coherence,
        )

        enable_auto_create()
        enable_squash_revisions()
        enable_update_nb_version()
        enable_external_data_coherence()
