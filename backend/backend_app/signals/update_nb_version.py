from django.db.models.signals import post_delete
from reversion.models import Version


def update_nb_version_on_version_delete(sender, instance, **kwargs):
    pk = instance.object_id
    ct = instance.content_type

    obj = ct.get_object_for_this_type(pk=pk)
    # Make sure we have a new coherent value
    obj.nb_versions = len(Version.objects.get_for_object(obj))
    obj.save()


def enable_update_nb_version():
    post_delete.connect(update_nb_version_on_version_delete, sender=Version)
