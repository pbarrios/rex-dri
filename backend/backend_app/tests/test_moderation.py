from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.test import override_settings

from backend_app.models.for_testing.moderation import ForTestingModeration
from backend_app.models.pendingModeration import PendingModeration
from backend_app.tests.utils import WithUserTestCase
from backend_app.tests.utils import get_dummy_university


class ModerationTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.api_moderation = "/api/test/moderation/"

    def test_setting_ok(self):
        self.assertTrue(settings.TESTING)

    #####

    def _submit_post_test(self, client, data, end_point=None):
        if end_point is None:
            end_point = self.api_moderation
        response = client.post(end_point, data, format="json")
        self.assertEqual(response.status_code, 201)
        return response

    def _submit_put_test(self, client, data, pk):
        response = client.put(self.api_moderation + str(pk) + "/", data, format="json")
        self.assertEqual(response.status_code, 200)
        return response

    def _test_retreive_instance(self, data):
        matching = ForTestingModeration.objects.filter(aaa=data["aaa"])
        self.assertTrue(matching.exists())

        return matching[0]

    def _test_retreive_instance_in_moderation(self, instance, expected_fail=False):
        ct = ContentType.objects.get_for_model(ForTestingModeration)
        pending = PendingModeration.objects.filter(
            content_type=ct, object_id=instance.pk
        )
        if expected_fail:
            self.assertFalse(pending.exists())
            return None
        else:
            self.assertTrue(pending.exists())
            self.assertEqual(len(pending), 1)
            return pending[0]

    def _test_val_null(self, val1, null):
        if null:
            self.assertIsNone(val1)
        else:
            self.assertIsNotNone(val1)

    def _test_moderated_val(self, instance, null=True):
        self._test_val_null(instance.moderated_by, null)
        self._test_val_null(instance.moderated_on, null)

    def _test_updated_val(self, instance, null=True):
        self._test_val_null(instance.updated_on, null)
        self._test_val_null(instance.updated_by, null)

    def _test_has_pending_moderation_val(self, instance, has: bool):
        self.assertEqual(instance.has_pending_moderation, has)

    def _test_new_obj_val(self, instance, data):
        self.assertEqual(instance.new_object["aaa"], data["aaa"])

    ####

    @override_settings(MODERATION_ACTIVATED=True)
    def test_moderation_activated(self):
        """
        Test to check that when moderation IS activated,
        the app behaves as expected regarding creation
        and moderation of models.
        """

        # phase 1
        # Need moderation
        data_1 = {"aaa": "Test"}
        self._submit_post_test(self.authenticated_client, data_1)
        instance = self._test_retreive_instance(data_1)

        self._test_updated_val(instance, null=True)
        self._test_moderated_val(instance, null=True)
        self._test_has_pending_moderation_val(instance, True)

        instance_in_moderation = self._test_retreive_instance_in_moderation(instance)
        self._test_updated_val(instance_in_moderation, null=False)
        self._test_new_obj_val(instance_in_moderation, data_1)

        # Phase 2
        # More moderation
        data_2 = {"aaa": "Test 2"}
        self._submit_put_test(self.authenticated_client, data_2, instance.pk)
        instance = self._test_retreive_instance(data_1)  # not data_2 !

        self._test_updated_val(instance, null=True)
        self._test_moderated_val(instance, null=True)
        self._test_has_pending_moderation_val(instance, True)

        instance_in_moderation = self._test_retreive_instance_in_moderation(instance)
        self._test_updated_val(instance_in_moderation, null=False)
        # here we espect data_2
        self._test_new_obj_val(instance_in_moderation, data_2)

        # Phase 3
        # Moderation with modification
        data_3 = {"aaa": "Test 3"}
        self._submit_put_test(self.moderator_client, data_3, instance.pk)
        instance = self._test_retreive_instance(data_3)  # not data_2 !

        self._test_updated_val(instance, null=False)
        self._test_moderated_val(instance, null=False)
        self.assertEqual(instance.updated_by, self.moderator_user)
        # (below) because the field aaa was updated by the moderator
        self.assertEqual(instance.moderated_by, self.moderator_user)

        instance_in_moderation = self._test_retreive_instance_in_moderation(
            instance, True
        )
        self._test_has_pending_moderation_val(instance, False)

        # Phase 4
        # Put that require moderation
        data_4 = {"aaa": "Test 4"}
        self._submit_put_test(self.authenticated_client, data_4, instance.pk)
        instance = self._test_retreive_instance(data_3)  # not data_4 !

        self._test_updated_val(instance, null=False)  # Not True
        self._test_moderated_val(instance, null=False)  # Not True
        self._test_has_pending_moderation_val(instance, True)

        instance_in_moderation = self._test_retreive_instance_in_moderation(instance)
        self._test_updated_val(instance_in_moderation, null=False)
        # here we espect data_4
        self._test_new_obj_val(instance_in_moderation, data_4)

        # Phase 5
        # Moderation with no modification
        self._submit_put_test(self.moderator_client, data_4, instance.pk)
        instance = self._test_retreive_instance(data_4)  # not data_2 !

        self._test_updated_val(instance, null=False)
        self._test_moderated_val(instance, null=False)
        # (below) because the field aaa was NOT updated by the moderator
        self.assertEqual(instance.updated_by, self.authenticated_user)
        self.assertEqual(instance.moderated_by, self.moderator_user)

        instance_in_moderation = self._test_retreive_instance_in_moderation(
            instance, True
        )
        self._test_has_pending_moderation_val(instance, False)

    ####

    @override_settings(MODERATION_ACTIVATED=True)
    def test_staff_no_moderation(self):
        """
        Test to check that staff member are not subject to moderation.
        """

        data_1 = {"aaa": "Test sdfdf"}
        self._submit_post_test(self.staff_client, data_1)
        instance = self._test_retreive_instance(data_1)

        self._test_updated_val(instance, null=False)
        self._test_moderated_val(instance, null=False)
        self.assertEqual(instance.updated_by, self.staff_user)
        self.assertEqual(instance.moderated_by, self.staff_user)

        self._test_retreive_instance_in_moderation(instance, expected_fail=True)

    ####

    @override_settings(MODERATION_ACTIVATED=False)
    def test_moderation_not_activated(self):
        """
        Test to check that when moderation IS NOT activated,
        the app behaves as expected regarding creation
        and moderation of models.
        """

        # phase 1
        # Test that an authentificated user can post data
        # And that there is nothing pending in moderation
        data_1 = {"aaa": "Test"}
        self._submit_post_test(self.authenticated_client, data_1)
        instance = self._test_retreive_instance(data_1)

        self._test_updated_val(instance, null=False)
        self._test_moderated_val(instance, null=False)
        self.assertEqual(instance.updated_by, self.authenticated_user)
        self.assertEqual(instance.moderated_by, self.authenticated_user)

        self._test_retreive_instance_in_moderation(instance, expected_fail=True)

    ####
    @override_settings(MODERATION_ACTIVATED=True)
    def test_moderation_version_instance_error(self):
        """
        Test to check that when moderation IS activated
        no error regarding saving occures with foreing key
        """
        u = get_dummy_university()
        data = {"comment": "", "useful_links": [], "universities": [u.pk]}
        api_end_point = "/api/universityDri/?universities={}/".format(u.pk)
        self._submit_post_test(self.dri_client, data, api_end_point)
