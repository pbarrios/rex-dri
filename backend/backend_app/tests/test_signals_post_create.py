from django.test import TestCase

from base_app.models import User
from backend_app.models.userData import UserData

from backend_app.models.universityInfo import UniversityInfo
from backend_app.models.universitySemestersDates import UniversitySemestersDates
from backend_app.tests.utils import get_dummy_university


class AutomaticCreationPostCreateTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username="toto")

        cls.univ = get_dummy_university()

    def test_user_data_automatically_created(self):
        self.assertTrue(UserData.objects.filter(owner=self.user).exists())

    def test_univ_data_automatically_created(self):
        self.assertTrue(UniversityInfo.objects.filter(university=self.univ).exists())
        self.assertTrue(
            UniversitySemestersDates.objects.filter(university=self.univ).exists()
        )
