import json

from backend_app.tests.utils import get_dummy_university, WithUserTestCase
from backend_app.models.lastVisitedUniversities import LastVisitedUniversity


class LastVisitedUniversityTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.api_endpoint = "/api/lastVisitedUniversities/"
        cls.university = get_dummy_university()

    def test_create_last_visited_university(self):
        data = {"university": self.university.pk}
        response = self.authenticated_client.post(
            self.api_endpoint, data, format="json"
        )
        self.assertEqual(response.status_code, 201)

    def test_get_last_visited_universities_empty(self):
        response = self.authenticated_client.get(self.api_endpoint)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(len(content), 0)

    def test_get_last_visited_universities_one(self):
        LastVisitedUniversity.objects.create(
            user=self.authenticated_user, university=self.university
        )

        response = self.authenticated_client.get(self.api_endpoint)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(len(content), 1)
        self.assertEqual(content[0]["university"], self.university.pk)

    def test_get_last_visited_universities_one_several_times(self):
        for i in range(10):
            LastVisitedUniversity.objects.create(
                user=self.authenticated_user, university=self.university
            )

        response = self.authenticated_client.get(self.api_endpoint)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(len(content), 1)
        self.assertEqual(content[0]["university"], self.university.pk)

    def test_get_last_visited_universities_multiple(self):
        universities = [get_dummy_university(i) for i in range(10)]
        for univ in universities:
            LastVisitedUniversity.objects.create(
                user=self.authenticated_user, university=univ
            )

        response = self.authenticated_client.get(self.api_endpoint)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(len(content), 5)
        for i, res in enumerate(content):
            univ_id = res["university"]
            target_univ_id = universities[-1 - i].pk
            self.assertEqual(univ_id, target_univ_id)
