from backend_app.models.recommendationList import RecommendationList
from base_app.models import User

from .loadGeneric import LoadGeneric


class LoadRecommendationLists(LoadGeneric):
    """
    Class to load the tags in the app.
    """

    def __init__(self, admin: User):
        self.admin = admin

    def load(self):
        recommendation_list_ex = RecommendationList(
            title="Ma première liste",
            owner=self.admin,
            is_public=True,
            description="Ceci est une super liste !",
            content=[],
        )
        recommendation_list_ex.save()
