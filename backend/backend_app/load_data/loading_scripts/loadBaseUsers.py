import os

from base_app.models import User


class LoadBaseUsers(object):
    def __init__(self):
        """
        Creating admin user by creating a user and setting is_staff is True
        didn't enable access to the admin system for an unknown reason.
        Need to use create_superuser.
        """
        user = User.objects.filter(username="admin")
        if len(user) > 0:
            self.admin = user[0]
        else:
            User.objects.create_superuser(
                username=os.environ["DJANGO_ADMIN_USERNAME"],
                email="null@null.fr",
                password=os.environ["DJANGO_ADMIN_PASSWORD"],
            )
            self.admin = User.objects.filter(
                username=os.environ["DJANGO_ADMIN_USERNAME"]
            )[0]

        self.bot = User.objects.get_or_create(
            username="#bot",
            defaults=dict(
                first_name="#bot",
                last_name="",
                email="bot@null.fr",
                password=User.objects.make_random_password(30),
                pseudo="bot",
                allow_sharing_personal_info=True,
                has_validated_cgu_rgpd=True,
            ),
        )

    def get_admin(self) -> User:
        return self.admin
