import datetime

from django.utils import timezone

from base_app.models import SiteInformation
from base_app.models import User
from .loadGeneric import LoadGeneric


class LoadSiteInformation(LoadGeneric):
    """
        Load currencies in the app
    """

    def __init__(self, admin: User):
        self.admin = admin

    def load(self):
        start = timezone.now()
        end = start + datetime.timedelta(days=5)
        info = SiteInformation(
            start=start,
            end=end,
            variant="success",
            message="Le site vient d'être créé !",
        )
        self.add_info_and_save(info, self.admin)
