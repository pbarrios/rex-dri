import csv
from decimal import Decimal
from os.path import abspath, join

from backend_app.load_data.utils import ASSETS_PATH
from backend_app.models.currency import Currency
from base_app.models import User

from .loadGeneric import LoadGeneric


class LoadCurrencies(LoadGeneric):
    """
        Load currencies in the app
    """

    def __init__(self, admin: User):
        self.admin = admin

    def load(self):
        currencies_file_loc = abspath(join(ASSETS_PATH, "currencies.csv"))

        with open(currencies_file_loc) as csvfile:
            reader = csv.reader(csvfile, quotechar='"')
            next(reader)
            for r in reader:
                currency = Currency(
                    code=r[0],
                    name=r[1],
                    symbol="",
                    one_EUR_in_this_currency=Decimal(r[2]),
                )
                currency.save()
                self.add_info_and_save(currency, self.admin)
