from django.db import models
from rest_framework.permissions import BasePermission

from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
    VersionedEssentialModuleSerializer,
    VersionedEssentialModuleViewSet,
)
from backend_app.models.exchange import Exchange
from backend_app.models.university import University
from backend_app.permissions.app_permissions import ReadOnly, IsStaff, NoDelete, NoPost
from backend_app.permissions.moderation import ModerationLevels


class SharedUnivFeedback(VersionedEssentialModule):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS
    university = models.OneToOneField(University, on_delete=models.PROTECT, null=True)
    comment = models.CharField(default="", blank=True, max_length=5000)


class SharedUnivFeedbackSerializer(VersionedEssentialModuleSerializer):
    check_obj_permissions_for_edit = True

    class Meta:
        model = SharedUnivFeedback
        fields = VersionedEssentialModuleSerializer.Meta.fields + (
            "university",
            "comment",
        )
        read_only_fields = ("university",)


class SharedUnivPermission(BasePermission):
    """
    Permission that checks that the requester went to the university.
    """

    def has_object_permission(self, request, view, obj: SharedUnivFeedback):
        return Exchange.objects.filter(
            student=request.user, university=obj.university
        ).exists()


class SharedUnivFeedbackViewSet(VersionedEssentialModuleViewSet):
    permission_classes = (
        NoDelete & NoPost & (ReadOnly | IsStaff | SharedUnivPermission),
    )
    queryset = SharedUnivFeedback.objects.all()
    serializer_class = SharedUnivFeedbackSerializer
    end_point_route = "sharedUnivFeedbacks"
    filterset_fields = ("university",)
    required_filterset_fields = ("university",)
