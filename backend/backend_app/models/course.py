import logging

from django.db import models

from backend_app.models.abstract.base import BaseModel
from backend_app.models.exchange import Exchange

logger = logging.getLogger("django")


class Course(BaseModel):
    # Not using utc_id as Primary Key here for our model to be more resilient
    utc_id = models.IntegerField(null=False, unique=True)
    utc_exchange_id = models.IntegerField(null=False)
    code = models.CharField(max_length=10, null=True, blank=True)
    title = models.CharField(default="", null=False, blank=True, max_length=200)
    link = models.URLField(null=True, blank=True, max_length=500)
    ects = models.DecimalField(default=0, null=True, decimal_places=2, max_digits=7)
    category = models.CharField(null=True, blank=True, max_length=5)
    profile = models.CharField(null=True, blank=True, max_length=10)
    tsh_profile = models.CharField(null=True, blank=True, max_length=21)
    student_login = models.CharField(null=True, blank=True, max_length=8)

    # Field to tell that for some reason there is no corresponding exchange in the UTC DB
    unlinked = models.BooleanField(default=False, null=False)

    # a bit of denormalization
    exchange = models.ForeignKey(
        Exchange, on_delete=models.CASCADE, related_name="exchange_courses", null=True
    )

    def save(self, *args, **kwargs):
        """
        Custom handling of denormalization
        """
        try:
            exchange = Exchange.objects.get(utc_id=self.utc_exchange_id)
            self.exchange = exchange
        except Exchange.DoesNotExist:
            logger.error(
                "Trying to find exchange {} "
                "when updating course {} but it doesn't exist".format(
                    self.utc_exchange_id, self.utc_id
                )
            )

        super().save(*args, **kwargs)
