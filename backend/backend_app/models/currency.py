from django.core.validators import MinValueValidator
from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import ReadOnly


class Currency(BaseModel):
    code = models.CharField(primary_key=True, max_length=3)
    name = models.CharField(max_length=100)
    symbol = models.CharField(default="", blank=True, max_length=30)
    one_EUR_in_this_currency = models.DecimalField(
        max_digits=20, decimal_places=6, validators=[MinValueValidator(0)]
    )


class CurrencySerializer(BaseModelSerializer):
    class Meta:
        model = Currency
        fields = BaseModelSerializer.Meta.fields + (
            "code",
            "name",
            "symbol",
            "one_EUR_in_this_currency",
        )


class CurrencyViewSet(BaseModelViewSet):
    queryset = Currency.objects.all()  # pylint: disable=E1101
    serializer_class = CurrencySerializer
    permission_classes = (ReadOnly,)
    end_point_route = "currencies"
