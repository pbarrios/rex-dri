from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from rest_framework import serializers

from backend_app.fields import JSONField
from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import ReadOnly
from base_app.models import User


class PendingModeration(BaseModel):
    """
    Model to hold models instances that are pending moderation.
    """

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=100)  # 100 should be ok
    referenced_object = GenericForeignKey("content_type", "object_id")

    updated_by = models.ForeignKey(User, on_delete=models.CASCADE)
    updated_on = models.DateTimeField(null=True)

    # Object pending moderation
    # No validation of the JSON it's used internally only and the data is
    # validated before arriving here.
    new_object = JSONField(default=dict)

    class Meta:
        unique_together = ("content_type", "object_id")


class PendingModerationSerializer(BaseModelSerializer):
    """
    Serializer for the Pending Moderation Model
    """

    content_type = serializers.CharField(read_only=True)
    object_id = serializers.CharField(read_only=True)

    updated_by = serializers.CharField(read_only=True)
    updated_on = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)

    new_object = serializers.JSONField(read_only=True)

    class Meta:
        model = PendingModeration
        fields = BaseModelSerializer.Meta.fields + (
            "content_type",
            "object_id",
            "updated_by",
            "updated_on",
            "new_object",
        )


class PendingModerationViewSet(BaseModelViewSet):
    """
    Viewset for the pending moderation model.
    """

    queryset = PendingModeration.objects.all()  # pylint: disable=E1101
    serializer_class = PendingModerationSerializer
    end_point_route = "pendingModeration"


class PendingModerationObjViewSet(BaseModelViewSet):
    """
    Viewset to retrieve the pending moderation data for an object
    """

    serializer_class = PendingModerationSerializer
    permission_classes = (ReadOnly,)
    end_point_route = (
        r"pendingModerationObj/(?P<content_type_id>[0-9]+)/(?P<object_pk>[0-9A-Za-z]+)"
    )

    def get_queryset(self):
        content_type_id = self.kwargs["content_type_id"]
        object_pk = self.kwargs["object_pk"]
        return PendingModeration.objects.filter(
            content_type=content_type_id, object_id=object_pk
        )
