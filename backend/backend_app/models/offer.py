import logging

from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.models.partner import Partner
from backend_app.models.shared import SEMESTER_OPTIONS
from backend_app.models.university import University
from backend_app.permissions.app_permissions import ReadOnly
from backend_app.utils import CustomPagination

logger = logging.getLogger("django")


class Offer(BaseModel):
    utc_partner_id = models.IntegerField(null=False)
    year = models.PositiveIntegerField(default=2018, null=True)
    semester = models.CharField(
        max_length=2, choices=SEMESTER_OPTIONS, default="A", null=True
    )

    comment = models.TextField(max_length=500, null=True)
    double_degree = models.BooleanField(default=False)
    is_master_offered = models.BooleanField(default=False, null=True)

    nb_seats_offered = models.PositiveIntegerField(null=True)
    # null => exchange not possible

    majors = models.CharField(max_length=4000, null=True)

    # A bit of denormalization
    partner = models.ForeignKey(Partner, on_delete=models.PROTECT, null=False)
    university = models.ForeignKey(
        University, on_delete=models.PROTECT, null=True, related_name="offers"
    )

    def save(self, *args, **kwargs):
        """
        Custom handling of denormalization and force character
        """

        # make sure the semester is upper case
        if self.semester is not None:
            self.semester = self.semester.upper()

        if self.utc_partner_id is not None:
            try:
                self.partner = Partner.objects.get(utc_id=self.utc_partner_id)
                self.university = self.partner.university
            except Partner.DoesNotExist:
                self.partner = None
                self.university = None
                logger.error(
                    "Trying to find partner {}"
                    "when updating offer {} but it doesn't exist".format(
                        self.utc_partner_id, self.pk
                    )
                )
        else:
            self.partner = None
            self.university = None
        super().save(*args, **kwargs)

    class Meta:
        unique_together = ("utc_partner_id", "year", "semester")


class OfferSerializer(BaseModelSerializer):
    class Meta:
        model = Offer
        fields = BaseModelSerializer.Meta.fields + (
            "year",
            "semester",
            "comment",
            "double_degree",
            "is_master_offered",
            "majors",
            "university",
            "nb_seats_offered",
        )


class OfferViewSet(BaseModelViewSet):
    queryset = Offer.objects.all().order_by(
        "-year", "semester"
    )  # pylint: disable=E1101
    serializer_class = OfferSerializer
    permission_classes = (ReadOnly,)
    end_point_route = "offers"
    required_filterset_fields = ("university",)
    filterset_fields = ("university",)
    pagination_class = CustomPagination
