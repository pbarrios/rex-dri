from django.db import models

from backend_app.fields import JSONField
from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
    VersionedEssentialModuleSerializer,
    VersionedEssentialModuleViewSet,
)
from backend_app.models.country import Country
from backend_app.models.university import University
from backend_app.permissions.app_permissions import NoPost, NoDelete
from backend_app.permissions.moderation import ModerationLevels
from backend_app.utils import get_module_defaults_for_bot, revision_bot
from backend_app.validation.validators import UsefulLinksValidator

useful_link_validator = UsefulLinksValidator()

UNIVERSITY_TAG_CHOICES = (
    "accommodation",
    "administrative",
    "transport",
    "student_life",
    "other",
)


class UniversityTaggedItem(VersionedEssentialModule):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    university = models.ForeignKey(
        University, on_delete=models.PROTECT, related_name="university_tagged_items"
    )
    tag = models.CharField(
        max_length=20,
        choices=[
            (name, name) for name in UNIVERSITY_TAG_CHOICES
        ],  # Choices is a list of Tuple
    )
    comment = models.CharField(default="", blank=True, max_length=5000)
    useful_links = JSONField(default=list, validators=[useful_link_validator])

    class Meta:
        unique_together = ("university", "tag")


class UniversityTaggedItemSerializer(VersionedEssentialModuleSerializer):
    class Meta:
        model = UniversityTaggedItem
        fields = VersionedEssentialModuleSerializer.Meta.fields + (
            "university",
            "tag",
            "comment",
            "useful_links",
        )


class UniversityTaggedItemViewSet(VersionedEssentialModuleViewSet):
    queryset = UniversityTaggedItem.objects.all()  # pylint: disable=E1101
    serializer_class = UniversityTaggedItemSerializer
    # permission_classes = (IsOwner,)
    end_point_route = "universityTaggedItems"
    filterset_fields = ("university",)
    required_filterset_fields = ("university",)
    permission_classes = VersionedEssentialModuleViewSet.permission_classes


#########################
#########################
#########################
#########################


COUNTRY_TAG_CHOICES = (
    "immigration",
    "administrative",
    "culture",
    "tourism",
    "transport",
    "other",
)


class CountryTaggedItem(VersionedEssentialModule):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    country = models.ForeignKey(
        Country, on_delete=models.PROTECT, related_name="country_tagged_items"
    )
    tag = models.CharField(
        max_length=20, choices=[(name, name) for name in COUNTRY_TAG_CHOICES]
    )
    comment = models.CharField(default="", blank=True, max_length=5000)
    useful_links = JSONField(default=list, validators=[useful_link_validator])

    class Meta:
        unique_together = ("country", "tag")


class CountryTaggedItemSerializer(VersionedEssentialModuleSerializer):
    class Meta:
        model = CountryTaggedItem
        fields = VersionedEssentialModuleSerializer.Meta.fields + (
            "country",
            "tag",
            "comment",
            "useful_links",
        )


class CountryTaggedItemViewSet(VersionedEssentialModuleViewSet):
    queryset = CountryTaggedItem.objects.all()  # pylint: disable=E1101
    serializer_class = CountryTaggedItemSerializer
    end_point_route = "countryTaggedItems"
    filterset_fields = ("country",)
    required_filterset_fields = ("country",)
    permission_classes = (NoPost & NoDelete,)


#########################
#########################
#########################
#########################


def initialize_all():
    defaults = get_module_defaults_for_bot()
    for university in University.objects.all():
        for tag in UNIVERSITY_TAG_CHOICES:
            with revision_bot():
                UniversityTaggedItem.objects.get_or_create(
                    tag=tag, university=university, defaults=defaults
                )

    for country in Country.objects.all():
        for tag in COUNTRY_TAG_CHOICES:
            with revision_bot():
                CountryTaggedItem.objects.get_or_create(tag=tag, country=country)
