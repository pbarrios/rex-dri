from django.db import models

from backend_app.models.abstract.scholarship import (
    Scholarship,
    ScholarshipSerializer,
    ScholarshipViewSet,
)
from backend_app.models.country import Country
from backend_app.permissions.moderation import ModerationLevels


class CountryScholarship(Scholarship):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    countries = models.ManyToManyField(Country, related_name="country_scholarships")


class CountryScholarshipSerializer(ScholarshipSerializer):
    class Meta:
        model = CountryScholarship
        fields = ScholarshipSerializer.Meta.fields + ("countries",)


class CountryScholarshipViewSet(ScholarshipViewSet):
    permission_classes = ScholarshipViewSet.permission_classes
    queryset = CountryScholarship.objects.all()  # pylint: disable=E1101
    serializer_class = CountryScholarshipSerializer
    end_point_route = "countryScholarships"
    filterset_fields = ("countries",)
