from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from backend_app.models.abstract.essentialModule import EssentialModule
from backend_app.models.course import Course
from backend_app.models.language import Language
from backend_app.permissions.moderation import ModerationLevels


class CourseFeedback(EssentialModule):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS

    course = models.OneToOneField(
        Course, on_delete=models.CASCADE, default=0, related_name="course_feedback"
    )
    language = models.ForeignKey(
        Language, on_delete=models.PROTECT, related_name="courses", null=True
    )
    comment = models.TextField(null=True, max_length=1500)
    adequation = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
    would_recommend = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
    working_dose = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
    following_ease = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )

    # Field to tell that the instance hasn't been edited as of now
    untouched = models.BooleanField(default=True, null=False)
