from django.db import models

from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.country import Country
from backend_app.permissions.app_permissions import IsStaff, IsDri, ReadOnly
from backend_app.permissions.moderation import ModerationLevels


class CountryDri(Module):
    moderation_level = ModerationLevels.DEPENDING_ON_SITE_SETTINGS
    countries = models.ManyToManyField(Country, related_name="country_dri")


class CountryDriSerializer(ModuleSerializer):
    class Meta:
        model = CountryDri
        fields = ModuleSerializer.Meta.fields + ("countries",)


class CountryDriViewSet(ModuleViewSet):
    queryset = CountryDri.objects.all()  # pylint: disable=E1101
    serializer_class = CountryDriSerializer
    permission_classes = (IsStaff | IsDri | ReadOnly,)
    end_point_route = "countryDri"
    filterset_fields = ("countries",)
