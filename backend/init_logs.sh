#!/bin/bash

# Init log locations to make sure they are caught by the logs_rotation service on startup
# And there is no issue in gitlab ci
mkdir -p /var/log/django
touch /var/log/django/error.log

# Frontend errors are logged through django
mkdir -p /var/log/frontend
touch /var/log/frontend/error.log
