"""
Django settings

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os
import sys
from os.path import normpath, join

from base_app.settings.dir_locations import BACKEND_ROOT_DIR, REPO_ROOT_DIR
from .app_settings import *  # noqa: F403, F401 # We need to load app specific settings

# A stupid assert to prevent pycharm from removing the * import
assert MODERATION_ACTIVATED or not MODERATION_ACTIVATED  # noqa: F405

#####################################
#
#   Django essential settings
#
#####################################


try:
    SECRET_KEY = os.environ["SECRET_KEY"]
except KeyError:
    raise Exception("Env variable missing. Please run `make setup`")

ROOT_URLCONF = "base_app.urls"

WSGI_APPLICATION = "base_app.wsgi.application"

STATIC_URL = "/static/"
STATIC_ROOT = normpath(join(BACKEND_ROOT_DIR, "static"))

MEDIA_ROOT = normpath(join(BACKEND_ROOT_DIR, "media"))
MEDIA_URL = "/media/"

#
#
#####################################
#
#   Sub-applications in the app
#
#####################################


INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_cas_ng",
    "reversion",
    "reversion_compare",
    "rest_framework",
    "rest_framework.authtoken",
    "backend_app",
    "base_app",
    "external_data",
    "stats_app",
    "webpack_loader",
    "django_filters",
]

#
#
#####################################
#
#   Rest framework configuration
#
#####################################


REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    "DEFAULT_PERMISSION_CLASSES": (
        "rest_framework.permissions.DjangoModelPermissions",
    ),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
}

#
#
#####################################
#
#   Webpack loader config to enable frontend hot module replacement
#
#####################################


WEBPACK_LOADER = {
    "DEFAULT": {
        "BUNDLE_DIR_NAME": "base_app/frontend_dist/bundles/",
        "STATS_FILE": join(REPO_ROOT_DIR, "frontend/webpack-stats.json"),
    }
}

#
#
#####################################
#
#   Middlewares applied to the request
#
#####################################


MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "base_app.middleware.RexDriRequestMiddleware",
]

#
#
#####################################
#
#   Big variables settings
#
#####################################


if os.environ["ENV"] == "DEV":
    DEBUG = True
    ALLOWED_HOSTS = ["*"]
    INSTALLED_APPS += ["django_extensions", "debug_toolbar"]
    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE

    # Force the debug toolbar to be shown
    DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": "base_app.utils.show_toolbar"}
else:
    DEBUG = False
    ALLOWED_HOSTS = ["127.0.0.1", "localhost", "rex.dri.utc.fr"]

TESTING = "pytest" in sys.modules

#
#
#####################################
#
#   Authentication related
#
#####################################


# Use a custom User model for optimization
AUTH_USER_MODEL = "base_app.User"

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "django_cas_ng.backends.CASBackend",
]

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation."
        "UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation." "MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation." "CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation." "NumericPasswordValidator"},
]

LOGIN_URL = "/user/login"
RGPD_URL = "/rgpd-raw"
LOGIN_EXEMPT_URLS = [LOGIN_URL, RGPD_URL]

if TESTING:
    # In a testing environment we may want to by pass the loginRequiredMiddleware
    LOGIN_EXEMPT_URLS.append("api")
    LOGIN_EXEMPT_URLS.append("/admin/")

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

#
#
#####################################
#
#   Django Templating settings
#
#####################################


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

#
#
#####################################
#
#   Database configuration
#
#####################################

# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ["POSTGRES_DB"],
        "USER": os.environ["POSTGRES_USER"],
        "PASSWORD": os.environ["POSTGRES_PASSWORD"],
        "HOST": os.environ["POSTGRES_HOST"],
        "PORT": os.environ["POSTGRES_PORT"],
    }
}

# To use Sqlite, uncomment below and comment above
# DATABASES = {
#     "default": {
#         "ENGINE": "django.db.backends.sqlite3",
#         "NAME": join(BACKEND_ROOT_DIR, "./database.db"),
#     }
# }


#
#
#####################################
#
#   Internationalization
#
#####################################

# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True

#
#
#####################################
#
#   Logging
#
#####################################

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"},
    },
    "formatters": {
        "verbose": {
            "format": "%(levelname)s|%(asctime)s|%(module)s|%(process)d|%(thread)d|%(message)s",
            "datefmt": "%d/%b/%Y %H:%M:%S",
        }
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "filters": ["require_debug_true"],
            "class": "logging.StreamHandler",
        },
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        },
        "log_to_file_django": {
            "level": "WARNING",
            "filters": ["require_debug_false"],
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": "/var/log/django/error.log",
        },
        "log_to_file_frontend": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "logging.FileHandler",
            "formatter": "verbose",
            "filename": "/var/log/frontend/error.log",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["console", "log_to_file_django", "mail_admins"],
            "level": "INFO",
        },
        "frontend": {"handlers": ["console", "log_to_file_frontend"], "level": "INFO"},
    },
}
