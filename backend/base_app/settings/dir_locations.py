import os
from os.path import dirname, join

####################################
#
#   Directory locations
#
####################################

BACKEND_ROOT_DIR = dirname(dirname(dirname(os.path.abspath(__file__))))
REPO_ROOT_DIR = dirname(BACKEND_ROOT_DIR)
ENVS_FILE_DIR = join(REPO_ROOT_DIR, "server/envs/")
