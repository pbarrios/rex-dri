from django import forms
from base_app.models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("has_validated_cgu_rgpd",)
