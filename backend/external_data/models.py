from django.db import models


class ExternalDataUpdateInfo(models.Model):
    timestamp = models.DateTimeField(auto_now=True)
    source = models.CharField(
        max_length=50
    )  # Source identifier of the data that was updated
