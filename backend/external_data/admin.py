from django.contrib import admin

from external_data.models import ExternalDataUpdateInfo

admin.site.register(ExternalDataUpdateInfo)
