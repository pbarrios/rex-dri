from backend_app.models.exchange import update_denormalized_univ_major_minor

from backend_app.models.university import update_denormalized_univ_field  # noqa: E402
from external_data.management.commands.utils import FixerData, UtcData  # noqa: E402
from base_app.management.commands.clean_user_accounts import (
    ClearUserAccounts,
    ClearSessions,
)
from stats_app.compute_stats import update_all_stats


def update_currencies():
    FixerData().update()


def update_utc_ent():
    UtcData().update()


def update_extra_denormalization():
    update_denormalized_univ_major_minor()
    update_denormalized_univ_field()


def clear_and_clean_sessions():
    ClearUserAccounts.run()
    ClearSessions.run()


def update_daily_stats():
    update_all_stats()
